@extends('dashboard.layouts.master')

@section('page-title', 'Edit Hotel Policy')

@section('page-header')
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>
<link href="{{ asset('assets/css/bootstrap.css')}}" rel="stylesheet">
<h1>
    Edit Rates
    <small>Rate Details</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('rate.index') }}">Rate Management</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')
@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@include('partials.messages')



{!! Form::open(['route' => ['rate.norcalupdate', $prop1->id], 'method' => 'PUT', 'id' => 'prop-form']) !!}

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
                        <div class="panel-heading">Room Rates</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                <div class="form-group">
                                    <label for="hotel_name"> Hotel</label>
                                   
                                        <select  name="hotel_name" class="form-control" id="hotel_name">
                                        <option disabled value="">Select</option>
                                              <?php foreach($hotel as $r => $value){ 
                                                $selected='disabled';
                                                if( $value['id'] == $prop1->hotel_id_fk ) { 
                                                    $selected='selected';
                                                    } ?>
                                                
                                                <option value="<?php echo $value['id']; ?>" <?php echo $selected; ?>><?php echo $value['hotel_name']; ?></option>   
                                            
                                            <?php } ?>
                                        </select>
                                        <?php
                                            $room = 0;
                                            if($prop1) 
                                             {
                                        $room = $prop1->room_type;
                                        }
                                                                
                                           ?>  
                                    </div>

                                    <div class="form-group">
                                        <label for="room_type">Room Type</label>
                                        <select name="room_type" class="form-control" id="room_type">
                                             <option value=""></option>
                                        </select>                         
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                <label for="datepicker_from">From</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type="text" class="form-control"  id="datepicker_from" name="datepicker_from" placeholder="From" value="{{$prop1->from_date }}" >
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                 <label for="datepicker_to">To</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepicker2'>
                                            <input type="text" class="form-control"  id="datepicker_to" name="datepicker_to" placeholder="To" value="{{$prop1->to_date }}">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label for="normal">
                                                <input type="radio"  name="normal1" id= "normal" value="1" checked readonly >
                                                        Normal Rate
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label for="special">
                                                <input type="radio"  name="normal1" id= "special" value="2" >
                                                        Special Rate
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="rates">Rate</label>
                                            <input type="text" class="form-control" id="rates" name="room_type_rates" placeholder="" value="{{$prop1->rate }}">
                                    </div>
                                </div>
                            </div>
                            <br><br>
                           
                           <div class="panel panel-default">
<div class="panel-heading">Extra Bed </div>    
<div class="panel-body">

        <div class="col-md-6">
            <div class="form-group" >
                <br>
                <label for="extra_bed">Extra Bed Allow ?</label>
                <br>
                
                <div>
                    <?php
                    if ($prop1->adult_bed){
                        $select = 'checked="checked"';
                        $style = 'style="display:block;"';
                        $room_rate = $prop1->adult_bed;
                        $rate_field_exist = $prop1->id;
                        $rate_field = 1;
                    } else {
                        $select = "";
                        $style = 'style="display:none;"';
                        $room_rate = "";
                        $rate_field_exist = "";
                        $rate_field = "";
                    }
                    ?>
                    <input type="checkbox" name="extra_bed" id="extra_bed" onchange="getrates(1)" value="1" <?php echo $select; ?>>
                </div>
            </div>    
            <div class="form-group" id="rate" <?php echo $style; ?> >
                <label for="extra_bed">Extra Bed Rate</label>   
                <input type="text" name="rate" placeholder="Eg:1000" value="<?php echo $room_rate; ?>" class="form-control">
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <input type="hidden" name="id" value="">
                <input type="hidden" name="rate_field" id="rate_field" value="<?php echo $rate_field; ?>"> 
                <input type="hidden" name="rate_field_exist" id="rate_field_exist" value="<?php echo $rate_field_exist; ?>">   
            </div> 
        </div>
                                
                           
                               <div class="col-md-6">
            <div class="form-group" >
                <br>
                <label for="extra_bed_kids">Extra Bed Allow For Kids..?</label>
                <br>
                
                <div>
                    <?php
                    if ($prop1->kids_bed){ 
                        $select = 'checked="checked"';
                        $style = 'style="display:block;"';
                        $room_rate = $prop1->kids_bed;
                        $rate_field_exist = $prop1->id;
                        $rate_field = 1;
                    } else {
                        $select = "";
                        $style = 'style="display:none;"';
                        $room_rate = "";
                        $rate_field_exist = "";
                        $rate_field = "";
                    }
                    ?>
                    <input type="checkbox" name="extra_bed_kids" id="extra_bed_kids" onchange="getrates(2)" value="2" <?php echo $select; ?>>
                </div>
            </div>    
            <div class="form-group" id="Kids_bed_rate" <?php echo $style; ?> >
                <label for="extra_bed_kids">Kids Bed Rate</label>   
                <input type="text" name="Kids_bed_rate" placeholder="Eg:1000" value="<?php echo $room_rate; ?>" class="form-control">
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <input type="hidden" name="id" value="">
                <input type="hidden" name="rate_field_kids" id="rate_field_kids" value="<?php echo $rate_field; ?>"> 
                <input type="hidden" name="rate_field_exist_kids" id="rate_field_exist_kids" value="<?php echo $rate_field_exist; ?>">   
            </div> 
        </div>
        </div>
        </div>
                             
                            <button  type="submit" class="btn btn-primary" id="update-login-details-btn" >
                                <i class="fa fa-refresh"></i>
                                    Update Details
                            </button>
                        </div>
                    </div>
    </div>
</div>

    {!! Form::close() !!}


@stop

@section('after-scripts-end')

<link href="{{ URL::asset('assets/css/bootstrap.css')}}" rel="stylesheet">

<script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                    format: 'YYYY-MM-DD',
                });
            });
</script>


<script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    format: 'YYYY-MM-DD',
                });
            });
</script>

<script>
    function getrates(id) {
    if(id==1){
       if(document.getElementById("extra_bed").checked == true) {
       $("#rate").show();
       $("#rate_field").val(1);
    } else {
       $("#rate").hide(); 
       $("#rate_field").val("");
    } 
    } else {
        
    if(document.getElementById("extra_bed_kids").checked == true) {
       $("#Kids_bed_rate").show();
       $("#rate_field_kids").val(1);
    } else {
       $("#Kids_bed_rate").hide(); 
       $("#rate_field_kids").val("");
    }
    }
}
</script>

<script type="text/javascript">
       
    //$('#hotel_name').change(function(){
        var hotel_name = $('#hotel_name').val();
        if(hotel_name=="")
        {
            alert("Please select a hotel ");
        }
        else
        {
            $.ajax({
                url     : '/public/en/dashboard/getname',
                type    : 'GET',
                data    : {hotel_name:hotel_name},
                success : function(data) {

                        $('#room_type') .find('option') .remove() .end();
                        var option = '';
                        option += '<option value="">Select</option>';
                        var options = $.map(data, function(el) { return el }) ;
                        
                        var length = options.length;
                        var room_t= <?php echo $room; ?> ;

                        for(var j = 0; j < length; j++) {

                          var select='disabled';
                            if(options[j].id == room_t) {

                               var select='selected';

                            }
                         option += '<option value="' +options[j].id +'" '+select+'> ' + options[j].room_type + '</option>';
                        }
                        $("select#room_type").html(option);
                }
            },"json");
        }
   // });
       
</script>
@stop
