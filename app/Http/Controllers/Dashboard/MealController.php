<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Meal_type;
use App\Http\Controllers\Controller;

class MealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meal = Meal_type::all();
        return view('dashboard.masters.mealtype.index',compact('meal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.masters.mealtype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'meal_type' => 'required',
                'meal_desc' => 'required'
                ]);
        $input=$request->all();
        Meal_type::create($input);
        return redirect()->back()->withSuccess(trans('Meal Type successfully added!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meal = Meal_type::findOrFail($id);
        return view('dashboard.masters.mealtype.edit',compact('meal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $meal = Meal_type::findOrFail($id);

        $this->validate($request, [
            'meal_type' => 'required',
            'meal_desc' => 'required'
        ]);

        $input = $request->all();
        $meal->fill($input)->save();     
        return redirect()->back()->withSuccess(trans('Meal Type successfully updated!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meal = Meal_type::findOrFail($id);
        $meal->delete();
        return redirect()->route('meal.index')->withSuccess(trans('Meal Type successfully deleted!'));
    }
}
