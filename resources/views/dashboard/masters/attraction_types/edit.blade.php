@extends('dashboard.layouts.master')

@section('page-title', 'Edit Attraction Type')

@section('page-header')
<h1>
    Edit Attraction Type
    <small>Attraction Types</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.attraction_types') }}">>Attraction Types</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => ['dashboard.attraction_types.update', $attraction_types->id], 'method' => 'PUT', 'id' => 'attraction-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Attraction Type Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="facility">Type</label>
                    <input type="text" class="form-control" id="type" name="type" placeholder="" required="" value="{{$attraction_types->type }}">
                </div>
                <div class="form-group">
                    <label for="facility_desc">Description</label>
                    <textarea name="description" placeholder="" class="form-control" required="">{{$attraction_types->description }}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Facility\UpdateRequest', '#attraction-form') !!}
@stop
