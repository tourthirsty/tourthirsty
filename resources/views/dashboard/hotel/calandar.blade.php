@extends('dashboard.layouts.master')

@section('page-title', 'Hotel Rates')

@section('page-header')
<h1>
    Hotel Rates
    
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="#">>Hotel Rates</a></li>
</ol>
@endsection

@section('content')

@include('partials.messages')
<div class="row">
   <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Hotel</div><?php //print_r($hotels); exit;?>
            <div class="panel-body">
                <div class="form-group">
                    <label for="hotel_facility">Select Your Hotel</label>
                    <select class="selectpicker form-control" id="hotel">
                        <?php
                            foreach ($hotels as $k => $value) {
                            $selected = "";
                            ?>
                            <option value="<?php echo $value->id; ?>" <?php echo $selected; ?>><?php echo $value->hotel_name; ?></option>

                            <?php
                        }
                       ?>
                    </select>
                     

                </div>
                <button  type="submit" onclick="getHotelrates()" class="btn btn-primary"><i class="fa fa-gear"></i> Go </button>
            </div>
        </div>
    </div>
</div>
@stop

@section('after-scripts-end')
<script>
function getHotelrates() {
    var hotel = 0;
    var hotel = $("#hotel").val();
    var posturl = "<?php echo url('/en/dashboard/hotel/rateinCalandar/'); ?>";
    var token = $("input[name=_token]").val();
    window.location.href = posturl+"/"+hotel;
}  
</script>
@stop
