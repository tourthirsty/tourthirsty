<?php

namespace App\Http\Controllers;
namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Property_type;
use App\Http\Controllers\Controller;

class ProptypeController extends Controller
{
	/**
	 * Displays the page with all property type.
	 *
	 *
	 */
    public function index() {
		$prop = Property_type::all();
		return view('dashboard.masters.prop_type.list',compact('prop'));
		
	}
	
	/**
     * Show the form for creating a new propert type.
     *
     * @return \Illuminate\Http\Response
     */
	public function create()
    {
        return view('dashboard.masters.prop_type.add');
    }
	
	 /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
	{
		$this->validate($request,[
				'prop_type_name' => 'required',
				'prop_type_desc' => 'required'
				]);
		$input=$request->all();
		Property_type::create($input);

        return redirect()->back()->withSuccess(trans('Type successfully added!'));
   
	}
	
	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function edit($id)
    {
         $prop = Property_type::findOrFail($id);
         return view('dashboard.masters.prop_type.edit',compact('prop'));
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 public function update(Request $request, $id)
    {
        $prop = Property_type::findOrFail($id);
        $this->validate($request, [
            'prop_type_name' => 'required',
            'prop_type_desc' => 'required'
        ]);

        $input = $request->all();
        $prop->fill($input)->save();
        return redirect()->back()->withSuccess(trans('Property type successfully updated!'));
    }
	
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function destroy($id)
    {
        $prop = Property_type::findOrFail($id);
        $prop->delete();
        Session::flash('flash_message', 'Task successfully deleted!');
        return redirect()->route('proptype.index');
    }
}
