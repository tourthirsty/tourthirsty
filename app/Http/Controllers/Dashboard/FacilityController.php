<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facility as Facility;

class FacilityController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Facility = Facility::all();

        return view('dashboard.masters.facilities.index',compact('Facility'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.masters.facilities.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'facility' => 'required',
            'facility_desc' => 'required'
        ]);

        
        
        $Facilty = new Facility;

        $Facilty->fac_name = $request->facility;
        $Facilty->fac_desc = $request->facility_desc;

        $Facilty->save();
        
        return redirect()->route('dashboard.facility')
                        ->withSuccess(trans('Facility Added Succesfully'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $facility = Facility::findOrFail($id);

         return view('dashboard.masters.facilities.edit',compact('facility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Facilities = Facility::findOrFail($id);
         $this->validate($request, [
            'facility' => 'required',
            'facility_desc' => 'required'
        ]);
//echo "<pre>"; print_r($request); exit;
        $Facilities->fac_name = $request->facility;
        $Facilities->fac_desc = $request->facility_desc;

        $Facilities->save();
        
        return redirect()->route('dashboard.facility')
                        ->withSuccess(trans('Details Updated Succesfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Facility = Facility::findOrFail($id);

        $Facility->delete();

        return redirect()->route('dashboard.facility')
                        ->withSuccess(trans('Details Deleted Succesfully'));
    }
}
