<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Special_rate extends Model
{
    protected $table = 'special_rates';

    protected $fillable = [
		'room_type', 'rate_type', 'from_date', 'to_date', 'rate',
	];
}
