@extends('dashboard.layouts.master')

@section('page-title','Nearest Place')

@section('page-header')
<link href="{{ asset('assets/css/bootstrap-multiselect.css') }}" rel="stylesheet" type="text/css" >
@if($errors->any())
<div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
</div>
@endif
<h1>
    Add Place to Visit
    <small></small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="#">Locations</a></li>
    <li class="active">Add Place to Visit</li>
</ol>
@endsection

@section('content')


@if(Session::has('flash_message'))
<div class="alert alert-success">
    {{ Session::get('flash_message') }}
</div>
@endif

<div class="row">
    <div class="col-md-12">
        {!! Form::open(['route' => ['location.storeplace'], 'method' => 'post', 'id' => 'place-details-form','files'=>true]) !!}
        <div class="panel panel-default">
            <div class="panel-heading">Place to Visit</div>
            <div class="panel-body">
                <div class="row form-group">
                    <div class="col-md-10">
                        <label for="place">Place</label>
                        <input type="text" class="form-control" rows="5" cols="5" id="place" placeholder="Eg:Munnar" name="place" value="">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-10">
                        <label for="desc">Description</label>
                        <textarea class="form-control" rows="5" cols="5" id="desc" placeholder="Enter the Description" name="desc" value=""></textarea>
                    </div>
                    
                </div>
                <div class="row form-group">
                    <div class="col-md-10">
                        <label for="desc">Select your Images</label>
                        <input type="file" class="form-control" id="" name="image1" placeholder="Select your Image">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-10">
                         <input type="file" class="form-control" id="" name="image2" placeholder="Select your Image">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-10">
                        <input type="file" class="form-control" id="" name="image3" placeholder="Select your Image">
                    </div>
                </div>
                <!-- #~~~~~~~~~~~~~~~~~~~~~Google Map Starts Here!~~~~~~~~~~~~~~~~~~~# -->

                <div class="row form-group">
                    <div class="col-md-10">
                        <label for="addmap">Select the Location in Map</label>
                        <input type="text" id="auto" name="add" value="" class="form-control" placeholder="Enter Place to mark location on map">
                    </div>
                </div>
                <div id="coords"></div>
                <div class="row col-md-12">
                    <div class="form-group">
                        <div id="gmap" class="col-md-12" style="height:500px;"></div>
                    </div>      
                </div>

                <!-- #~~~~~~~~~~~~~~~~~~~~~Google Map Ends Here!~~~~~~~~~~~~~~~~~~~# -->        
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <!--<label>Latitude:</label>-->
                            <input type="hidden" readonly="readonly" name="latitude" class="form-control txtfld" id="lat" value="">
                            <input type="hidden" readonly="readonly" name="longitude" class="form-control txtfld" id="lng" value="">
                            <input type="hidden" class="form-control" id="" name="id" placeholder="" value="<?php echo $id; ?>">
                            <input type="hidden" class="form-control" id="distance" name="distance" value="">
                            <input type="hidden" class="form-control" id="placelatitude" name="placelatitude" placeholder="" value="<?php echo $latitude; ?>">
                            <input type="hidden" class="form-control" id="placelongitude" name="placelongitude" placeholder="" value="<?php echo $longitude; ?>">
                        </div>
                    </div>
                </div>
                <br>                           
                <div> 
                    <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                        <i class="fa fa-refresh"></i>
                        Update Details
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@stop

@section('after-scripts-end')

<script async defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBmSevauEk2OuvnnY8DY5po9ghDYmRrFs4&libraries=places&callback=initMap"></script>

<script>

function initMap() {

   var latit = "0";
   var longi = "0";

   var map = new google.maps.Map(document.getElementById('gmap'), {
       center: {lat: 10.0036852, lng: 76.28473400000007},
       zoom: 19
   });

   var input = /** @type {!HTMLInputElement} */(
           document.getElementById('auto'));
   //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
   var autocomplete = new google.maps.places.Autocomplete(input);
   autocomplete.bindTo('bounds', map);
   var infowindow = new google.maps.InfoWindow();
   var marker = new google.maps.Marker({
       position: new google.maps.LatLng(latit, longi),
       map: map,
       anchorPoint: new google.maps.Point(0, -29)
   });

   autocomplete.addListener('place_changed', function () {
       infowindow.close();
       marker.setVisible(false);
       var place = autocomplete.getPlace();
       if (!place.geometry) {
           window.alert("Autocomplete's returned place contains no geometry");
           return;
       }
       if (place.geometry.viewport) {
           map.fitBounds(place.geometry.viewport);
       } else {
           map.setCenter(place.geometry.location);
           map.setZoom(17);  // Why 17? Because it looks good.
       }
       marker.setIcon(/** @type {google.maps.Icon} */({
           url: place.icon,
           size: new google.maps.Size(71, 71),
           origin: new google.maps.Point(0, 0),
           anchor: new google.maps.Point(17, 34),
           scaledSize: new google.maps.Size(35, 35)
       }));
       marker.setPosition(place.geometry.location);
       marker.setVisible(true);
       var address = '';


       if (place.address_components) {
           address = [
               (place.address_components[0] && place.address_components[0].short_name || ''),
               (place.address_components[1] && place.address_components[1].short_name || ''),
               (place.address_components[2] && place.address_components[2].short_name || '')
           ].join(' ');
       }

       infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
       infowindow.open(map, marker);
       var la = place.geometry.location.lat();
       var lo = place.geometry.location.lng();
       var elem = document.getElementById("lat");
       elem.value = la;
       var elem1 = document.getElementById("lng");
       elem1.value = lo;
       var distance = getDistanceFromLatLonInKm(la,lo);
       $("#distance").val(distance);
   });


}
function getDistanceFromLatLonInKm(lat1,lon1) {
  var lat2 = $("#placelatitude").val();
  var lon2 = $("#placelongitude").val();
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}


</script>
{!! JsValidator::formRequest('App\Http\Requests\Location\CreateRequest', '#place-details-form') !!}

@stop

