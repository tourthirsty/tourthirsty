<?php

namespace App\Http\Requests\Master\Roomtype;
use App\Http\Requests\Request;
use App\Room_type;

class UpdateRoomtypeRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'Room_type_name' => 'required',
		];
	}
}
