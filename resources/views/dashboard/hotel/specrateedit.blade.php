@extends('dashboard.layouts.master')

@section('page-title', 'Edit Hotel Policy')

@section('page-header')

<h1>
    Edit Rates
    <small>Rate Details</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.hotel.edit', $prop2->hotel_id_fk) }}">Hotel Details</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@include('partials.messages')



{!! Form::open(['route' => ['hotel.speccalupdate', $prop2->id], 'method' => 'PUT', 'id' => 'prop-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Rate Details</div>
            <div class="panel-body">
                 <div class="row">
            <div class="col-md-4">
                 <div class="form-group">
                    <label for="room_type">Room Type</label>
                    <input type="text" class="form-control" id="room_type" name="room_type" required="" value="{{$prop2->room_type }}" readonly>
                </div>
                </div>
                <div class="col-sm-4">
                                    <div class="form-group" style="padding-top: 24px;">
                                        <div class='input-group date' id='datetimepicker1'>
                                            <!--<label for="to">From</label>-->
                                            <input type="text" class="form-control"  id="from_date" name="from_date" placeholder="From" value="{{$prop2->from_date }}" >
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" style="padding-top: 24px;" >
                                        <div class='input-group date' id='datetimepicker2' >
                                            <!--<label for="datepicker_to">To</label>-->
                                            <input type="text" class="form-control"  id="to_date" name="to_date" placeholder="To" value="{{$prop2->to_date }}">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                    <div class="col-md-4">
                <div class="form-group">
                    <div class="radio">
                                            <label for="normal">
                                                <input type="radio"  name="normal1" id= "normal" value="1"  readonly>
                                                        Normal Rate
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label for="special">
                                                <input type="radio"  name="normal1" id= "special" value="2" checked readonly>
                                                        Special Rate
                                            </label>
                                        
                </div>
                </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="rate">Rates</label>
                    <input type="text" class="form-control" id="rate" name="rate" required="" value="{{$prop2->rate }}">
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('after-scripts-end')

<link href="{{ URL::asset('assets/css/bootstrap.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                    format: 'YYYY-MM-DD',
                });
            });
</script>


<script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    format: 'YYYY-MM-DD',
                });
            });
</script>

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Proptype\UpdateProptypeRequest', '#prop-form') !!}
@stop