@extends('dashboard.layouts.master')

@section('page-title', 'Edit Property Details')

@section('page-header')



<h1>
    Edit Property Details
    <small>Property Details</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('propertydetails.index') }}">Property details</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@include('partials.messages')



{!! Form::open(['route' => ['dashboard.propertydetails.update', $prop->id], 'method' => 'PUT', 'id' => 'prop-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Property Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="hotel_details_no_rooms">No.of Rooms</label>
                    <input type="text" class="form-control" id="hotel_details_no_rooms" name="hotel_details_no_rooms" required="" value="{{$prop->hotel_details_no_rooms }}">
                </div>
				<div class="form-group">
                    <label for="hotel_details_checkin_time">Checkin Time</label>
                    <input type="text" class="form-control" id="hotel_details_checkin_time" name="hotel_details_checkin_time" required="" value="{{$prop->hotel_details_checkin_time }}">
                </div>
				<div class="form-group">
                    <label for="hotel_details_checkout_time">Checkout Time</label>
                    <input type="text" class="form-control" id="hotel_details_checkout_time" name="hotel_details_checkout_time" required="" value="{{$prop->hotel_details_checkout_time }}">
                </div>
                <div class="form-group">
                    <label for="hotel_details_no_restaurants">No. of Restaurants</label>
                    <input type="text" class="form-control" id="hotel_details_no_restaurants" name="hotel_details_no_restaurants" required="" value="{{$prop->hotel_details_no_restaurants }}">
                </div>
				<div class="form-group">
                    <label for="hotel_details_roomservice_timing">Room Service Timing</label>
                    <input type="text" class="form-control" id="hotel_details_roomservice_timing" name="hotel_details_roomservice_timing" required="" value="{{$prop->hotel_details_roomservice_timing }}">
                </div>
				<div class="form-group">
                    <label for="hotel_details_bar">No. of Bars</label>
                    <input type="text" class="form-control" id="hotel_details_bar" name="hotel_details_bar" required="" value="{{$prop->hotel_details_bar }}">
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Masters\Proptype\UpdateProptypeRequest', '#prop-form') !!}
@stop