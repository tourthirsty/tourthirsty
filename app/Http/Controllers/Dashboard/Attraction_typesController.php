<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Attraction_types as Attraction_types;

class Attraction_typesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Attraction_types = Attraction_types::all();

        return view('dashboard.masters.attraction_types.index',compact('Attraction_types'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.masters.attraction_types.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'type' => 'required',
            'description' => 'required'
        ]);

        
        
        $Attraction_types = new Attraction_types;

        $Attraction_types->type = $request->type;
        $Attraction_types->description = $request->description;

        $Attraction_types->save();
        
        return redirect()->route('dashboard.attraction_types')
                        ->withSuccess(trans('Attraction Type Added Succesfully'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $attraction_types = Attraction_types::findOrFail($id);

         return view('dashboard.masters.attraction_types.edit',compact('attraction_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Attraction_types = Attraction_types::findOrFail($id);
         $this->validate($request, [
            'type' => 'required',
            'description' => 'required'
        ]);
//echo "<pre>"; print_r($request); exit;
        $Attraction_types->type = $request->type;
        $Attraction_types->description = $request->description;

        $Attraction_types->save();
        
        return redirect()->route('dashboard.attraction_types')
                        ->withSuccess(trans('Details Updated Succesfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Attraction_types = Attraction_types::findOrFail($id);

        $Attraction_types->delete();

        return redirect()->route('dashboard.attraction_types')
                        ->withSuccess(trans('Details Deleted Succesfully'));
    }
}
