<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fhraiclassification extends Model
{
    protected $table = 'fhraiclassification';
    protected $fillable =[
        'fhrai_class_name',
        'fhrai_class_desc'
    ];
}
