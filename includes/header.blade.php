<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Meta -->
    <meta name="description" content="@yield('meta_description', 'Default Description')">
    <meta name="author" content="@yield('meta_author', 'Aniji Jacob')">
    @yield('meta')

    <title>@yield('page-title') | {{ settings('app_name') }}</title>

    <!--<link href="{{'assets/frontend/css/bootstrap.css' }}" rel="stylesheet">
    <link href="{{'assets/frontend/css/style.css' }}" rel="stylesheet">
    <link href="{{'assets/frontend/css/font-awesome.min.css' }}" rel="stylesheet">-->
    <link href=" {{ URL::asset('assets/frontend/css/bootstrap.css') }}" rel="stylesheet">
    <link href=" {{ URL::asset('assets/frontend/css/style.css') }}" rel="stylesheet">
    <link href=" {{ URL::asset('assets/frontend/css/font-awesome.min.css') }}" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
  </head>
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="topbrd"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-4 no-padding-right logo"><a href="{{url('/')}}"><img  src="{{ URL::asset('assets/frontend/images/logo.jpg') }}"/></a></div>
          <div class="col-md-8 col-sm-8 col-xs-12 menuarea no-padding-right">
            <div class="col-md-12 menutop no-padding-right">
              <span><img src="{{URL::asset('assets/frontend/images/phone.png')}}"/>234-56-8901</span>
              <span><img src="{{URL::asset('assets/frontend/images/inbox.png')}}"/>info@tourthirsty.com</span>

              @if (Auth::guest())
                <span>{!! link_to('login', trans('app.login')) !!}</span>
                <!--<span>{!! link_to('register', trans('app.sign_up')) !!}</span>-->
                 
              @else
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->present()->name }} <span class="caret"></span>
                  </a>

                  <ul class="dropdown-menu" role="menu">
                    <li>{!! link_to_route('dashboard', trans('app.dashboard')) !!}</li>
                    <li>{!! link_to_route('auth.logout', trans('app.logout')) !!}</li>
                  </ul>
                </li>
              @endif
              
                
            </div>
            <div style="clear:both;"></div>
            @include('frontend.includes.nav')
        </div>
    </div>
</div>