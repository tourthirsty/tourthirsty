<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel_tech_detail extends Model
{
        protected $table = 'hotel_tech_details';
		protected $fillable =[
        'hotel_fac_name',
        'hotel_fac_desc'
    ];
}
