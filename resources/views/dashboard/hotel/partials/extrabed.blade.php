<div class="panel panel-default">
<div class="panel-heading">Extra Bed </div>    
<div class="panel-body">
        <div class="col-md-6">
            <div class="form-group" >
                <br>
                <label for="extra_bed">Extra Bed Allow ?</label>
                <br>
                <?php // if(isset($Extra_bed_details)) { echo "here"; } else { echo "else";}  exit;?>
                <div>
                    <?php
                    if (isset($Extra_bed_details)) {
                        $select = 'checked="checked"';
                        $style = 'style="display:block;"';
                        $room_rate = $Extra_bed_details->room_rate;
                        $rate_field_exist = $Extra_bed_details->id;
                        $rate_field = 1;
                    } else {
                        $select = "";
                        $style = 'style="display:none;"';
                        $room_rate = "";
                        $rate_field_exist = "";
                        $rate_field = "";
                    }
                    ?>
                    <input type="checkbox" name="extra_bed" id="extra_bed" onchange="getrates(1)" class="switch" <?php echo $select; ?>>
                </div>
            </div>    
            <div class="form-group" id="rate" <?php echo $style; ?> >
                <label for="extra_bed">Extra Bed Rate</label>   
                <input type="text" name="rate" placeholder="Eg:1000" value="<?php echo $room_rate; ?>" class="form-control">
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="rate_field" id="rate_field" value="<?php echo $rate_field; ?>"> 
                <input type="hidden" name="rate_field_exist" id="rate_field_exist" value="<?php echo $rate_field_exist; ?>">   
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-refresh"></i>
                    Update Details
                </button>
            </div> 
        </div>

    </div>

