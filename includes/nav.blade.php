<div class="menubtm">
    <nav role="navigation" class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse no-padding-right">
            <ul class="nav navbar-nav" id="nav">
                <li class=""><a href="{{url('/')}}">HOME</a></li>
                <li class="rgtbrd"></li>
                <li><a href="#">HOLIDAYS</a></li>
                <li class="rgtbrd"></li> 
                <li><a href="{{url('destination')}}">DESTINATIONS</a></li>
                <li class="rgtbrd"></li>
                <li><a href="{{url('showcms/1')}}">ABOUT US</a></li>
                <li class="rgtbrd"></li>
                <li><a href="{{url('contact_us')}}">CONTACT US</a></li>
            </ul>
        </div>
    </nav>
</div>