@extends('dashboard.layouts.master')

@section('page-title', 'Edit Activities')

@section('page-header')
<h1>
    Edit Activity
    <small>Activity</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.activities') }}">>Activities</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => ['dashboard.activities.update', $Activities->id], 'method' => 'PUT', 'id' => 'activity-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Activity Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="email">Activity</label>
                    <input type="text" class="form-control" id="activity" name="activity" placeholder="Hill Station" required="" value="{{$Activities->activity }}">
                </div>
                <div class="form-group">
                    <label for="username">Description</label>
                    <textarea name="description" placeholder="" class="form-control" required="">{{$Activities->description }}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\UpdateActivitiesRequest', '#activity-form') !!}
@stop
