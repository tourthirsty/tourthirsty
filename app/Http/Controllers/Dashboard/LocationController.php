<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Session\SessionRepository;
use App\Repositories\User\UserRepository;
use App\Http\Requests;
use Session;
use DB;
use Input;
use App\Location as Locations;
use App\Locationtype as Loctype;
use App\Places_to_visit as Places;
use App\Images_place as Placeimages;

class LocationController extends Controller
{
    private $users;

    /**
     * UsersController constructor.
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users) {
        $this->middleware('auth');
        $this->middleware('session.database', ['only' => ['sessions', 'invalidateSession']]);
        $this->users = $users;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = DB::select(DB::raw('SELECT  a.*,
        GROUP_CONCAT(b.loc_type_name ORDER BY b.id) locname FROM locations a
        INNER JOIN locationtypes b ON FIND_IN_SET(b.id, a.loc_type) > 0 GROUP   BY a.id')); 
        return view('dashboard.location.index',compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $loctype = Loctype::all(); 
        return view('dashboard.location.create',compact('loctype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'Location_name' => 'required',
            'location_types' => 'required',
            'Location_desc' => 'required',
        ]);
            
        $loc = new Locations;
        $loc->loc_name = $request->Location_name;
        $loc->loc_type = implode(',', Input::get('location_types'));
        $loc->loc_desc = $request->Location_desc;
        $loc->loc_latitude=$request->latitude;
        $loc->loc_longitude=$request->longitude;
        $loc->save();
        return redirect()->route('location.index')
                        ->withSuccess(trans('Details Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $loc = Locations::findOrFail($id);
        $loctype = Loctype::all(); 
        return view('dashboard.location.edit',compact('loc','loctype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $loc = Locations::findOrFail($id);

        $this->validate($request, [
            'Location_name' => 'required',
            'location_types' => 'required',
            'Location_desc' => 'required',
        ]);
        $loc->loc_name = $request->Location_name;
        $loc->loc_type = implode(',', Input::get('location_types'));
        $loc->loc_desc = $request->Location_desc;
        $loc->loc_latitude=$request->latitude;
        $loc->loc_longitude=$request->longitude;
        $loc->save();
        return redirect()->route('location.index')
                        ->withSuccess(trans('Details Updated Successfully'));

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $loc = Locations::findOrFail($id);
        $loc->delete();
        Session::flash('flash_message', 'Task successfully deleted!');
        return redirect()->route('location.index')
                        ->withSuccess(trans('Details Deleted Successfully'));
   
    }
     public function getPlacetovisit($id) {
        $loc = Locations::findOrFail($id);
        $places = Places::where('loc_id_fk', '=', $id)->get();
        return view('dashboard.location.placetovisit',compact('id','loc','places'));
    }
    public function createPlacetovisit($id) {
        $loc = Locations::find($id);
        $latitude = $loc->loc_latitude;
        $longitude = $loc->loc_longitude; 
        return view('dashboard.location.createplace',compact('id','latitude','longitude'));
    }
    public function storePlacetovisit(Request $request) {
       //echo "<pre>"; print_r($request->all());
         $this->validate($request, [
            'place' => 'required',
            'desc' => 'required',
           ]);
        $place = new Places;
        $place->loc_id_fk = $request->id;
        $place->place = $request->place;
        $place->desc = $request->desc;
        $place->dist_from_loc = $request->distance;
	$place->place_latitude = $request->latitude;
        $place->place_longitude = $request->longitude;
        $place->save();
        $placeId = $place->id;
        
        
               for($i=1;$i<=3;$i++) {
               if($request->hasFile('image'.$i))
	       {    
	       $destinationPath="upload/places";
	       $file = $request->file('image'.$i);
	       $filename=$file->getClientOriginalName(); 
	       $myfile = value(function() use ($file){
			$filename = str_random(10) . '.' . $file->getClientOriginalExtension();
			return strtolower($filename);
		});
	       $request->file('image'.$i)->move($destinationPath,$myfile);
               //---------Insert the Details--------------//
               $images = new Placeimages;
               $images->place_id_fk = $placeId;
               $images->url = $myfile;
               $images->save();
               }
       
	   }
        Session::flash('flash_message', 'Details Updated Succesfully!');
      return \Redirect::route('location.placetovisit', $request->id);
    }
    public function editPlacetovisit($id) {
        $place = Places::where('places_to_visit.id', $id)
               ->join('locations', 'locations.id', '=', 'places_to_visit.loc_id_fk') 
               ->leftJoin('images_place', 'images_place.place_id_fk', '=', 'places_to_visit.id')
               ->select('places_to_visit.*',DB::raw('group_concat(images_place.url) as url'),DB::raw('group_concat(images_place.id) as imageId'),'locations.loc_latitude','locations.loc_longitude')
               ->first();
       return view('dashboard.location.editplace',compact('id','place'));
    }
     public function deletePlaceimage(Request $request)
    {
       
       $loc = Placeimages::findOrFail($request->id);
       $loc->delete();
       
   
    }
    public function updatePlacetovisit(Request $request) {

       // echo "<pre>"; print_r($request->all()); exit;
        $this->validate($request, [
            'place' => 'required',
            'desc' => 'required',
           ]);
         $place = Places::findOrFail($request->id);
         $place->place = $request->place;
         $place->desc = $request->desc;
         $place->dist_from_loc = $request->distance;
         $place->place_latitude = $request->latitude;
         $place->place_longitude = $request->longitude;
         $place->save();
         for($i=1;$i<=3;$i++) {
               if($request->hasFile('image'.$i))
	       {    
	       $destinationPath="upload/places";
	       $file = $request->file('image'.$i);
	       $filename=$file->getClientOriginalName(); 
	       $myfile = value(function() use ($file){
			$filename = str_random(10) . '.' . $file->getClientOriginalExtension();
			return strtolower($filename);
		});
	       $request->file('image'.$i)->move($destinationPath,$myfile);
               //---------Insert the Details--------------//
               $images = new Placeimages;
               $images->place_id_fk = $request->id;
               $images->url = $myfile;
               $images->save();
               }
         }
         Session::flash('flash_message', 'Details Updated Succesfully!');
         return \Redirect::route('location.placetovisit', $request->placeId);

    }
    public function showPlacetovisit($id) {
        $place = Places::where('places_to_visit.id', $id)
               ->join('locations', 'locations.id', '=', 'places_to_visit.loc_id_fk') 
               ->leftJoin('images_place', 'images_place.place_id_fk', '=', 'places_to_visit.id')
               ->select('places_to_visit.*',DB::raw('group_concat(images_place.url) as url'),DB::raw('group_concat(images_place.id) as imageId'),'locations.loc_latitude','locations.loc_longitude')
               ->first();
       return view('dashboard.location.viewplace',compact('id','place'));
    }
    public function deleteNearplace($id) {
        $loc = Places::findOrFail($id);
        $loc->delete();
        $images = Placeimages::where('place_id_fk', '=', $id)->get();
        foreach ($images as $value) {
         //File::delete('upload/places/' . $value['url']);   
         $value->delete();
         }
        Session::flash('flash_message', 'Details Deleted Succesfully!');
        return \Redirect::route('location.placetovisit', $loc->loc_id_fk);
    }
}
