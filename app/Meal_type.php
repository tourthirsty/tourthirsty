<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal_type extends Model
{
    protected $table = 'meal_types';
    protected $fillable = array(
			'meal_type',
			'meal_desc');
}
