<?php

namespace App\Http\Requests\Hotel;

use App\Http\Requests\Request;
use App\Hotel;
use App\User;

class CreateRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'full_name' => 'required|min:3',
			'group_name' => 'required|min:6',
			'phone' => 'numeric|min:6',
 			'email' => 'required|email',
			'username' => 'required|min:6',
			'password' => 'required|min:6|confirmed',
		    ];
	}
}
