@extends('dashboard.layouts.master')

@section('page-title','Edit Aminities')

@section('page-header')

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
   
<h1>Edit Room Aminities
	<small>Edit Room Aminity details</small>
</h1>

<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('aminities.index') }}">Room Aminities</a></li>
    <li class="active">Update Details</li>
</ol>

@endsection

@section('content')

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@include('partials.messages')

{!! Form::open(['route' => ['dashboard.aminities.update', $prop->id], 'method' => 'PUT', 'id' => 'aminities-form']) !!}

<div class="row">
	<div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Aminity Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="room_amn_name">Aminity</label>
                    <input type="text" class="form-control" id="room_amn_name" name="room_amn_name" placeholder="Central Air Conditioning,Room A/C..." required="" value="{{$prop->room_amn_name }}">
                </div>
                <div class="form-group">
                    <label for="room_amn_desc">Description</label>
                    <textarea name="room_amn_desc" placeholder="Eg:The hill stations are high-altitude towns used, especially by European colonialists..." class="form-control" required="">{{$prop->room_amn_desc }}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>

{!! Form::close() !!}

@stop

@section('after-scripts-end')
	{!! JsValidator::formRequest('App\Http\Requests\Master\Aminities\UpdateAminitiesRequest', '#aminities-form') !!}
@stop

