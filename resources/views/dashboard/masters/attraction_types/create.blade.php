@extends('dashboard.layouts.master')

@section('page-title', 'Add Attraction Type')

@section('page-header')
<h1>
    Add Attraction Type
    <small>Attraction Types</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.attraction_types') }}">>Attraction types</a></li>
    <li class="active">Add New</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => 'dashboard.attraction_types.store', 'files' => true, 'id' => 'attraction-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Attraction types</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="facility">Type</label>
                    <input type="text" class="form-control" id="type" name="type" placeholder="Nature & Parks" required="">
                </div>
                <div class="form-group">
                    <label for="facility_desc">Description</label>
                    <textarea name="description" placeholder="Eg:A nature park is a landscape protected by means of long-term planning, use and agriculture...." class="form-control" required=""></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Attraction_types\CreateRequest', '#attraction-form') !!}
@stop
