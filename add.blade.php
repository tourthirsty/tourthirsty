@extends('dashboard.layouts.master')

@section('page-title')

@section('page-header')

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
    <h1>
       Add Property Details
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li><a href="{{ route('proptype.index') }}">Property Details</a></li>
        <li class="active">Create</li>
      </ol>
@endsection

@section('content')


@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif


    {!! Form::open(['route' => 'dashboard.propertydetails.store', 'id' => 'permission-form']) !!}


<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Property details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="hotel_details_no_rooms">No.of Rooms</label>
                    <input type="text" class="form-control" id="hotel_details_no_rooms"
                           name="hotel_details_no_rooms">
                </div>
				<div class="form-group">
                    <label for="hotel_details_checkin_time">Checkin Time</label>
                    <input type="text" class="form-control" id="hotel_details_checkin_time"
                           name="hotel_details_checkin_time" >
                </div>
				<div class="form-group">
                    <label for="hotel_details_checkout_time">Checkout Time</label>
                    <input type="text" class="form-control" id="hotel_details_checkout_time"
                           name="hotel_details_checkout_time" >
                </div>
                
                <div class="form-group">
                    <label for="hotel_details_no_restaurants">No. of Restaurants</label>
                    <input type="text" class="form-control" id="hotel_details_no_restaurants"
                           name="hotel_details_no_restaurants">
                </div>
				<div class="form-group">
                    <label for="hotel_details_roomservice_timing">Room Service Timing</label>
                    <input type="text" class="form-control" id="hotel_details_roomservice_timing"
                           name="hotel_details_roomservice_timing">
                </div>
				<div class="form-group">
                    <label for="hotel_details_bar">No. of Bars</label>
                    <input type="text" class="form-control" id="hotel_details_bar"
                           name="hotel_details_bar">
                </div>
                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-2">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Create 
        </button>
    </div>
</div>

@stop

@section('fter-scripts-end')
    
        {!! JsValidator::formRequest('App\Http\Requests\Masters\Proptype\CreateProptypeRequest', '#permission-form') !!}
   <

@stop