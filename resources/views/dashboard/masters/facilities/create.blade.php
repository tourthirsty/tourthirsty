@extends('dashboard.layouts.master')

@section('page-title', 'Add Hotel Facility')

@section('page-header')
<h1>
    Add Facility
    <small>Facility</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.facility') }}">>Facility</a></li>
    <li class="active">Add New</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => 'dashboard.facility.store', 'files' => true, 'id' => 'facility-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Facilities</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="facility">Facility</label>
                    <input type="text" class="form-control" id="acility" name="facility" placeholder="AC Room" required="">
                </div>
                <div class="form-group">
                    <label for="facility_desc">Description</label>
                    <textarea name="facility_desc" placeholder="Eg:The hill stations are high-altitude towns used, especially by European colonialists..." class="form-control" required=""></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Facility\CreateRequest', '#facility-form') !!}
@stop
