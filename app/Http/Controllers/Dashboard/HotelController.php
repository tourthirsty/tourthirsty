<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Events\User\Deleted;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Session\SessionRepository;
use App\Repositories\User\UserRepository;
use App\Support\Enum\UserStatus;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Fhraiclassification as Fhrai;
use App\Hotel as Hotel;
use App\User as User;
use App\Role as Role;
use Session;
use App\Propertyinformation as Propertyinfo;
use App\Facility as Facility;
use App\Hotel_property_detail as hoteldetails;
use App\Hotel_policy as policy;
use App\Hotel_bank_detail as bank;
use App\Hotel_authorized_contact as contact;
use App\Hotelfacility as Hotelfacility;
use App\Property_type as Property_type;
use App\Hotel_prop_type as Hotelproptype;
use App\Hotel_tech_detail as techdetails;
use App\Room_type as Room_type;
use App\Room_aminity as Room_aminity;
use App\Hotelroomaminities as Hotel_room_aminities;
use App\Hotel_group_detail as Groupdetails;
use App\Meal_type as Mealtype;
use App\Hotel_meal_type as Hotelmeal;
use App\Room_details as Room_details;
use App\Extra_bed_details as Extra_bed_details;
use Auth;
use App\rate as Rates;
use App\Special_rate as sp_rate;
use App\Availability as Available;

class HotelController extends Controller {

    private $users;

    /**
     * UsersController constructor.
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users) {
        $this->middleware('auth');
        $this->middleware('session.database', ['only' => ['sessions', 'invalidateSession']]);
        $this->users = $users;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $User = Hotel::users();
        return view('dashboard.hotel.index', compact('User'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('dashboard.hotel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'full_name' => 'required',
            'optiontype' => 'required',
            'phone' => 'numeric',
            'email' => 'required',
            'password' => 'required'
        ]);
       
        $data = $request->all() + ['status' => UserStatus::ACTIVE];

        // Username should be updated only if it is provided.
        // So, if it is an empty string, then we just leave it as it is.

        $data['username'] = null;
        $data['first_name'] = $request->full_name;
        $data['last_name'] = null;
        $user = $this->users->create($data);
        $this->users->updateSocialNetworks($user->id, []);
        $this->users->setRole($user->id, $request->get('role'));
        $id = $user->id;
        Session::put('hoteluser', $id);
        if($request->optiontype==1) {
           return redirect()->route('hotel.creategroup',$id)
                        ->withSuccess(trans('Hotel User Added Succesfully')); 
        } elseif($request->optiontype==2) {
            return redirect()->route('hotel.createHotels',0)
                        ->withSuccess(trans('Hotel User Added Succesfully')); 
        }
        return redirect()->route('dashboard.hotel')
                        ->withSuccess(trans('Hotel User Added Succesfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }
    /**
     * Removes the user from database.
     *
     * @param User $user
     * @return $this
     */
    public function delete(User $user) {

            $this->users->delete($user->id);

            event(new Deleted($user));

            return redirect()->route('dashboard.hotel')
                    ->withSuccess(trans('app.user_deleted'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $fhrai = Fhrai::lists('fhrai_class_name', 'id');
        $Propertyinfo = Propertyinfo::where('hotel_id_fk', '=', $id)->first();
        $facility = Facility::all();
	    $type= Property_type::all();
	    $room_type = Room_type::all();
        $Room_aminity = Room_aminity::all();
        $Hotelroom_amenities = Hotel_room_aminities::where('hotel_id_fk', '=', $id)->get();
        $room_type_edit = Hotel_room_aminities::select('room_type')->orderBy('id', 'asc')->groupBy('room_type')->where('hotel_id_fk', '=', $id)->get() ;
        $room_aminities_edit = Hotel_room_aminities::select('room_amn_id_fk','room_type')->groupBy('room_amn_id_fk')->groupBy('room_type')->where('hotel_id_fk', '=', $id)->get();
        $Hotelfacility = Hotelfacility::where('hotel_id_fk', '=', $id)->get();
	    $Propertydetails = hoteldetails::where('hotel_id_fk', '=', $id)->first();
	    $hotelpolicy = policy::where('hotel_id_fk', '=', $id)->first();
	    $bankdetails = bank::where('hotel_id_fk', '=', $id)->first();
	    $authcontact = contact::where('hotel_id_fk', '=', $id)->get();
	    $hoteltype = Hotelproptype::where('hotel_id_fk', '=', $id)->get();
	    $tech = techdetails::where('hotel_id_fk', '=', $id)->first();
       	$Mealtype = Mealtype::all();
        $Hotelmeal = Hotelmeal::where('hotel_id_fk', '=', $id)->get();
        $group = Groupdetails::where('hotel_grp_name', '=' , $id)->get();
        $Extra_bed_details = Extra_bed_details::where('hotel_id_fk', '=', $id)->where('type', '=', 0)->first();
        $Extra_kids_bed_details = Extra_bed_details::where('hotel_id_fk', '=', $id)->where('type', '=', 1)->first();
        $room_details_edit = Room_details::where('hotel_id_fk', '=', $id)->get();
        $room_select = Room_details::select('room_type')->distinct('room_type')->where('hotel_id_fk', '=', $id)->get();
        $rate_details= Rates::where('hotel_id_fk', '=', $id)->get();
        $special_rates = sp_rate::where('hotel_id_fk', '=', $id)->get();
        $hotel = Hotel::where('id', '=', $id)->first();
        $sales= contact::where('hotel_id_fk', '=', $id)->where('hotel_auth_category', '=', 'Sales')->first();
        $reser= contact::where('hotel_id_fk', '=', $id)->where('hotel_auth_category', '=', 'Reservations')->first();
        $account= contact::where('hotel_id_fk', '=', $id)->where('hotel_auth_category', '=', 'Accounts')->first();
       return view('dashboard.hotel.edit',compact('id','Propertyinfo','fhrai','facility','Hotelfacility','Propertydetails','hotelpolicy','bankdetails','authcontact','type','hoteltype','tech','room_aminities_edit','room_type_edit','Hotelroom_amenities','Room_aminity','room_type','Mealtype','Hotelmeal','group','Extra_bed_details','room_details_edit','Extra_kids_bed_details','hotel','sales','reser','account','rate_details','room_select','special_rates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
    public function storePropertyinfo(Request $request) {
           
            $this->validate($request, [
                'prop_name' => 'required',
                'web_address' => 'required',
                'fhrai' => 'required',
                'phone' => 'numeric',
                //'fax' => 'required',
                'email' => 'required',
                
            ]);

            if($request->propertyinfo){

                $id = $request->propertyinfo;
                return $this->updatePropertyinfo($request,$id); 
                
            } else {
                  
            $Property_info = new Propertyinfo;
            
            $Property_info->prop_info_name = $request->prop_name;
            //$Property_info->prop_info_grp_name = $request->group_name;
            //$Property_info->prop_info_chain_name = $request->chain_name;
            $Property_info->prop_info_web_address = $request->web_address; 
            $Property_info->fhrai_class_id = $request->fhrai;
            $Property_info->prop_info_tel_number = $request->phone;
            $Property_info->prop_info_fax_number = $request->fax;
            $Property_info->prop_info_email = $request->email;
            
            $Property_info->hotel_id_fk = $request->id;

            $Property_info->save();
            }
            return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Property Information Added Succesfully'));
        
    }
    public function updatePropertyinfo(Request $request,$id) {
            $Property_info = Propertyinfo::findOrFail($id);
           
            $Property_info->prop_info_name = $request->prop_name;
            //$Property_info->prop_info_grp_name = $request->group_name;
            //$Property_info->prop_info_chain_name = $request->chain_name;
            $Property_info->prop_info_web_address = $request->web_address; 
            $Property_info->fhrai_class_id = $request->fhrai;
            $Property_info->prop_info_tel_number = $request->phone;
            $Property_info->prop_info_fax_number = $request->fax;
            $Property_info->prop_info_email = $request->email;
            
            $Property_info->hotel_id_fk = $request->id;
            
            $Property_info->save();
            return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Property Information Updated Succesfully'));
        
    }
    public function storeHotelfacility(Request $request) {
       
         $Hotelfac = Hotelfacility::where('hotel_id_fk', '=', $request->id)->get();
         foreach ($Hotelfac as $value) {
         $value->delete();
         }
        if($request->hotel_facility) {
        foreach ($request->hotel_facility as $value) {
                $Hotel_facility = new Hotelfacility;
                $Hotel_facility->hotel_id_fk = $request->id;	
                $Hotel_facility->fac_id_fk = $value;

                $Hotel_facility->save();
         }
        }
        return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Hotel facility Updated Succesfully'));
     }
	public function propdetailstore(Request $request){
      
            $this->validate($request, [
                'hotel_details_no_rooms' => 'required',
                'timepicker_in' => 'required',
                'timepicker_out' => 'required',
                'hotel_details_no_restaurants' => 'required',
                'timepicker_from' => 'required',
                'timepicker_to' => 'required',
                
            ]);

            if($request->propertydetails){

                $id = $request->propertydetails;
                return $this->updatepropdetails($request,$id); 
                
            } else {            
            $propertydetails = new hoteldetails;
            
            $propertydetails->hotel_details_no_rooms = $request->hotel_details_no_rooms;
            $propertydetails->hotel_details_checkin_time = $request->timepicker_in;
            $propertydetails->hotel_details_checkout_time = $request->timepicker_out;
            $propertydetails->hotel_details_no_restaurants = $request->hotel_details_no_restaurants; 
            $propertydetails->hotel_details_roomservice_from = $request->timepicker_from;
            $propertydetails->hotel_details_roomservice_to = $request->timepicker_to;
			$propertydetails->hotel_details_bar = $request->hotel_details_bar;
            $propertydetails->hotel_id_fk = $request->id;

            $propertydetails->save();
            }
            return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Property Details Added Succesfully'));
        

	}
	public function updatepropdetails(Request $request,$id)
	{
			$propertydetails = hoteldetails::findOrFail($id);
			$propertydetails->hotel_details_no_rooms = $request->hotel_details_no_rooms;
            $propertydetails->hotel_details_checkin_time = $request->timepicker_in;
            $propertydetails->hotel_details_checkout_time = $request->timepicker_out;
            $propertydetails->hotel_details_no_restaurants = $request->hotel_details_no_restaurants; 
            $propertydetails->hotel_details_roomservice_from = $request->timepicker_from;
            $propertydetails->hotel_details_roomservice_to = $request->timepicker_to;
            $propertydetails->hotel_details_bar = $request->hotel_details_bar;
            $propertydetails->hotel_id_fk = $request->id;
            
            $propertydetails->save();
            return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Property Details Updated Succesfully'));
	}
	public function storepropdetailsdesc(Request $request)
	{
		$this->validate($request,[
			'hotel_details_desc'=>'required'
		]);
		if($request->descdetails)
		{
			$id=$request->descdetails;
			return $this->updatepropdetailsdesc($request,$id);
		} 
		else 
		{
			$propertydetails = new hoteldetails;
			$propertydetails->hotel_details_desc=$request->hotel_details_desc;
			$propertydetails->hotel_id_fk = $request->id;
			$propertydetails->save();
		}
		return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Property Description Added Succesfully'));
	}
	public function updatepropdetailsdesc(Request $request,$id)
	{
		$propertydetails=hoteldetails::findOrFail($id);
		$propertydetails->hotel_details_desc=$request->hotel_details_desc;
		$propertydetails->hotel_id_fk = $request->id;
		$propertydetails->save();
		return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Property Description Updated Succesfully'));
	}
    public function storepropinfoaddress(Request $request)
    {
        $this->validate($request, [
                //'address' => 'required',
            ]);

            if($request->addressinfo){

                $id = $request->addressinfo;
                return $this->updatePropertyinfoaddress($request,$id); 
                
            } else {
             
            $Property_info = new Propertyinfo;
            
            $Property_info->latitude=$request->latitude;
            $Property_info->longitude=$request->longitude;
            $Property_info->hotel_id_fk = $request->id;

            $Property_info->save();
            }
            return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Hotel Address Added Succesfully'));
    }
    public function updatePropertyinfoaddress(Request $request,$id) {
            $Property_info = Propertyinfo::findOrFail($id);
            $id1 = $request->id;
            $hotel = Hotel::findOrFail($id1); 
            
            $Property_info->latitude=$request->latitude;
            $Property_info->longitude=$request->longitude;
            $Property_info->hotel_id_fk = $request->id;
            $hotel->address = $request->address1;
            $Property_info->save();
            $hotel->save();
            return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Hotel Address Updated Succesfully'));
        
    }
	public function storehotelpolicy(Request $request)
	{
		 $this->validate($request, [
                'hotel_policy_noshow' => 'required',
                'hotel_policy_cancellation' => 'required'
		]);
		if($request->policydetails)
		{
			$id=$request->policydetails;
			return $this->updatehotelpolicy($request,$id);
		}
		else
		{
			$propertydetails = new policy;
			$propertydetails->hotel_policy_noshow=$request->hotel_policy_noshow;
			$propertydetails->hotel_policy_cancellation=$request->hotel_policy_cancellation;
			$propertydetails->hotel_id_fk = $request->id;
			$propertydetails->save();
		}
		return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Hotel Policy Updated Succesfully'));
		
	}
	public function updatehotelpolicy(Request $request,$id)
	{
		$propertydetails=policy::findOrFail($id);
		$propertydetails->hotel_policy_noshow=$request->hotel_policy_noshow;
		$propertydetails->hotel_policy_cancellation=$request->hotel_policy_cancellation;
		$propertydetails->hotel_id_fk = $request->id;
		$propertydetails->save();
		return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Hotel Policy Updated Succesfully'));
	}
	public function storebankdetails(Request $request)
	{
		$this->validate($request, [
                'bank_details_acc_name' => 'required',
                'bank_details_acc_number' => 'required',
                'bank_details_bank_name' => 'required',
                'bank_details_branch' => 'required',
                'bank_details_bank_address' => 'required',
				'bank_details_ifsc_code' => 'required',
				
            ]);

            if($request->detailsbank){

                $id = $request->detailsbank;
                return $this->updatebankdetails($request,$id); 
                
            } else {            
            $propertydetails = new bank;
            
            $propertydetails->bank_details_acc_name = $request->bank_details_acc_name;
            $propertydetails->bank_details_acc_number = $request->bank_details_acc_number;
            $propertydetails->bank_details_bank_name = $request->bank_details_bank_name;
            $propertydetails->bank_details_branch = $request->bank_details_branch; 
            $propertydetails->bank_details_bank_address = $request->bank_details_bank_address;
			$propertydetails->bank_details_ifsc_code = $request->bank_details_ifsc_code;
			$propertydetails->bank_details_swift_code = $request->bank_details_swift_code;
            $propertydetails->hotel_id_fk = $request->id;

            $propertydetails->save();
            }
            return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Bank Details Added Succesfully'));
	}
	public function updatebankdetails(Request $request,$id)
	{
			$propertydetails=bank::findOrFail($id);
			$propertydetails->bank_details_acc_name = $request->bank_details_acc_name;
			$propertydetails->bank_details_acc_number = $request->bank_details_acc_number;
            $propertydetails->bank_details_bank_name = $request->bank_details_bank_name;
            $propertydetails->bank_details_branch = $request->bank_details_branch; 
            $propertydetails->bank_details_bank_address = $request->bank_details_bank_address;
			$propertydetails->bank_details_ifsc_code = $request->bank_details_ifsc_code;
			$propertydetails->bank_details_swift_code = $request->bank_details_swift_code;
            $propertydetails->hotel_id_fk = $request->id;
			$propertydetails->save();
			return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Bank Details Updated Succesfully'));
	}
	public function storecontactdetails(Request $request)
	{
        $this->validate($request, [
                'sales_mobile' => 'numeric',
                'reservations_mobile' => 'numeric',
                'accounts_mobile' => 'numeric',

                ]);
		$Hotelcon = contact::where('hotel_id_fk', '=', $request->id)->get();
        foreach ($Hotelcon as $value) {
        $value->delete();
        }  
        if($request->sales_name) {

            $arrtyp1 = array(
                'hotel_id_fk' => $request->id,
                'hotel_auth_name' => $request->sales_name,
                'hotel_auth_category' => $request->sales_category,
                'hotel_auth_designation' => $request->sales_designation,
                'hotel_auth_email' => $request->sales_email,
                'hotel_auth_landline' => $request->sales_landline,
                'hotel_auth_mobile' => $request->sales_mobile
            ) ;
            $data[]=$arrtyp1;
        }
        if($request->reservations_name) {

            $arrtyp2 = array(
                'hotel_id_fk' => $request->id,
                'hotel_auth_name' => $request->reservations_name,
                'hotel_auth_category' => $request->reservations_category,
                'hotel_auth_designation' => $request->reservations_designation,
                'hotel_auth_email' => $request->reservations_email,
                'hotel_auth_landline' => $request->reservations_landline,
                'hotel_auth_mobile' => $request->reservations_mobile
            ) ;
            $data[]=$arrtyp2;
        }
        if($request->accounts_name) {

            $arrtyp3 = array(
                'hotel_id_fk' => $request->id,
                'hotel_auth_name' => $request->accounts_name,
                'hotel_auth_category' => $request->accounts_category,
                'hotel_auth_designation' => $request->accounts_designation,
                'hotel_auth_email' => $request->accounts_email,
                'hotel_auth_landline' => $request->accounts_landline,
                'hotel_auth_mobile' => $request->accounts_mobile
            ) ;
            $data[]=$arrtyp3;
        }                
        contact::insert($data);
        return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans(' Contact Added Succesfully'));
	}
	public function storeHoteltype(Request $request) 
    {
       
        $Hotelprop = Hotelproptype::where('hotel_id_fk', '=', $request->id)->get();
        foreach ($Hotelprop as $value) {
            $value->delete();
        }
        if($request->hotel_type) {
            foreach ($request->hotel_type as $value) {
                $Hotel_property = new Hotelproptype;
                $Hotel_property->hotel_id_fk = $request->id;	
                $Hotel_property->prop_id_fk = $value;
                $Hotel_property->save();
            }
        }
        return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Property Type Updated Succesfully'));
    }
	public function storetechdetails(Request $request)
	{
		

            if($request->technicaldetails){

                $id = $request->technicaldetails;
                return $this->updatetechdetails($request,$id); 
                
            } else {            
            $propertydetails = new techdetails;
            
            $propertydetails->hotel_pms_name = $request->Pms_name;
            $propertydetails->hotel_channel_name = $request->channel_manager;
		    $propertydetails->hotel_id_fk = $request->id;
            $propertydetails->save();
            }
            return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Details Added Succesfully'));
	}
	public function updatetechdetails(Request $request,$id)
	{
			$propertydetails=techdetails::findOrFail($id);
			$propertydetails->hotel_pms_name = $request->Pms_name;
            $propertydetails->hotel_channel_name = $request->channel_manager;
			$propertydetails->hotel_id_fk = $request->id;
			$propertydetails->save();
			return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Detailes Updated Succesfully'));
	}
         public function storeHotelroomamenities(Request $request) {
        
         $Hotelroomtype = Hotel_room_aminities::where('hotel_id_fk', '=', $request->id)->get();
         $Room_details = Room_details::where('hotel_id_fk', '=', $request->id)->get();
         foreach ($Room_details as $value) {
         $value->delete();
         }
         foreach ($Hotelroomtype as $value) {
         $value->delete();
         }
         for($i=0;$i<$request->counter;$i++) {
             
          $str = 'room_type'.$i;
          $room_type = $request->$str;   
          $room_str = 'room_aminities'.$i;
          $room_aminities = $request->$room_str;
	  $rooms = "rooms".$i;
	  $adults = "adults".$i;
	  $kids = "kids".$i;
               $this->validate($request, [
                $str => 'required',
                $room_str => 'required',
                $rooms => 'required',
                $adults => 'required',
                $kids => 'required',
               
            ]);
               
         foreach ($room_aminities as $value) {
                $hotel_roomtype = new Hotel_room_aminities;
                $hotel_roomtype->hotel_id_fk = $request->id;	
                $hotel_roomtype->room_type = $room_type;
                $hotel_roomtype->room_amn_id_fk = $value;	
                $hotel_roomtype->save();
                $room_type_name = $hotel_roomtype->room_type; 
                }
                $room_details = new Room_details;
                $room_details->hotel_id_fk = $request->id;
                $room_details->room_type = $room_type_name;
              
                $room_details->no_of_rooms = $request->$rooms;
               
                $room_details->max_occ_adults = $request->$adults;
              
                $room_details->max_occ_kids = $request->$kids;
                $room_details->save();
                
               
         
         
         } 
        return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Details Updated Succesfully'));
    }
    public function listgroup($id)
    {
        
            $Group = Groupdetails::where('user_id_fk', '=', $id)->get();
            return view('dashboard.hotel_grp.index',compact('Group','id'));
    }
    public function creategroup($id)
    {
            return view('dashboard.hotel_grp.create',compact('id'));
    }
    public function groupstore(Request $request)
    {

        $this->validate($request, [
                'hotel_grp_name' => 'required',
                'hotel_grp_address' => 'required',
                'hotel_grp_contact' => 'required',
         ]);
         $gr = new Groupdetails;
         $gr->hotel_grp_name = $request->hotel_grp_name;
         $gr->hotel_grp_address = $request->hotel_grp_address;
         $gr->hotel_grp_contact = $request->hotel_grp_contact;
         $gr->user_id_fk = $request->group;
         $gr->save();
         
        Session::flash('flash_message', 'Group successfully added!!!!');

        return redirect()->route('hotel.listgroup',$request->group);
    }
    public function editgroup($id)
    {
        $Group= Groupdetails::findOrFail($id);

         return view('dashboard.hotel_grp.edit',compact('Group'));
    }
    public function updategroup(Request $request, $id)
    {
        $group = Groupdetails::findOrFail($id);

        $this->validate($request, [
            'hotel_grp_name' => 'required',
            'hotel_grp_address' => 'required',
            'hotel_grp_contact' => 'required',
        ]);

        $input = $request->all();

        $group->fill($input)->save();
        //Session::flash('flash_message', 'Group successfully updated!');

        return redirect()->back()->withSuccess(trans('Group successfully updated!'));

    }
    public function destroygroup($id)
    {
        $group = Groupdetails::findOrFail($id);

        $group->delete();

        Session::flash('flash_message', 'group successfully deleted!');

        return redirect()->route('hotel.listgroup',$id);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createHotels($id=NULL) {
       return view('dashboard.hotel.createhotels',compact('id'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeHotels(Request $request) {

        $this->validate($request, [
            'hotel_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'image' => 'required',
            
        ]);
            if($request->hoteluser) {
             $user_id_fk =  $request->hoteluser;  
            } else {
             $user_id_fk =  $request->created;    
            }
            $Hotel = new Hotel;
            $Hotel->group_id_fk = $request->group_id;
            $Hotel->user_id_fk = $user_id_fk;
            $Hotel->hotel_name = $request->hotel_name;
            $Hotel->address = $request->address; 
            $Hotel->phone = $request->phone; 
            $Hotel->created_by = $request->created; 
            if($request->hasFile('image'))
            {
                $destinationPath="upload/places";
                $file = $request->file('image');
                $myfile = value(function() use ($file)
                {
                    $filename = str_random(10) . '.' . $file->getClientOriginalExtension();
                    return strtolower($filename);
                });
                $request->file('image')->move($destinationPath,$myfile);
                $Hotel->images=$myfile;
            }
            $Hotel->save();
            $id = $Hotel->id;

        return redirect()->route('dashboard.hotel.edit',$id)
                        ->withSuccess(trans('Hotel Added Succesfully'));
    }
   public function hotels($id=NULL) 
    {
       
        if($id) {
          $hotels = Hotel::where('group_id_fk', '=', $id)->get();
          $group = Groupdetails::findOrFail($id);
        } else {
          $hoteluser = $value = Session::get('hoteluser');    
          $hotels = Hotel::where('user_id_fk', '=', $hoteluser)->get();
          $group = "";
        }
        return view('dashboard.hotel.hotels',compact('hotels','id','group'));
    } 
    public function editHotels($id) 
    {
	   return "here";
    }
    public function destroyhotel($id,Request $request)
    {   
        
        
        $hotel = Hotel::findOrFail($id);

        $hotel->delete();

        return redirect()->back()->withSuccess(trans('Hotel deleted Succesfully'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeExtrabed(Request $request) {

      
            if($request->rate_field==1){
                $this->validate($request, [
                'rate' => 'required',
                     ]);
            }
            if($request->rate_field_kids==1){
                $this->validate($request, [
                'Kids_bed_rate' => 'required',
                     ]);
            }
            
            if(($request->rate_field=="")&&($request->rate_field_exist)) {
            $id = $request->rate_field_exist;
            $bed_details = Extra_bed_details::where('id', '=', $id)->get();
		 foreach ($bed_details as $value) {
		 $value->delete();
		 }
            
	    } else {
            if($request->rate_field_exist) {
               $id = $request->rate_field_exist;
               $bed_details = Extra_bed_details::findOrFail($id); 
               $bed_details->room_rate = $request->rate;
               $bed_details->save();
            } else {
            $Extra_bed = new Extra_bed_details;
            $Extra_bed->hotel_id_fk = $request->id;
            $Extra_bed->room_rate = $request->rate;
            $Extra_bed->type = 0;
            $Extra_bed->save();
            } }
       return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Details Updated Succesfully'));
    }
   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeExtrakidsbed(Request $request) {

            if($request->rate_field_kids==1){
                $this->validate($request, [
                'Kids_bed_rate' => 'required',
                     ]);
            }
            
            if(($request->rate_field_kids=="")&&($request->rate_field_exist_kids)) {
            $id = $request->rate_field_exist_kids;
            $bed_details = Extra_bed_details::where('id', '=', $id)->get();
		 foreach ($bed_details as $value) {
		 $value->delete();
		 }
            
	    } else {
            if($request->rate_field_exist_kids) {
               $id = $request->rate_field_exist_kids;
               $bed_details = Extra_bed_details::findOrFail($id); 
               $bed_details->room_rate = $request->Kids_bed_rate;
               $bed_details->save();
            } else {
            $Extra_bed = new Extra_bed_details;
            $Extra_bed->hotel_id_fk = $request->id;
            $Extra_bed->room_rate = $request->Kids_bed_rate;
            $Extra_bed->type = 1;
            $Extra_bed->save();
            } }
       return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Details Updated Succesfully'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storemeals(Request $request)
    {   

        $meal = Hotelmeal::where('hotel_id_fk', '=', $request->id)->get();
        $Mealtype = Mealtype::all();
        foreach ($meal as $value) {
             $value->delete();
        }
        $meal_t = new Hotelmeal;
        $data = array();
        
        if($request->meal_type_cp) {

            $arrtyp1 = array(
                'hotel_id_fk' => $request->id,
                'meal_id_fk' => '1',
                'freeorpaid' => $request->type_cp,
                'meal_rates' => $request->rate1
            ) ;
            $data[]=$arrtyp1;
        }

        if($request->meal_type_ep) {

            $arrtyp2 = array(
                'hotel_id_fk' => $request->id,
                'meal_id_fk'=> '2',
                'freeorpaid' => $request->type_ep,
                'meal_rates' => $request->rate2
             ); 
             $data[]=$arrtyp2; 
        }

        if($request->meal_type_ap) {
            
            
            $arrtyp3 = array(
                'hotel_id_fk' => $request->id,
                'meal_id_fk' => '3',
                'freeorpaid' => $request->type_ap,
                'meal_rates' => $request->rate3
            );   
            $data[]=$arrtyp3; 
        }

        if($request->meal_type_map) {
            
            $arrtyp4 = array(
                'hotel_id_fk' => $request->id,
                'meal_id_fk' => '4',
                'freeorpaid' => $request->type_map,
                'meal_rates' => $request->rate4
                ) ;  
            $data[]=$arrtyp4; 
        }
          
        Hotelmeal::insert($data);
        return redirect()->route('dashboard.hotel.edit',$request->id)
                        ->withSuccess(trans('Meal Type Updated Succesfully'));
    }
   public function allHotels() 
    {
          if (Auth::user()->hasRole('Hotel')) {
			return $this->userHotels();
		}
          $hotels = Hotel::allHotels();
          return view('dashboard.hotel.allHotels',compact('hotels'));
    }
    public function userHotels() {
          
          $user = Auth::user()->id; 
          $id = (int) $user;
          $hotels = Hotel::where('hotels.user_id_fk', $id)
               ->leftJoin('hotel_group_details', 'hotel_group_details.id', '=', 'hotels.group_id_fk')
               ->select('hotels.*','hotel_group_details.hotel_grp_name')
               ->get();
         
          return view('dashboard.hotel.allHotels',compact('hotels')); 
    }
    public function allGroups()
    {       if (Auth::user()->hasRole('Hotel')) {
			return $this->userGroups();
		}
            $Groups = Hotel::allGroups();
            return view('dashboard.hotel_grp.allGroups',compact('Groups'));
    }
    public function userGroups() {
          
          $user = Auth::user()->id; 
          $id = (int)$user;
          $Groups = Groupdetails::where('hotel_group_details.user_id_fk', $id)
               ->join('users', 'users.id', '=', 'hotel_group_details.user_id_fk')
               ->select('hotel_group_details.*','users.first_name')
               ->get();
          return view('dashboard.hotel_grp.allGroups',compact('Groups'));
    }
    
    
     public function rateCalandar() 
    {
         $id = Auth::user()->id; 
         if($id==1) {
         $hotels = Hotel::allHotels();
         } else {
         $hotels = Hotel::where('user_id_fk', '=', $id)->get();
         }
         return view('dashboard.hotel.calandar',compact('hotels'));
         
    }
    public function rateinCalandar($id) 
    {    
        return view('dashboard.hotel.rateCalandar',compact('id'));
    }
    public function rateCalandarDetails(Request $request) {
        $type = $request->type;
        $hotel = $request->id; 
        if ($type == 'fetch') {
            $events = array();
            $rates = Rates::where('hotel_id_fk', '=', $hotel)->get()->toArray();
            $special_rates = sp_rate::where('hotel_id_fk', '=', $hotel)->get()->toArray();
            $details = array_merge($rates, $special_rates);
            foreach ($details as $fetch) {
                $e = array();
                $e['id'] = $fetch['id'];
                $e['title'] = $fetch['room_type'] . " -" . $fetch['rate'];
                $from = date_create($fetch['from_date']);
                $to = date_create($fetch['to_date']);
                $diff = date_diff($from, $to);
                $count = $diff->format("%a");
                if ($count > 0) {

                    $strDateFrom = $fetch['from_date'];
                    $strDateTo = $fetch['to_date'];

                    $aryRange = array();

                    $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
                    $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

                    if ($iDateTo >= $iDateFrom) {
                        array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
                        while ($iDateFrom < $iDateTo) {
                            $iDateFrom+=86400; // add 24 hours
                            array_push($aryRange, date('Y-m-d', $iDateFrom));
                        }
                    }
                    for ($i = 0; $i < count($aryRange); $i++) {
                        $e['start'] = $aryRange[$i];
                        $e['end'] = $aryRange[$i];
                        $allday = true;
                        $e['allDay'] = $allday;
                        array_push($events, $e);
                    }
                } else {
                    $e['start'] = $fetch['from_date'];
                    $e['end'] = $fetch['to_date'];

                    $allday = true;
                    $e['allDay'] = $allday;

                    array_push($events, $e);
                }
            }

            echo json_encode($events);
        }
    }
    public function availabilityCalandar() 
    {
         $id = Auth::user()->id; 
         if($id==1) {
         $hotels = Hotel::allHotels();
         } else {
         $hotels = Hotel::where('user_id_fk', '=', $id)->get();
         }
         return view('dashboard.hotel.calandarAvailability',compact('hotels'));
         
    }
    public function availabilityinCalandar($id) 
    {    
        return view('dashboard.hotel.availabilityCalandar',compact('id'));
    }
   public function availabilityCalandarDetails(Request $request) {
        $type = $request->type;
        $hotel = $request->id; 
        if ($type == 'fetch') {
            $events = array();
            $rates = Available::where('availabilities.hotel_id_fk', $hotel)
               ->groupBy('availabilities.id')     
               ->join('rates', 'rates.hotel_id_fk', '=', 'availabilities.hotel_id_fk') 
               ->select('rates.from_date','rates.to_date','availabilities.number','availabilities.room_type')
               ->get();
           
            $details = $rates;
            foreach ($details as $fetch) {
                $e = array();
                $e['id'] = $fetch['id'];
                $e['title'] = $fetch['room_type'] . " -" . $fetch['number'];
                $from = date_create($fetch['from_date']);
                $to = date_create($fetch['to_date']);
                $diff = date_diff($from, $to);
                $count = $diff->format("%a");
                if ($count > 0) {

                    $strDateFrom = $fetch['from_date'];
                    $strDateTo = $fetch['to_date'];

                    $aryRange = array();

                    $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
                    $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

                    if ($iDateTo >= $iDateFrom) {
                        array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
                        while ($iDateFrom < $iDateTo) {
                            $iDateFrom+=86400; // add 24 hours
                            array_push($aryRange, date('Y-m-d', $iDateFrom));
                        }
                    }
                    for ($i = 0; $i < count($aryRange); $i++) {
                        $e['start'] = $aryRange[$i];
                        $e['end'] = $aryRange[$i];
                        $allday = true;
                        $e['allDay'] = $allday;
                        array_push($events, $e);
                    }
                } else {
                    $e['start'] = $fetch['from_date'];
                    $e['end'] = $fetch['to_date'];

                    $allday = true;
                    $e['allDay'] = $allday;

                    array_push($events, $e);
                }
            }

            echo json_encode($events);
        }
    }


}
