<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Session\SessionRepository;
use App\Repositories\User\UserRepository;
use Auth;
use Input;
use App\Hotel as Hotel;
use App\Room_details as Room_details;
use App\Availability as Available;

class AvailabilityController extends Controller
{
    private $users;

    /**
     * UsersController constructor.
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users) {
        $this->middleware('auth');
        $this->middleware('session.database', ['only' => ['sessions', 'invalidateSession']]);
        $this->users = $users;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (Auth::user()->hasRole('Hotel')) {
        $id = Auth::user()->id;

        $hotel = Hotel::where('user_id_fk', '=', $id)->get();

        return view('dashboard.availability.index',compact('hotel'));
    }
    else{
        $hotel = Hotel::all();
        return view('dashboard.availability.index',compact('hotel'));
    }
    }
    
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $Avail=Available::where('hotel_id_fk', '=', $request->hotel_name)->get();
        foreach ($Avail as $value) {
         $value->delete();
         }
       
        $count=count($request->room_type);
        for($i=0;$i<$count;$i++)
        {
            $str = 'number'.$i;
          $num = $request->$str;   
            
         $this->validate($request, [
            $str => 'in:0,1,2,3,4,5,6,7,8,9,10',
        ]);
        
      $availability = new Available;
            $availability->hotel_id_fk=$request->hotel_name;
            $availability->room_type=$request->room_type[$i];
            $availability->number=$num;
            $availability->from_date=$request->datepicker_from[$i];
            $availability->to_date=$request->datepicker_to[$i];
            $availability->save();
        
        }

        return redirect()->route('availability.index')
                        ->withSuccess(trans('Details Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
