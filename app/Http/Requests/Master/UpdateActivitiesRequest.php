<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;
use App\Activities;

class UpdateActivitiesRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'activity' => 'required|min:3',
			'description' => 'required|min:6',
		    ];
	}
}
