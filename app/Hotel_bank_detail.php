<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel_bank_detail extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'hotel_bank_details';
}
