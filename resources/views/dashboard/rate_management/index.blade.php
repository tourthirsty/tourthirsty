@extends('dashboard.layouts.master')

@section('page-title')

@section('page-header')
<meta name="_token" content="{!! csrf_token() !!}"/>
<!--@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif-->
    <h1>
       Rate
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li class="active">Rate</li>
    </ol>
@endsection

@section('content')

@include('partials.messages')


@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

<div class="row">
    <div class="col-md-12">
        
        {!! Form::open(['route' => ['rate.storecaldetails'], 'method' => 'post', 'id' => 'tech-details-form']) !!}
                    <div class="panel panel-default">
                        <div  class="panel-heading">Room Rates
                        <div style="float:right;">
                        <a href="{{ route('hotel.rateCalandar') }}">Calendar View</a>
                        </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="hotel_name"> Hotel</label>
                                        <select  name="hotel_name" class="form-control" id="hotel_name">
                                            <option value="">Select</option>
                                                <?php foreach($hotel as $r => $value){ ?>
                                                    <option value="<?php echo $value['id']; ?>"><?php echo $value['hotel_name']; ?></option>   
                                                <?php } ?>
                                        </select> 
                                        
                                    </div>

                                    <div class="form-group">
                                        <label for="room_type">Room Type</label>
                                        <select name="room_type" class="form-control" id="room_type">
                                             <option value=""></option>
                                        </select>                         
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                <label for="datepicker_from">From</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepicker1'>
                                           
                                            <input type="text" class="form-control"  id="datepicker_from" name="datepicker_from" placeholder="From" >
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                 <label for="datepicker_to">To</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepicker2'>
                                            
                                            <input type="text" class="form-control"  id="datepicker_to" name="datepicker_to" placeholder="To" >
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label for="normal">
                                                <input type="radio"  name="normal1" id= "normal" value="1" >
                                                        Normal Rate
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label for="special">
                                                <input type="radio"  name="normal1" id= "special" value="2" >
                                                        Special Rate
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="rates">Rate</label>
                                            <input type="text" class="form-control" id="room_type_rates" name="room_type_rates" placeholder="" value="">
                                    </div>
                                </div>
                            </div>
                            <br><br>
                           
                            @include('dashboard.rate_management.partials.extrabed')
                                
                           
                                @include('dashboard.rate_management.partials.extrakidsbed')
                             
                            <button  type="submit" class="btn btn-primary" id="update-login-details-btn" >
                                <i class="fa fa-refresh"></i>
                                    Update Details
                            </button>
                           
                            <div class="row" style="padding-top: 24px;">
                                <div class="col-md-10">
                                    <div class ="box-body table-responsive no-padding">
                                        <table class="table table-hover table-bordered table-striped">
                                            <thead>
                                                <tr><h4><b>Normal Rates</b></h4></tr>
                                                <th>Hotel Name</th>
                                                <th>Room Type</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Rate</th>
                                                <th>Extra Bed Rate</th>
                                                <th>Kids Extra Bed Rate</th>
                                                <th style="border-right-width:1px;"></th>
                                            </thead>
                                            <tbody id="test">
                                            </tbody>
                                        </table>
                                    </div>
                                
                                <br>
                               
                                    <div class ="box-body table-responsive no-padding">
                                        <table class="table table-hover table-bordered ">
                                            <thead>
                                                <tr><h4><b>Special Rates</b></h4></tr>
                                                <th>Hotel Name</th>
                                                <th>Room Type</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Rate</th>
                                                <th>Extra Bed Rate</th>
                                                <th>Kids Extra Bed Rate</th>
                                                <th style="border-right-width:1px;"></th>
                                            </thead>
                                            <tbody id="test1">
                                            </tbody>
                                        </table>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div> 
                    </div>
                    {!! Form::close() !!}
                </div>
            
        
        @stop
        @section('after-scripts-end')

<link href="{{ URL::asset('assets/css/bootstrap.css')}}" rel="stylesheet">

    



<script>
    function getrates(id) {
    if(id==1){
       if(document.getElementById("extra_bed").checked == true) {
       $("#rate").show();
       $("#rate_field").val(1);
    } else {
       $("#rate").hide(); 
       $("#rate_field").val("");
    } 
    } else {
        
    if(document.getElementById("extra_bed_kids").checked == true) {
       $("#Kids_bed_rate").show();
       $("#rate_field_kids").val(1);
    } else {
       $("#Kids_bed_rate").hide(); 
       $("#rate_field_kids").val("");
    }
    }
}
</script>



<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                    format: 'YYYY-MM-DD',
                });
            });
</script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    format: 'YYYY-MM-DD',
                });
            });
</script>


<script type="text/javascript">
      //alert("Please select a hotel "); 
    $('#hotel_name').change(function(){
        var hotel_name = $('#hotel_name').val();
       
        
            
            alert("Please select a room type ");
            
        
        
            $.ajax({
                url     : '/public/en/dashboard/getname',
                type    : 'GET',
                data    : {hotel_name:hotel_name},
                success : function(data) {


                        $('#room_type') .find('option') .remove() .end();
                        var option = '';
                        var length1 = '';
                        var length2 = '';
                        var length3 = '';

                        option += '<option value="">Select</option>';
                        var options = $.map(data, function(el) { return el }) ;
                        var length1 = options.length;
                        

                       
                        for(var j = 0; j < length1; j++) 
                        {
                         option += '<option value="' + options[j].id + '" > ' + options[j].room_type + '</option>';
                        }

                        

                        $("select#room_type").html(option);
                        
                    }
                
            },"json");
        
    });
       
</script>

<script type="text/javascript">
       
    $('#room_type').change(function(){
        var room_name = $('#room_type').val();
        var hotel_name = $('#hotel_name').val();
        if(room_name=="")
        {
            alert("Please select a room type ");
        }
        else
        {
            $.ajax({
                url     : '/public/en/dashboard/getroom',
                type    : 'GET',
                data    : {rooms:room_name,hotels:hotel_name},
                success : function(data) {
                    var op='';
                    var op = $.map(data, function(el) { return el }) ;
                    
                    var table1='';
                        var table2='';
                        var table3='';
                        var table4='';
                        
                        if(op[0]['Rates'].length)
                        {
                          var length2 = op[0]['Rates'].length;
                          for(var j = 0; j < length2; j++) 
                          {
                            var table2= op[0]['Rates'][j].id; 

                            table1 += '<tr>';
                            table1 += '<td> '+op[0]['Rates'][j].hotel_name+' </td>';
                            table1 += '<td> '+op[0]['Rates'][j].room_type+' </td>';
                            table1 += '<td> '+op[0]['Rates'][j].from_date+' </td>';
                            table1 += '<td> '+op[0]['Rates'][j].to_date+' </td>';
                            table1 += '<td> '+op[0]['Rates'][j].rate+' </td>';
                            table1 += '<td> '+op[0]['Rates'][j].adult_bed+' </td>';
                            table1 += '<td> '+op[0]['Rates'][j].kids_bed+' </td>';
                            
                            
                            table1 += '<td class="text-center">';
                            table1 += '<a href="norcaledit/'+table2+'" class="btn btn-primary btn-circle edit" title="Edit Rate" data-toggle="tooltip" data-placement="top" style="border-radius:17px;padding:6px;border-radius:15px;text-align:center;font-size: 12px; line-height: 1.428571429;"><i class="glyphicon glyphicon-edit"></i></a>';
                            table1 += '<a href="norcaldestroy/'+table2+'" class="btn btn-danger btn-circle" title="Delete Rate"data-toggle="tooltip" data-placement="top" data-method="DELETE" data-confirm-title="Please Confirm" data-confirm-text="Are you sure that you want to delete ? data-confirm-delete="Yes Delete" style="border-radius:17px;padding:6px; border-radius:15px;text-align:center;font-size: 12px; line-height: 1.428571429;"><i class="glyphicon glyphicon-trash"></i></a>';
                            table1 += '</td>';
                            table1 += '</tr>';

                          }

                        }
                        else
                        {
                            table1 += '<tr>';
                            table1 += '<td colspan="10"><em>No records found</em></td>';
                            table1 += '</tr>'; 
                        }

                        if(op[0]['Sp_rates'].length)
                        {
                          var length3 = op[0]['Sp_rates'].length;
                          for(var j = 0; j < length3; j++) 
                          {
                            var table4= op[0]['Sp_rates'][j].id; 
                            table3 += '<tr>';
                            table3 += '<td> '+op[0]['Sp_rates'][j].hotel_name+' </td>';
                            table3 += '<td> '+op[0]['Sp_rates'][j].room_type+' </td>';
                            table3 += '<td> '+op[0]['Sp_rates'][j].from_date+' </td>';
                            table3 += '<td> '+op[0]['Sp_rates'][j].to_date+' </td>';
                            table3 += '<td> '+op[0]['Sp_rates'][j].rate+' </td>';
                            table3 += '<td> '+op[0]['Sp_rates'][j].adult_bed+' </td>';
                            table3 += '<td> '+op[0]['Sp_rates'][j].kids_bed+' </td>';
                            table3 += '<td class="text-center">';
                            table3 += '<a href="speccaledit/'+table4 +'" class="btn btn-primary btn-circle edit" title="Edit Rate" data-toggle="tooltip" data-placement="top" style="border-radius:17px;padding:6px;border-radius:15px;text-align:center;font-size: 12px; line-height: 1.428571429;"><i class="glyphicon glyphicon-edit"></i></a>';
                            table3 += '<a href="speccaldestroy/'+ table4 +'" class="btn btn-danger btn-circle" title="Delete Rate"data-toggle="tooltip" data-placement="top" data-method="DELETE" data-confirm-title="Please Confirm" data-confirm-text="Are you sure that you want to delete ? data-confirm-delete="Yes Delete" style="border-radius:17px;padding:6px; border-radius:15px;text-align:center;font-size: 12px; line-height: 1.428571429;"><i class="glyphicon glyphicon-trash"></i></a>';
                            table3 += '</td>';
                            table3 += '</tr>';

                          }

                        }
                        else
                        {
                            table3 += '<tr>';
                            table3 += '<td colspan="10"><em>No records found</em></td>';
                            table3 += '</tr>'; 
                        }
                        $("tbody#test").html('');
                        $("tbody#test1").html('');
                        $("tbody#test").append(table1);
                        $("tbody#test1").append(table3);
                    }
                
            },"json");
        }
    });
</script>

<script type="text/javascript">
$.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>
@stop




