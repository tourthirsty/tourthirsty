@extends('dashboard.layouts.master')

@section('page-title')

@section('page-header')
<link href="{{ URL::asset('assets/css/bootstrap.css')}}" rel="stylesheet">
    <h1>
       Availability
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <!--<li><a href="{{ route('availability.index') }}">Availability</a></li>-->
        <li class="active">Availability</li>
      </ol>
@endsection

@section('content')
@include('partials.messages')

{!! Form::open(['route' => ['availability.store'], 'method' => 'post', 'id' => 'tech-details-form']) !!}

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Locations</div>
            <div class="panel-body">
                <div class="row"> 
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="hotel_name">Hotel</label>

                        <select  name="hotel_name" class="form-control" id="hotel_name">
                            <option value="">Select</option>

                            <?php foreach($hotel as $r => $value){ ?>
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['hotel_name']; ?></option>   
                            <?php } ?>
                        </select> 

                    </div>
                    
                    <br>
                    
                    <div class="form-group col-sm-2" id="room"></div>
                    <div class="form-group col-sm-2" id="from"></div>
                    <div class="form-group col-sm-2" id="to"></div>
                    <div class="form-group col-sm-4" id="number"></div>
                    
                    </div>
                </div>
                <button  type="submit" class="btn btn-primary" id="update-login-details-btn">
                    <i class="fa fa-refresh"></i>
                        Update Details
                </button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
        
        @stop
        @section('after-scripts-end')



   

 <script type="text/javascript">
       
    $('#hotel_name').change(function(){
        var hotel_name = $('#hotel_name').val();

        if(hotel_name=="")
        {
            alert("Please select a hotel ");
        }
        else
        {
            $.ajax({
                url     : '/en/dashboard/getcount',
                type    : 'GET',
                data    : {hotel_name:hotel_name},
                success : function(data) {
                     
                    var options = $.map(data, function(el) { return el }) ;
                    
                    /*$('#room_type') .find('option') .remove() .end();
                        var option = '';
                        option += '<option value="">Select</option>';
                        var options = $.map(data, function(el) { return el }) ;
                        alert(options);*/
                         var length = options.length;
                         if(length==0)
                         {
                            
                                alert("No Room Types");
                         }
                         else
                        {
                        var room = '';
                        var number = '';
                        var from='';
                        var to='';
                        for(var j = 0; j < length; j++) {

                            
                            room += '<label for="room_type">Room Type</label>';
                            room +='<input type="text" name="room_type[]" class="form-control" value="' + options[j].room_type +'">';
                            room +='<br><br>';

                            from +='<label for="datepicker_from">From</label>';
                            from +='<div class="input-group date datetimepicker" class="">';
                            from +='<input type="text" class="form-control"  id="datepicker_from" name="datepicker_from[]" placeholder="From" value="'+ options[j].from_date +'" >';
                            from +='<span class="input-group-addon">';
                            from +='<span class="glyphicon glyphicon-calendar"></span>';
                            from +='</span>';
                            from +='</div>';
                            from +='</div>';
                            from +='<br><br>';

                            to +='<label for="to">To</label>';
                            to +='<div class="input-group date datetimepicker2">';
                            to +='<input type="text" class="form-control" id="datepicker_to" name="datepicker_to[]" placeholder="From" value="'+ options[j].to_date +'" >';
                            to +='<span class="input-group-addon">';
                            to +='<span class="glyphicon glyphicon-calendar"></span>';
                            to +='</span>';
                            to +='</div>';
                            to +='</div>';
                            to +='<br><br>';
                            
                            number += '<label for="number">Available Rooms(only upto 10 Rooms)</label>';
                            number += '<input type="text" name="number'+j+'" class="form-control" placeholder="upto 10 rooms" value="'+ options[j].number +'">';
                            number +='<br><br>'; 
                            

                            $("#room").html(room);
                            $("#from").html(from);
                            $("#to").html(to);
                            $("#number").html(number);
                            $('.datetimepicker').datetimepicker({
                                format: 'YYYY-MM-DD',
                            });
                            $('.datetimepicker2').datetimepicker({
                                format: 'YYYY-MM-DD',
                            });
                        }
                    }
                }
                
            },"json");
        }
   });
       
</script> 

{!! JsValidator::formRequest('App\Http\Requests\availability\CreateRequest', '#hotel-form') !!}
@stop

