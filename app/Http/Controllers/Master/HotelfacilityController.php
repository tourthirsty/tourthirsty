<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Session;
use App\Hotelfacility as Hotelfacility;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HotelfacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Hotelfacility = Hotelfacility::all();

        return view('master.hotel_facilities.index',compact('Hotelfacility'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.hotel_facilities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'hotel_facility' => 'required',
            'hotel_facility_desc' => 'required'
        ]);

        
        
        $hotel_fac = new Hotelfacility;

        $hotel_fac->hotel_fac_name = $request->hotel_facility;
        $hotel_fac->hotel_fac_desc = $request->hotel_facility_desc;

        $hotel_fac->save();
        
        return redirect()->route('master.hotelFacility')
                        ->withSuccess(trans('Hotel Facility Added Succesfully'));
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $hotel_fac = Hotelfacility::findOrFail($id);

         return view('master.hotel_facilities.edit',compact('hotel_fac'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hotel_fac = Hotelfacility::findOrFail($id);
        $this->validate($request, [
            'hotel_facility' => 'required',
            'hotel_facility_desc' => 'required'
        ]);

        $hotel_fac->hotel_fac_name = $request->hotel_facility;
        $hotel_fac->hotel_fac_desc = $request->hotel_facility_desc;

        $hotel_fac->save();
        
        return redirect()->route('master.hotelFacility')
                        ->withSuccess(trans('Hotel Facility Updated Succesfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotel_fac = Hotelfacility::findOrFail($id);

        $hotel_fac->delete();

        return redirect()->route('master.hotelFacility')
                        ->withSuccess(trans('Hotel Facility Deleted Succesfully'));
    }
}
