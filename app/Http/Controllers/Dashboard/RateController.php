<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Hotel as Hotel;
use App\User as User;
use Session;
use Input;
use Auth;
use DB;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Session\SessionRepository;
use App\Repositories\User\UserRepository;
use App\Room_details as Room_details;
use App\rate as Rates;
use App\Special_rate as sp_rate;
use App\Extra_bed_details as Extra_bed_details;

class RateController extends Controller
{
    private $users;

    /**
     * UsersController constructor.
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users) 
    {
        $this->middleware('auth');
        $this->middleware('session.database', ['only' => ['sessions', 'invalidateSession']]);
        $this->users = $users;
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->hasRole('Hotel'))
        {
            $id = Auth::user()->id;
            $hotel = Hotel::where('user_id_fk', '=', $id)->get();
            return view('dashboard.rate_management.index',compact('hotel'));
        }
        else
        {
            $hotel = Hotel::all();
            return view('dashboard.rate_management.index',compact('hotel'));
        }
        
    }
    public function storecaldetails(Request $request)
    {
        $id = Auth::user()->id;
        $this->validate($request, [
                'room_type' => 'required',
                'datepicker_from' => 'required',
                'datepicker_to' => 'required',
                'normal1' => 'required',
                'room_type_rates' => 'required',
        ]);

        
        if($request->normal1 == 1)
        {
            if($request->extra_bed == 1 && $request->extra_bed_kids == 2)
            { 
                $normaldetails = new Rates;
                $normaldetails->rate_type = $request->normal1;
                $normaldetails->room_type = $request->room_type;    
                $normaldetails->from_date = $request->datepicker_from;
                $normaldetails->to_date = $request->datepicker_to;
                $normaldetails->rate = $request->room_type_rates;
                $normaldetails->hotel_id_fk = $request->hotel_name;
                $normaldetails->adult_bed = $request->rate;
                $normaldetails->kids_bed = $request->Kids_bed_rate;
                $normaldetails->save();
            }
            else if($request->extra_bed == 1 && $request->extra_bed_kids == '')
            {
                $normaldetails = new Rates;
                $normaldetails->rate_type = $request->normal1;
                $normaldetails->room_type = $request->room_type;    
                $normaldetails->from_date = $request->datepicker_from;
                $normaldetails->to_date = $request->datepicker_to;
                $normaldetails->rate = $request->room_type_rates;
                $normaldetails->hotel_id_fk = $request->hotel_name;
                $normaldetails->adult_bed = $request->rate; 
                $normaldetails->save(); 
            }
            else if($request->extra_bed == '' && $request->extra_bed_kids == 2)
            {
                $normaldetails = new Rates;
                $normaldetails->rate_type = $request->normal1;
                $normaldetails->room_type = $request->room_type;    
                $normaldetails->from_date = $request->datepicker_from;
                $normaldetails->to_date = $request->datepicker_to;
                $normaldetails->rate = $request->room_type_rates;
                $normaldetails->hotel_id_fk = $request->hotel_name;
                $normaldetails->kids_bed = $request->Kids_bed_rate;
                $normaldetails->save();
            }
            else if($request->extra_bed == '' && $request->extra_bed_kids == '')
            {
                $normaldetails = new Rates;
                $normaldetails->rate_type = $request->normal1;
                $normaldetails->room_type = $request->room_type;    
                $normaldetails->from_date = $request->datepicker_from;
                $normaldetails->to_date = $request->datepicker_to;
                $normaldetails->rate = $request->room_type_rates;
                $normaldetails->hotel_id_fk = $request->hotel_name;
                $normaldetails->save();
            }
        }
        else
        {
            if($request->extra_bed == 1 && $request->extra_bed_kids == 2)
            { 
                $specialdetails = new sp_rate;
                $specialdetails->rate_type = $request->normal1; 
                $specialdetails->room_type = $request->room_type;    
                $specialdetails->from_date = $request->datepicker_from;
                $specialdetails->to_date = $request->datepicker_to;
                $specialdetails->rate = $request->room_type_rates;
                $specialdetails->hotel_id_fk = $request->hotel_name;
                $specialdetails->adult_bed = $request->rate;
                $specialdetails->kids_bed = $request->Kids_bed_rate;
                $specialdetails->save();
            }
            else if($request->extra_bed == 1 && $request->extra_bed_kids == '')
            { 
                $specialdetails = new sp_rate;
                $specialdetails->rate_type = $request->normal1; 
                $specialdetails->room_type = $request->room_type;    
                $specialdetails->from_date = $request->datepicker_from;
                $specialdetails->to_date = $request->datepicker_to;
                $specialdetails->rate = $request->room_type_rates;
                $specialdetails->hotel_id_fk = $request->hotel_name;
                $specialdetails->adult_bed = $request->rate; 
                $specialdetails->save();
            
            }
            else if($request->extra_bed == '' && $request->extra_bed_kids == 2)
            { 
                $specialdetails = new sp_rate;
                $specialdetails->rate_type = $request->normal1; 
                $specialdetails->room_type = $request->room_type;    
                $specialdetails->from_date = $request->datepicker_from;
                $specialdetails->to_date = $request->datepicker_to;
                $specialdetails->rate = $request->room_type_rates;
                $specialdetails->hotel_id_fk = $request->hotel_name;
                $specialdetails->kids_bed = $request->Kids_bed_rate;
                $specialdetails->save();
            
            }
            else if($request->extra_bed == '' && $request->extra_bed_kids == '')
            { 
                $specialdetails = new sp_rate;
                $specialdetails->rate_type = $request->normal1; 
                $specialdetails->room_type = $request->room_type;    
                $specialdetails->from_date = $request->datepicker_from;
                $specialdetails->to_date = $request->datepicker_to;
                $specialdetails->rate = $request->room_type_rates;
                $specialdetails->hotel_id_fk = $request->hotel_name;
                $specialdetails->save();
            
            }

        }
        return redirect()->route('rate.index')
                    ->withSuccess(trans('Details Added Succesfully'));
    }
   
    public function norcaldestroy(Request $request,$id)
    {
        
        $cal = Rates::findOrFail($id);
        $cal->delete();
        Session::flash('flash_message', 'Task successfully deleted!');
        return redirect()->back();
    }
    public function speccaldestroy(Request $request,$id)
    {
        
        $sp = sp_rate::findOrFail($id);
        $sp->delete();
        Session::flash('flash_message', 'Task successfully deleted!');
        return redirect()->back();
    }
    public function norcaledit(Request $request,$id)
    {
        $user_id = Auth::user()->id;
        $prop1 = Rates::findOrFail($id);
        $hotel = Hotel::where('user_id_fk', '=', $user_id)->get();
        return view('dashboard.rate_management.norrateedit',compact('prop1','hotel'));
    }
    public function speccaledit(Request $request,$id)
    {
        $user_id = Auth::user()->id;
        $prop2 = sp_rate::findOrFail($id);
        $hotel = Hotel::where('user_id_fk', '=', $user_id)->get();
         
         return view('dashboard.rate_management.specrateedit',compact('prop2','hotel'));
    }
    public function norcalupdate(Request $request,$id)
    {
        $prop = Rates::findOrFail($id);
        $this->validate($request, [
               
                'datepicker_from' => 'required',
                'datepicker_to' => 'required',
                'room_type_rates' => 'required',
           ]);

        if($request->extra_bed == 1 && $request->extra_bed_kids==2)
        {
            $prop->rate_type = $request->normal1;
            $prop->room_type = $request->room_type;    
            $prop->from_date = $request->datepicker_from;
            $prop->to_date = $request->datepicker_to;
            $prop->rate = $request->room_type_rates;
            $prop->hotel_id_fk = $request->hotel_name;
            $prop->adult_bed = $request->rate;
            $prop->kids_bed = $request->Kids_bed_rate;
            $prop->save();  
        }
        else if($request->extra_bed == '' && $request->extra_bed_kids == 2)
        {
            $prop->rate_type = $request->normal1;
            $prop->room_type = $request->room_type;    
            $prop->from_date = $request->datepicker_from;
            $prop->to_date = $request->datepicker_to;
            $prop->rate = $request->room_type_rates;
            $prop->hotel_id_fk = $request->hotel_name;
            $prop->adult_bed = '';
            $prop->kids_bed = $request->Kids_bed_rate;
            $prop->save();
        }
        else if($request->extra_bed == 1 && $request->extra_bed_kids == '')
        {
            $prop->rate_type = $request->normal1;
            $prop->room_type = $request->room_type;    
            $prop->from_date = $request->datepicker_from;
            $prop->to_date = $request->datepicker_to;
            $prop->rate = $request->room_type_rates;
            $prop->hotel_id_fk = $request->hotel_name;
            $prop->adult_bed = $request->rate;
            $prop->kids_bed = '';
            $prop->save();
        }
        else if($request->extra_bed == '' && $request->extra_bed_kids == '')
        {
            $prop->rate_type = $request->normal1;
            $prop->room_type = $request->room_type;    
            $prop->from_date = $request->datepicker_from;
            $prop->to_date = $request->datepicker_to;
            $prop->rate = $request->room_type_rates;
            $prop->hotel_id_fk = $request->hotel_name;
            $prop->adult_bed = '';
            $prop->kids_bed = '';
            $prop->save();
        }

        return redirect()->route('rate.index')
                    ->withSuccess(trans('Details Updated Succesfully'));
    }
    public function speccalupdate(Request $request,$id)
    {

        $prop = sp_rate::findOrFail($id);

        $this->validate($request, [
                'datepicker_from' => 'required',
                'datepicker_to' => 'required',
                'room_type_rates' => 'required',
           ]);

        if($request->extra_bed == 1 && $request->extra_bed_kids==2)
        {
            $prop->rate_type = $request->normal1;
            $prop->room_type = $request->room_type;    
            $prop->from_date = $request->datepicker_from;
            $prop->to_date = $request->datepicker_to;
            $prop->rate = $request->room_type_rates;
            $prop->hotel_id_fk = $request->hotel_name;
            $prop->adult_bed = $request->rate;
            $prop->kids_bed = $request->Kids_bed_rate;
            $prop->save();  
        }
        else if($request->extra_bed == '' && $request->extra_bed_kids == 2)
        {
            $prop->rate_type = $request->normal1;
            $prop->room_type = $request->room_type;    
            $prop->from_date = $request->datepicker_from;
            $prop->to_date = $request->datepicker_to;
            $prop->rate = $request->room_type_rates;
            $prop->hotel_id_fk = $request->hotel_name;
            $prop->adult_bed = '';
            $prop->kids_bed = $request->Kids_bed_rate;
            $prop->save();
        }
        else if($request->extra_bed == 1 && $request->extra_bed_kids == '')
        {
            $prop->rate_type = $request->normal1;
            $prop->room_type = $request->room_type;    
            $prop->from_date = $request->datepicker_from;
            $prop->to_date = $request->datepicker_to;
            $prop->rate = $request->room_type_rates;
            $prop->hotel_id_fk = $request->hotel_name;
            $prop->adult_bed = $request->rate;
            $prop->kids_bed = '';
            $prop->save();
        }
        else if($request->extra_bed == '' && $request->extra_bed_kids == '')
        {
            $prop->rate_type = $request->normal1;
            $prop->room_type = $request->room_type;    
            $prop->from_date = $request->datepicker_from;
            $prop->to_date = $request->datepicker_to;
            $prop->rate = $request->room_type_rates;
            $prop->hotel_id_fk = $request->hotel_name;
            $prop->adult_bed = '';
            $prop->kids_bed = '';
            $prop->save();
        }

        return redirect()->route('rate.index')
                    ->withSuccess(trans('Details Updated Succesfully'));
    }

}
