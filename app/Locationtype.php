<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locationtype extends Model {
	protected $table = 'locationtypes';
        protected $fillable = array(
			'loc_type_name',
			'loc_type_desc');
	
}
