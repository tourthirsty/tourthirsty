<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use Session;

use App\Http\Requests;

use App\Room_type;

use App\Http\Controllers\Controller;

class RoomtypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prop = Room_type::all();
		return view('dashboard.masters.room_type.list',compact('prop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.masters.room_type.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
				'room_type_name' => 'required',
				'room_type_desc' => 'required'
				]);
		$input=$request->all();
		Room_type::create($input);
		Session::flash('flash_message', 'Room Type successfully added!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prop = Room_type::findOrFail($id);

         return view('dashboard.masters.room_type.edit',compact('prop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prop = Room_type::findOrFail($id);

        $this->validate($request, [
            'room_type_name' => 'required',
            'room_type_desc' => 'required'
        ]);

        $input = $request->all();

        $prop->fill($input)->save();
        Session::flash('flash_message', 'Room Type successfully updated!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prop = Room_type::findOrFail($id);

        $prop->delete();

        Session::flash('flash_message', 'Room Type successfully deleted!');

        return redirect()->route('roomtype.index');
    }
}
