<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\Request;
use App\Location_types;

class UpdateLocationTypeRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'location_type' => 'required|min:3',
			'location_type_desc' => 'required|min:6',
		    ];
	}
}
