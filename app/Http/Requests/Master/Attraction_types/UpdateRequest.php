<?php

namespace App\Http\Requests\Master\Attraction_types;

use App\Http\Requests\Request;
use App\Attraction_types;

class UpdateRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'type' => 'required|min:3',
			'description' => 'required|min:6',
		    ];
	}
}
