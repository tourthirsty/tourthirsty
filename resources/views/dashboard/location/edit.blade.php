@extends('dashboard.layouts.master')

@section('page-title')

@section('page-header')

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
    <h1>
       Edit Locations
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li><a href="{{ route('location.index') }}">Locations</a></li>
        <li class="active">Create</li>
      </ol>
@endsection

@section('content')


@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif


    {!! Form::open(['route' => ['location.update', $loc->id] , 'id' => 'location-form']) !!}


<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Locations</div>
            <div class="panel-body">
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="Location_name">Location Name</label>
                    <input type="text" class="form-control" id="Location_name"
                           name="Location_name" value="{{$loc ? $loc->loc_name : '' }}">
                </div>
                <div class="form-group">
                    <label for="Location_type">Location Type</label>
                    <select name="location_types[]" id="ddlCars2" class="ddlCars2 form-control" multiple="multiple">
                    <?php
                    
                    foreach ($loctype as $k => $value) 
                    { 
                        $selected = "";
                        $exp = explode(',',$loc->loc_type);

                        for($i=0;$i<count($exp);$i++)
                        {

                            if($exp[$i] == $value['id'])
                            { 
                                $selected = "selected";
                                echo $exp[$i];
                            } ?>
                            <?php } ?>
                        <option value="<?php echo $value['id']; ?>" <?php echo $selected; ?>><?php echo $value['loc_type_name']; ?></option>
                      
                      <?php } ?>
                      </select>
                </div>
				<div class="form-group">
                    <label for="Location_desc">Location Description</label>
                    <textarea name="Location_desc" id="Location_desc" class="form-control" value="">{{$loc ? $loc->loc_desc : '' }}</textarea>
                </div>
                <!-- #~~~~~~~~~~~~~~~~~~~~~Google Map Starts Here!~~~~~~~~~~~~~~~~~~~# -->
                          
                    <div class="form-group">
                        <input type="text" id="auto" name="add" value="" class="form-control" placeholder="Enter hotel address to mark location on map">
                    </div>
                
               </div>
                
                <div class="col-md-12">
                    <div class="form-group">
                        <div id="gmap" class="col-md-12" style="height:500px;"></div>
                    </div>      
                </div>

                <!-- #~~~~~~~~~~~~~~~~~~~~~Google Map Ends Here!~~~~~~~~~~~~~~~~~~~# -->      
                                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <!--<label>Latitude:</label>-->
                            <input type="hidden" readonly="readonly" name="latitude" class="form-control txtfld" id="lat" value="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <!--<label>Longitude:</label>-->
                            <input type="hidden" readonly="readonly" name="longitude" class="form-control txtfld" id="lng" value="">
                        </div>
                    </div>
                </div>
                
                    </div>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Create 
                        </button>
                    </div>
                    <?php         $lati = 0;
                                  $long = 0;
                                if($loc) 
                                {
                                    if($loc->loc_latitude && $loc->loc_longitude)
                                    {
                                        $lati = $loc->loc_latitude;
                                        $long = $loc->loc_longitude;

                                    }
                                }  
                                ?>    
                    
                </div>
            </div>
        </div>
    </div>

@stop

@section('after-scripts-end')

<script type="text/javascript" src="{{ asset('assets/js/bootstrap-multiselect.js') }}"></script>

<script>
$(document).ready(function() {
    $('.ddlCars2').multiselect({ 
          includeSelectAllOption: true,
            enableFiltering:true  
         });  
});    
</script>

<script async defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBmSevauEk2OuvnnY8DY5po9ghDYmRrFs4&libraries=places&callback=initMap"></script>

 <script>
      
      function initMap() {
        var latit=<?php echo $lati; ?> ;
        var longi=<?php echo $long; ?> ;
        var map = new google.maps.Map(document.getElementById('gmap'), {
          center: {lat:latit , lng:longi },
          zoom: 15
        });
        
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('auto'));
        //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
             position: new google.maps.LatLng(latit , longi),
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

         autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
          }
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          }));
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
          var address = '';
          

          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

           infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
          infowindow.open(map, marker);
          var la = place.geometry.location.lat();
          var lo = place.geometry.location.lng();
          var elem = document.getElementById("lat");
            elem.value = la;
          var elem1 = document.getElementById("lng");
            elem1.value = lo;

});


}

</script>

@stop

@section('fter-scripts-end')
    
        {!! JsValidator::formRequest('App\Http\Requests\Location\CreateLocationRequest', '#location-form') !!}
   

@stop