@extends('dashboard.layouts.master')

@section('page-title')

@section('page-header')

    <h1>
       Add Add-ons
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li><a href="{{ route('additionalamin.index') }}">Add-ons</a></li>
        <li class="active">Create</li>
      </ol>

@endsection

@section('content')

@include('partials.messages')

    {!! Form::open(['route' => 'dashboard.additionalamin.store', 'id' => 'permission-form']) !!}


<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Add-ons</div>
            <div class="panel-body">

                <div class="form-group">
                    <label for="add_aminity">Add-ons</label>
                    <input type="text" class="form-control" id="add_aminity"
                           name="add_aminity" placeholder="Eg:Room decorations, Extra room service ....">
                </div>
                
                <div class="form-group">
                    <label for="add_amini_desc">Description</label>
                    <textarea name="add_amini_desc" id="add_amini_desc" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <label for="ad_amin_rate">Rate</label>
                    <input type="text" class="form-control" id="ad_amin_rate" name="ad_amin_rate" placeholder="Eg:Rs.1000">
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Create 
        </button>
    </div>
</div>

@stop

@section('fter-scripts-end')
    
        {!! JsValidator::formRequest('App\Http\Requests\Master\Proptype\CreateProptypeRequest', '#permission-form') !!}
   <

@stop