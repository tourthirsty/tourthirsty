@include('frontend.includes.header')
<link href=" {{ URL::asset('assets/frontend/css/innerstyle.css')}}" rel="stylesheet">
<link href=" {{ URL::asset('assets/frontend/css/styletogle.css')}}" rel="stylesheet">
    <section>
        <div class="innerbnr">
            <div class="container no-padding-right">
                <h3 class="innertitle">DESTINATIONS</h3>
                    <div class="innerwrapr">
                        <div class="pagetxt">HOME <span>> DESTINATIONS</span></div>
                        <div class="subbox">
                            <span>Destinations to visit in kerala</span>
                            <span><input type="text" placeholder="Search Destinations"/>
                                <button><i class="fa fa-search" aria-hidden="true"></i></button>
                            </span>
                            <div class="maprgt"><img class="img-responsive" src="{{URL::asset('assets/frontend/images/topmap.png')}}"/></div>
                        </div>
                        <div class="btmwrapr">
                            <div class="col-md-7 col-sm-12 col-xs-12 destinatnbox">
                                <?php if (count($loc))
                                {
                                    foreach ($loc as $location)
                                    {
                                        $image = explode(',', $location->url);
                                        if(isset($image[0])) 
                                        {
                                            $img1 = $image[0];
                                        } 
                                        else
                                        {
                                            $img1 = ""; 
                                        }
                                        if(isset($image[1])) 
                                        {
                                            $img2 = $image[1];
                                        } 
                                        else 
                                        {
                                            $img2 = ""; 
                                        }
                                        if(isset($image[2]))
                                        {
                                            $img3 = $image[2];
                                        } 
                                        else 
                                        {
                                            $img3 = "";
                                        } ?>
                                        <div class="destinatn">
                                        <?php if($img1) 
                                        { ?>
                                            <img class="img-responsive" src="{{ asset('upload/places/'.$img1) }}" style="width:265px;height:162px;"/>
                                        <?php }
                                        elseif($img2) 
                                        {  ?>
                                            <img class="img-responsive" src="{{ asset('upload/places/'.$img2) }}"/>
                                        <?php }
                                        elseif($img3) {  ?>
                                            <img class="img-responsive" src="{{ asset('upload/places/'.$img3) }}"/>
                                        <?php } ?>
                                            <div>
                                                <h6>{{ $location->loc_name }}</h6>
                                                <p>{{ $location->loc_desc }}</p>
                                            </div>
                                        </div>
                                    <?php } ?>
                               <?php } ?>
                                    </div>
                                        <div class="col-md-5 col-sm-12 col-xs-12 rightbar">
                                            <h5>Travelers Choice of Hotels</h5>
                                            <div class="hotels">
                                                <img src="images/hotel1.png"/>
                                                <div>
                                                    <h6>The Classik Fort Hotel(Cochin)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>3 travellers mentioned "budget hotel" and "budgeted hotel" when describing this
                                                        hotel</p>
                                                </div>
                                            </div>
                                             <div class="hotels">
                                                <img src="images/hotel2.png"/>
                                                <div>
                                                    <h6>Sagara Beach Resort(Kovalam)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>2 travellers mentioned "discount" when describing this hotel</p>
                                                </div>
                                            </div>
                                             <div class="hotels">
                                                <img src="images/hotel3.png"/>
                                                <div>
                                                    <h6>Marine Palace(Varkala)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>2 travellers mentioned "cheap" when describing this hotel</p>
                                                </div>
                                            </div>
                                              <div class="hotels">
                                                <img src="images/hotel4.png"/>
                                                <div>
                                                    <h6>Backwater Retreat Theme House(Kumarakom)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>2 travellers mentioned "affordable" when describing this hotel</p>
                                                </div>
                                            </div>
                                            <div class="hotels">
                                                <img src="images/hotel5.png"/>
                                                <div>
                                                    <h6>Pulimootil Estate(Munnar)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>2 travellers mentioned "discount" when describing this hotel</p>
                                                </div>
                                            </div>
                                            <div class="hotels">
                                                <img src="images/hotel6.png"/>
                                                <div>
                                                    <h6>Vythiri Meadows(Vythiri)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>2 travellers mentioned "discount" when describing this hotel</p>
                                                </div>
                                            </div>
                                            <div class="hotels">
                                                <img src="images/hotel7.png"/>
                                                <div>
                                                    <h6>Chandys Windy Woods(Munnar)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>"Unique and magical!"<br/>Awsome</p>
                                                </div>
                                            </div>
                                            <div class="hotels">
                                                <img src="images/hotel8.png"/>
                                                <div>
                                                    <h6>Misty Mountain Resort(Munnar)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>"Unique and magical!"<br/>Awsome</p>
                                                </div>
                                            </div>
                                               <div class="hotels">
                                                <img src="images/hotel9.png"/>
                                                <div>
                                                    <h6>The Panoramic Getaway(Munnar)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>"Unique and magical!"<br/>Awsome</p>
                                                </div>
                                            </div>
                                               <div class="hotels">
                                                <img src="images/hotel10.png"/>
                                                <div>
                                                    <h6>Wild Elephant Eco Friendly Resort(Munnar)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>"A very nice place to stay"<br/>"Overall satisfied"</p>
                                                </div>
                                            </div>
                                               <div class="hotels">
                                                <img src="images/hotel11.png"/>
                                                <div>
                                                    <h6>Ramada Allepey(Alappuzha)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>"A very nice place to stay"<br/>"Overall satisfied"</p>
                                                </div>
                                            </div>
                                               <div class="hotels">
                                                <img src="images/hotel12.png"/>
                                                <div>
                                                    <h6>Lake Palace Resort(Alappuzha)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>"A very nice place to stay"<br/>"Overall satisfied"</p>
                                                </div>
                                            </div>
                                             <div class="hotels">
                                                <img src="images/hotel13.png"/>
                                                <div>
                                                    <h6>Kumarakom Lake Resort(Kumarakom)</h6>
                                                     <span>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                                <i class="fa fa-star grey" aria-hidden="true"></i>
                                                     </span>
                                                    <p>"A very nice place to stay"<br/>"Overall satisfied"</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  <div class="pageblock">
                                        <ul class="pagination">
                                            <li><a href="#"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li class="active"><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
                                        </ul>
                                  </div>
                            </div>
                        </div>
                   </div>
               </section>
               <section>
                    <div class="container">
                        <div class="row">
                            <div class="btmad"><img class="img-responsive" src="{{ URL::asset('assets/frontend/images/btmad.jpg')}}"/></div>
                        </div>
                    </div>
                </section>
               
        @include('frontend.includes.footer')
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/accordion.js"></script>
<script>
$(document).ready(function () {
    var $content = $(".content").hide();
    $(".toggle").on("click", function (e) {
        $(this).toggleClass("expanded");
        $(this).next().slideToggle();

    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){ 
        $("[href]").each(function() {
    if (this.href == window.location.href) {
        $(this).addClass("selected");
        }
    });
});
</script>
