<?php

namespace App\Http\Requests\Master\Fhrai;

use App\Http\Requests\Request;
use App\Fhraiclassification;

class UpdateRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'fhrai_class' => 'required|min:3',
			'fhrai_class_desc' => 'required|min:6',
		    ];
	}
}
