<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotelfacility extends Model
{
    protected $table = 'hotelfacilities';
    protected $fillable =[
        'hotel_fac_name',
        'hotel_fac_desc'
    ];
}
