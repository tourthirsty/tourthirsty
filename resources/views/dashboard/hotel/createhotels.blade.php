@extends('dashboard.layouts.master')

@section('page-title', 'Add Hotel User')

@section('page-header')
<h1>
    Add Hotels
    <small>Hotel</small>
</h1>

<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('hotel.hotels',$id) }}">Hotel</a></li>
    <li class="active">Add New</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => 'dashboard.hotel.storeHotels', 'files' => true, 'id' => 'hotel-form']) !!}

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Hotel Details</div>
            <div class="panel-body">
                        <div class="form-group">
                            <label for="hotel_name">Hotel Name</label>
                            <input type="text" class="form-control" id="hotel_name" name="hotel_name" placeholder="Hotel Name" value="">
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea class="form-control" id="address" name="address" placeholder="Address"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="phone">@lang('app.phone')</label>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="@lang('app.phone')" value="">
                        </div>


                        <div class="row form-group">
                            <div class="col-md-10">
                                <label for="desc">Select your Images</label>
                                <input type="file" class="form-control" id="" name="image" placeholder="Select your Image">
                            </div>
                        </div>





            </div>

        </div>
    </div>
    
</div>

 <div class="row">
        <div class="col-md-12">
            <input type="hidden" name="hoteluser"  value=<?php echo $value = Session::get('hoteluser'); ?>>
            <input type="hidden" class="form-control" id="created" name="created"  value=<?php echo Auth::user()->id;?>>
            <input type="hidden" class="form-control" id="group_id" name="group_id"  value=<?php echo $id; ?> >
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-save"></i>
                Create Hotel
            </button>
        </div>
    </div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Hotel\CreateHotelRequest', '#hotel-form') !!}
@stop
