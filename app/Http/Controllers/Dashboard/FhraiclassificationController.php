<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Fhraiclassification as Fhrai;

class FhraiclassificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
      
        $Fhrai = Fhrai::all();

        return view('dashboard.masters.fhraiclass.index',compact('Fhrai'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.masters.fhraiclass.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'fhrai_class' => 'required',
            'fhrai_class_desc' => 'required'
        ]);

        
        
        $Fhrai = new Fhrai;

        $Fhrai->fhrai_class_name = $request->fhrai_class;
        $Fhrai->fhrai_class_desc = $request->fhrai_class_desc;

        $Fhrai->save();
        
        return redirect()->route('dashboard.fhrai')
                        ->withSuccess(trans('Class Added Succesfully'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $fhrai = Fhrai::findOrFail($id);

         return view('dashboard.masters.fhraiclass.edit',compact('fhrai'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Fhrai = Fhrai::findOrFail($id);
         $this->validate($request, [
            'fhrai_class' => 'required',
            'fhrai_class_desc' => 'required'
        ]);

        $Fhrai->fhrai_class_name = $request->fhrai_class;
        $Fhrai->fhrai_class_desc = $request->fhrai_class_desc;

        $Fhrai->save();
        
        return redirect()->route('dashboard.fhrai')
                        ->withSuccess(trans('Details Updated Succesfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Fhrai = Fhrai::findOrFail($id);

        $Fhrai->delete();

        return redirect()->route('dashboard.fhrai')
                        ->withSuccess(trans('Details Deleted Succesfully'));
    }
}
