@include('frontend.includes.header')
<link href=" {{ URL::asset('assets/frontend/css/innerstyle.css')}}" rel="stylesheet">
<link href=" {{ URL::asset('assets/frontend/css/styletogle.css')}}" rel="stylesheet">
@section('page-title', trans('app.login'))
@section('page-title', trans('app.sign_up'))

<section>
<div class="innerbnr">
<div class="container no-padding-right">

  <h3 class="innertitle">LOGIN</h3>
  <div class="innerwrapr">
    <div class="pagetxt"><a href="index.html">HOME</a> <span>> LOGIN</span></div>
    <div class="subbox">Login and Register
      <div class="imgrgt"><img class="img-responsive" src="{{URL::asset('assets/frontend/images/chair.png')}}"/></div>
    </div>
    <div class="btmwrapr">
      <div class="row">
          @include('partials/messages')
        <div class="col-md-6">
          <div class="login">
            <h4>PLEASE LOGIN <i class="fa fa-key icons" aria-hidden="true"></i></h4>
          </div>
          <form role="form" action="<?=url('login')?>" method="POST" id="login-form" autocomplete="off">
            <input type="hidden" value="<?=csrf_token()?>" name="_token">

            @if (Input::has('to'))
                <input type="hidden" value="{{ Input::get('to') }}" name="to">
            @endif
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" name="username" id="username" class="form-control" placeholder="@lang('app.email_or_username')">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" name="password" id="password" class="form-control" placeholder="@lang('app.password')">
            </div>
            <div class="col-md-6 no-padding">
              @if (settings('remember_me'))
              <div class="checkbox">
                <label>
                   <input type="checkbox" name="remember" id="remember"> @lang('app.remember_me')</label>
              </div>
               @endif
              
            @if (settings('forgot_password'))
                <a href="<?=url('password/remind')?>">@lang('app.i_forgot_my_password')</a>
            @endif </div>
            <div class="col-md-6 no-padding">
                <button type="submit" class="btn btn-default login-btn" id="btn-login">@lang('app.log_in')</button>
              <!--<button type="submit" class="btn btn-default login-btn">LOGIN</button>-->
            </div>
          </form>
          <div class="col-md-12 no-padding or">
            <div class="or-text"> OR </div>
          </div>
          <div class="col-md-12 no-padding"> <a href="#">
            <div class="fb"> <i class="fa fa-facebook" aria-hidden="true"></i> Login with Facebook</div>
            </a> <a href="#">
            <div class="google"> <i class="fa fa-google-plus" aria-hidden="true"></i> Login with Google Plus</div>
            </a> </div>
        </div>
        <div class="col-md-6">
          <div class="login">
            <h4>PLEASE SIGN IN TO PROCEED <i class="fa fa-pencil icons" aria-hidden="true"></i></h4>
          </div>

           <form role="form" action="<?=url('register')?>" method="post" id="registration-form" autocomplete="off">

            <input type="hidden" value="<?=csrf_token()?>" name="_token">

            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" name="email" id="email" class="form-control" placeholder="@lang('app.email')" value="{{ old('email') }}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Username</label>
              <input type="text" name="username" id="username" class="form-control" placeholder="@lang('app.username')"  value="{{ old('username') }}">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" name="password" id="password" class="form-control" placeholder="@lang('app.password')">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Confirm Password</label>
              <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="@lang('app.confirm_password')">
            </div>
            <div style="clear:both;"></div>
            <div class="col-md-12 no-padding">
                @if (settings('registration.captcha.enabled'))
                    <div class="form-group has-feedback">
                        {!! app('captcha')->display() !!}
                    </div>
                @endif
            </div>
            <div class="col-md-12 no-padding">
               @if (settings('tos'))
                <div class="col-md-6 no-padding">
                  <div class="checkbox">
                     <input type="checkbox" name="tos" id="tos"> @lang('app.i_accept')<a href="#tos-modal" data-toggle="modal">@lang('app.terms_of_service')</a>
                  </div>
                  <div class="checkbox">
                    
                  </div>
                </div>
                @endif
                <div class="col-md-6 no-padding">
                    <button type="submit" class="btn btn-default login-btn">Register</button>
                    
                  <!--<button type="submit" class="btn btn-default login-btn">CREATE ACCOUNT</button>-->
                </div>
            </div>
          </form>
        </div>
            @if (settings('tos'))
        <div class="modal fade" id="tos-modal" tabindex="-1" role="dialog" aria-labelledby="tos-label">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="@lang('app.terms_of_service')">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title" id="tos-label">@lang('app.terms_of_service')</h3>
                    </div>
                    <div class="modal-body">
                        <h4>1. Terms</h4>

                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Donec quis lacus porttitor, dignissim nibh sit amet, fermentum felis.
                            Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
                            cubilia Curae; In ultricies consectetur viverra. Nullam velit neque,
                            placerat condimentum tempus tincidunt, placerat eu lectus. Nam molestie
                            porta purus, et pretium risus vehicula in. Cras sem ipsum, varius sagittis
                            rhoncus nec, dictum maximus diam. Duis ac laoreet est. In turpis velit, placerat
                            eget nisi vitae, dignissim tristique nisl. Curabitur sollicitudin, nunc ut
                            viverra interdum, lacus...
                        </p>

                        <h4>2. Use License</h4>

                        <ol type="a">
                            <li>
                                Aenean vehicula erat eu nisi scelerisque, a mattis purus blandit. Curabitur congue
                                ollis nisl malesuada egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit:
                            </li>
                        </ol>

                        <p>...</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('app.close')</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

      </div>
    </div>
  </div>
</div>
</div>
</section>


@include('frontend.includes.footer')

@section('after-scripts-end')
    {!! Html::script('assets/js/login.js') !!}
    {!! JsValidator::formRequest('App\Http\Requests\Auth\LoginRequest', '#login-form') !!}
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script> 
@stop 
@section('after-scripts-end')
    {!! JsValidator::formRequest('App\Http\Requests\Auth\RegisterRequest', '#registration-form') !!}
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
@stop