<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\Activity\ActivityRepository;
use App\Repositories\User\UserRepository;
use App\Support\Enum\UserStatus;
use Auth;
use Carbon\Carbon;
use App\Hotel_group_detail as Groupdetails;
use App\Hotel as Hotel;

class DashboardController extends Controller {
	/**
	 * @var UserRepository
	 */
	private $users;
	/**
	 * @var ActivityRepository
	 */
	private $activities;

	/**
	 * DashboardController constructor.
	 * @param UserRepository $users
	 * @param ActivityRepository $activities
	 */
	public function __construct(UserRepository $users, ActivityRepository $activities) {
		$this->middleware('auth');
		$this->users = $users;
		$this->activities = $activities;
	}

	/**
	 * Displays dashboard based on user's role.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
		if (Auth::user()->hasRole('Admin')) {
			return $this->adminDashboard();
		}

		return $this->defaultDashboard();
	}

	/**
	 * Displays dashboard for admin users.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	private function adminDashboard() {
		$usersPerMonth = $this->users->countOfNewUsersPerMonth(
			Carbon::now()->startOfYear(),
			Carbon::now()
		);

		$stats = [
			'total' => $this->users->count(),
			'new' => $this->users->newUsersCount(),
			'banned' => $this->users->countByStatus(UserStatus::BANNED),
			'unconfirmed' => $this->users->countByStatus(UserStatus::UNCONFIRMED),
		];

		$latestRegistrations = $this->users->latest(8);

		return view('dashboard.admin', compact('stats', 'latestRegistrations', 'usersPerMonth'));
	}

	/**
	 * Displays default dashboard for non-admin users.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	private function defaultDashboard() {
		$activities = $this->activities->userActivityForPeriod(
			$id = Auth::user()->id,
			Carbon::now()->subWeeks(2),
			Carbon::now()
		)->toArray();
		$Groupdetails = Groupdetails::where('user_id_fk', '=' , $id)->get();
                $NoofGroup = count($Groupdetails);
                if($NoofGroup!=0) {
                 $hotel = 0;  
                 $group =  $Groupdetails[0]['id']; 
                } else {
                $hotel = Hotel::where('user_id_fk', '=', $id)->first();
		$hotel =  $hotel['id'];
                }
		return view('dashboard.default', compact('activities','hotel','group'));
	}

}
