@extends('dashboard.layouts.master')

@section('page-title', 'Edit Group Details')

@section('page-header')

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<h1>
    Edit Group Details
    <small>Group Details</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('hotel.allGroups') }}">Group</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@include('partials.messages')



{!! Form::open(['route' => ['hotel.updategroup', $Group->id], 'method' => 'PUT', 'id' => 'group-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Group Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="hotel_grp_name">Group</label>
                    <input type="text" class="form-control" id="hotel_grp_name" name="hotel_grp_name" required="" value="{{$Group->hotel_grp_name }}">
                </div>
                 <div class="form-group">
                    <label for="hotel_grp_address">Address</label>
                    <textarea class="form-control" id="hotel_grp_address" name="hotel_grp_address" required="">{{$Group->hotel_grp_address }}</textarea>
                </div>
                <div class="form-group">
                    <label for="hotel_grp_contact">Contact</label>
                    <input type="text" name="hotel_grp_contact" class="form-control" required="" value="{{$Group->hotel_grp_contact }}">
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Hotel_group\UpdateGroupRequest', '#group-form') !!}
@stop
