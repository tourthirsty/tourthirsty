<?php

namespace App\Http\Requests\Hotel;

use App\Http\Requests\Request;
use App\Hotel;
use App\User;

class CreateHotelRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'hotel_name' => 'required|min:3',
			'address' => 'required|min:6',
			'phone' => 'required|min:6',
 			
		    ];
	}
}
