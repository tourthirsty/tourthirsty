<style>
    .remove{ margin-right: 10px;
            float: left;
    }
    .bootstrap-switch-container {
        height: 29px;
    }
    .well { 
        margin-bottom: 20px;
        margin-top: 20px;
    }
    .update {
    bottom: 6px;
    left: 45px;
    margin-bottom: 0;
    position: absolute;
}
.updates {
    bottom: 36px;
    left: 140px;
    /* margin-bottom: 0; */
    position: absolute;
}
.removes {
    float: right;
    margin-right: 0;
    position: absolute;
    right: -107px;
    top: 20px;
}

</style>
<div class="panel panel-default" style="margin-bottom: 15px;">
    <div class="panel-heading">Room Aminities</div>
    <div class="panel-body">
<?php if (count($room_type_edit)) {
    ?>

    <?php for ($i = 0; $i < count($room_type_edit); $i++) {
        ?>
        <div class="optionBox" id="optionBox">
            <div id="docs" class="block">
                <div class="col-md-8">
                    <div class="well clearfix">
                        <div class="form-group">
                            <label for="room_type">Room Categories</label> 
                            <input type="text" name="room_type<?php echo $i; ?>" placeholder="Enter Room Category" value="<?php echo $room_type_edit[$i]->room_type; ?>" class="form-control">
                        </div>
                        <div class="form-group">

                            <b> Select Room Aminities </b>
                            <select name="room_aminities<?php echo $i; ?>[]" id="ddlCars2" class="ddlCars2 form-control" multiple="multiple">
                                <?php
                                foreach ($Room_aminity as $k => $value) {
                                    $selected = "";
                                    foreach ($room_aminities_edit as $h => $aminities_edit) {

                                        if (($room_details_edit[$i]->room_type == $room_aminities_edit[$h]->room_type) && $aminities_edit->room_amn_id_fk == $value['id']) {

                                            $selected = "selected";
                                            break;
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $value['id']; ?>" <?php echo $selected; ?>><?php echo $value['room_amn_name']; ?></option>

                                <?php } ?>
                            </select>
                        </div>
                        <br>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="rooms">No of Rooms</label> 
                                <?php
                                if (count($room_details_edit)) {
                                    $rooms = $room_details_edit[$i]->no_of_rooms;
                                    $adults = $room_details_edit[$i]->max_occ_adults;
                                    $kids = $room_details_edit[$i]->max_occ_kids;
                                } else {
                                    $rooms = "";
                                    $adults = "";
                                    $kids = "";
                                }
                                ?>
                                <input type="text" name="rooms<?php echo $i; ?>" placeholder="No of Rooms" value="<?php echo $rooms; ?>" class="form-control"><br>
                            </div>
                        </div>    
                        <div class="main col-lg-9 myHalfCol">
                            <div class="col-lg-6">
                                <label for="adults">Adults</label> 
                                <div class="clearfix"></div>
                                <input type="text" name="adults<?php echo $i; ?>" placeholder="Max Occupancy of Adults" value="<?php echo $adults; ?>" class="form-control"><br>
                            </div>
                            <div class="col-lg-6">
                                <label for="rooms">Kids</label> 
                                <div class="clearfix"></div>
                                <input type="text" name="kids<?php echo $i; ?>" placeholder="Max Occupancy of Kids" value="<?php echo $kids; ?>" class="form-control"><br>
                            </div>
                        </div>

                    </div>
                   <?php  if ($i != 0) { ?>
                        <span id="remove" class="remove btn-primary btn removes">Remove Row</span>
                    <?php  } ?>
                </div>
                 <div class="clearfix"></div>
            </div>
            </div>
        <?php } ?>
        <input type="hidden" name="id" placeholder="" value="<?php echo $id; ?>">
        <input type="hidden" value="<?php echo count($room_type_edit); ?>" id="counter" name="counter"/><br>
     
      <div class="form-group update">
            <span class="add btn-primary btn">Add Row</span>
            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                <i class="fa fa-refresh"></i>
                Update Details
            </button>
        </div> 
<?php } else { ?>
            <div class="optionBox" id="optionBox">
                <div>  
                    <div class="col-md-8">
                        <div class="well clearfix">
                            <div class="form-group">
                                <label for="room_type">Room Categories</label> 
                                <input type="text" name="room_type0" placeholder="Enter Room Category" value="" class="form-control">
                            </div>
                            <div class="form-group">

                                <b> Select Room Aminities </b>
                                <select name="room_aminities0[]" id="ddlCars2" class="ddlCars2 form-control" multiple="multiple">
                                    <?php
                                    foreach ($Room_aminity as $k => $value) {
                                        $selected = "";
                                        ?>
                                        <option value="<?php echo $value['id']; ?>" <?php echo $selected; ?>><?php echo $value['room_amn_name']; ?></option>

                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <br>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="rooms">No of Rooms</label> 
                                    <?php
                                    $rooms = "";
                                    $adults = "";
                                    $kids = "";
                                    ?>
                                    <input type="text" name="rooms0" placeholder="No of Rooms" value="<?php echo $rooms; ?>" class="form-control"><br>
                                </div>
                            </div>    
                            <div class="main col-lg-9 myHalfCol">
                                <div class="col-lg-6">
                                    <label for="adults">Adults</label> 
                                    <div class="clearfix"></div>
                                    <input type="text" name="adults0" placeholder="Max Occupancy of Adults" value="<?php echo $adults; ?>" class="form-control"><br>
                                </div>
                                <div class="col-lg-6">
                                    <label for="rooms">Kids</label> 
                                   
                                    <input type="text" name="kids0" placeholder="Max Occupancy of Kids" value="<?php echo $kids; ?>" class="form-control"><br>
                                </div>
                            </div>

                        </div>
                        <input type="hidden" name="id" placeholder="" value="<?php echo $id; ?>">
                        
                    </div>
                     <div class="clearfix"></div>
                </div> 

                <input type="hidden" value=1 id="counter" name="counter"/><br>
                <div id="docs" class="block"></div>
                
                <div class="form-group update">
                  <span class="add btn-primary btn">Add Row</span>
                        <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                        <i class="fa fa-refresh"></i>
                        Update Details
                    </button>  
                </div>   
            </div>
        <?php } ?>
    </div>
</div>
