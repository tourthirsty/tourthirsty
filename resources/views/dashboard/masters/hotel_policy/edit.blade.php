@extends('dashboard.layouts.master')

@section('page-title', 'Edit Hotel Policy')

@section('page-header')



<h1>
    Edit Property Policy
    <small>Policy Details</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('propertydetails.index') }}">Policy Details</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@include('partials.messages')



{!! Form::open(['route' => ['dashboard.hotelpolicy.update', $prop->id], 'method' => 'PUT', 'id' => 'prop-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Policy Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="hotel_policy_name">Name of Policy</label>
                    <input type="text" class="form-control" id="hotel_policy_name" name="hotel_policy_name" required="" value="{{$prop->hotel_policy_name }}">
                </div>
				<div class="form-group">
                    <label for="hotel_policy_desc">Description</label>
                    <input type="text" class="form-control" id="hotel_policy_desc" name="hotel_policy_desc" required="" value="{{$prop->hotel_policy_desc }}">
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Masters\Proptype\UpdateProptypeRequest', '#prop-form') !!}
@stop