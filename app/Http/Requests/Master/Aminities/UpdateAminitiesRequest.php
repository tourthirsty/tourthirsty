<?php

namespace App\Http\Requests\Master\Aminities;
use App\Http\Requests\Request;
use App\Room_aminity;

class UpdateAminitiesRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'room_amn_desc' => 'required',
		];
	}
}
