<?php

namespace App\Http\Requests\Hotel_group;
use App\Http\Requests\Request;
use App\Hotel_group_detail;

class CreateGroupRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'hotel_grp_name' => 'required',
			'hotel_grp_address' => 'required',
			'hotel_grp_contact' => 'required',
		];
	}
}
