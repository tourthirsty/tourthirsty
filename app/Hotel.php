<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Hotel extends Model

{
    public function scopeUsers($id)
    {
        
       return $users = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->select('users.*')
            ->where('role_id','=', 3) 
            ->get();   
         
    }
     public function scopeallHotels($id)
    {
        
       return $users = DB::table('hotels')
            ->leftJoin('hotel_group_details', 'hotel_group_details.id', '=', 'hotels.group_id_fk')
            ->select('hotels.*','hotel_group_details.hotel_grp_name')
            ->get();   
         
    }
    public function scopeallGroups($id)
    {
        
       return $users = DB::table('hotel_group_details')
            ->join('users', 'users.id', '=', 'hotel_group_details.user_id_fk')
            ->select('hotel_group_details.*','users.first_name')
            ->get();   
         
    }
      
}
