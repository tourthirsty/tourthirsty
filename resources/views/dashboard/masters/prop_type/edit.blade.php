@extends('dashboard.layouts.master')

@section('page-title', 'Edit Property Type')

@section('page-header')

<!--@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif-->

<h1>
    Edit Property Type
    <small>Property Type Details</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('proptype.index') }}">Property Types</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

<!--@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif-->

@include('partials.messages')



{!! Form::open(['route' => ['dashboard.proptype.update', $prop->id], 'method' => 'PUT', 'id' => 'prop-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Property Type Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="prop_type_name">Property Type</label>
                    <input type="text" class="form-control" id="prop_type_name" name="prop_type_name" placeholder="Budget Hotels, Standard Hotels..." required="" value="{{$prop->prop_type_name }}">
                </div>
                <div class="form-group">
                    <label for="prop_type_desc">Description</label>
                    <textarea name="prop_type_desc" placeholder="Eg:The hill stations are high-altitude towns used, especially by European colonialists..." class="form-control" required="">{{$prop->prop_type_desc }}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Proptype\UpdateProptypeRequest', '#prop-form') !!}
@stop