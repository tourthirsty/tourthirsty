@extends('dashboard.layouts.master')

@section('page-title')

@section('page-header')

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
    <h1>
       Add Hotel Policy
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li><a href="{{ route('hotelpolicy.index') }}">Hotel Policy</a></li>
        <li class="active">Create</li>
      </ol>
@endsection

@section('content')


@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif


    {!! Form::open(['route' => 'dashboard.hotelpolicy.store', 'id' => 'permission-form']) !!}


<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Hotel Policy</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="hotel_policy_name">Policy Name</label>
                    <input type="text" class="form-control" id="hotel_policy_name"
                           name="hotel_policy_name">
                </div>
				<div class="form-group">
                    <label for="hotel_policy_desc">Description</label>
                    <textarea name="hotel_policy_desc" id="hotel_policy_desc" class="form-control"></textarea>
                </div>
                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-2">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Create 
        </button>
    </div>
</div>

@stop

@section('fter-scripts-end')
    
        {!! JsValidator::formRequest('App\Http\Requests\Masters\Proptype\CreateProptypeRequest', '#permission-form') !!}
   <

@stop