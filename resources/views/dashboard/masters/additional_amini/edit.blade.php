@extends('dashboard.layouts.master')

@section('page-title', 'Edit Add-ons')

@section('page-header')

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<h1>
    Edit Add-ons
    <small>Add-ons Details</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('additionalamin.index') }}">Add-ons</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

<!--@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif-->

@include('partials.messages')



{!! Form::open(['route' => ['dashboard.additionalamin.update', $aminity->id], 'method' => 'PUT', 'id' => 'prop-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Add-ons Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="add_aminity">Add-ons</label>
                    <input type="text" class="form-control" id="add_aminity" name="add_aminity" placeholder="" required="" value="{{$aminity->add_aminity }}">
                </div>
                <div class="form-group">
                    <label for="add_amini_desc">Description</label>
                    <textarea name="add_amini_desc" placeholder="" class="form-control" required="">{{$aminity->add_amini_desc }}</textarea>
                </div>
                <div class="form-group">
                    <label for="ad_amin_rate">Rate</label>
                    <input type="text" class="form-control" id="ad_amin_rate" name="ad_amin_rate" placeholder="" required="" value="{{$aminity->ad_amin_rate }}">
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Proptype\UpdateProptypeRequest', '#prop-form') !!}
@stop