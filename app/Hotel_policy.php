<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel_policy extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'hotel_policies';
	
}
