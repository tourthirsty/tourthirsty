<?php

namespace App\Http\Requests\Masters\Aminities;

use App\Http\Requests\Request;

class BaseAminitiesRequest extends Request {
	/**
	 * Validation messages.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'aminity.unique' => 'Aminity alredy exist',
		];
	}
}