<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'rate1' => [
            'required' => 'Rate for CP not defined',
        ],
        'rate2' => [
            'required' => 'Rate for EP not defined',
        ],
        'rate3' => [
            'required' => 'Rate for AP not defined',
        ],
        'rate4' => [
            'required' => 'Rate for MAP not defined',
        ],
        'number0' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number1' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number2' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number3' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number4' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number5' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number6' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number7' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number8' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number9' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number10' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],
        'number11' => [
            'in' => 'Number of Rooms must not be more than 10',
        ],



    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => ['hotel_details_no_rooms' => 'No. of rooms',
                'hotel_details_checkin_time' => 'Checkin time',
                'hotel_details_checkout_time' => 'Check out time',
                'hotel_details_no_restaurants' => 'No. of Restaurant',
                'hotel_details_roomservice_timing' => 'Room Service',
                'hotel_details_bar' => 'No. of Bar',
                'add_aminity' => 'Add-ons name',
                'add_amini_desc' => 'Add-ons description',
                'ad_amin_rate' => 'Add-ons rate',
                'prop_type_name' => 'Property type name',
                'prop_type_desc' => 'Property type description',
                   ],

];
