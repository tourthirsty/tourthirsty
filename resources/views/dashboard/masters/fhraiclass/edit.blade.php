@extends('dashboard.layouts.master')

@section('page-title', 'Edit FHRAI Classification')

@section('page-header')
<h1>
    Edit FHRAI Classification
    <small>FHRAI Classification</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.fhrai') }}">>FHRAI Classification</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => ['dashboard.fhrai.update', $fhrai->id], 'method' => 'PUT', 'id' => 'fhrai-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">FHRAI Classification</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="fhrai_class">FHRAI Class</label>
                    <input type="text" class="form-control" id="fhrai_class" name="fhrai_class" placeholder="AC Room" required="" value="{{$fhrai->fhrai_class_name }}">
                </div>
                <div class="form-group">
                    <label for="fhrai_class_desc">Description</label>
                    <textarea name="fhrai_class_desc" placeholder="Eg:The hill stations are high-altitude towns used, especially by European colonialists..." class="form-control" required="">{{$fhrai->fhrai_class_desc }}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Fhrai\UpdateRequest', '#fhrai-form') !!}
@stop