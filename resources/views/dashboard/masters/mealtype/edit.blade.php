@extends('dashboard.layouts.master')

@section('page-title', 'Meal Type')

@section('page-header')

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<h1>
    Edit Meal Type
    <small>Meal Type</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('meal.index') }}">Meal Types</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@include('partials.messages')



{!! Form::open(['route' => ['dashboard.meal.update', $meal->id], 'method' => 'PUT', 'id' => 'update_meal-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Meal Type Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="meal_type">Meal Type</label>
                    <input type="text" class="form-control" id="meal_type" name="meal_type" placeholder="" required="" value="{{$meal->meal_type }}">
                </div>
                <div class="form-group">
                    <label for="meal_desc">Description</label>
                    <textarea name="meal_desc" placeholder="" class="form-control" required="">{{$meal->meal_desc }}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Mealtype\UpdateMealRequest', '#update_meal-form') !!}
@stop