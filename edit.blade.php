@extends('dashboard.layouts.master')

@section('page-title', 'Manage Hotel Users')

@section('page-header')
<link href="{{ asset('assets/css/bootstrap-multiselect.css') }}" rel="stylesheet" type="text/css" >
<h1>
    Hotel Users
    <small>Manage Hotel Users</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.hotel') }}">Hotel Users</a></li>
</ol>
@endsection

@section('content')

@include('partials.messages')

<div class="nav-tabs-custom">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#details" aria-controls="details" role="tab" data-toggle="tab">
                <i class="fa fa-info-circle "></i>
                Property Information
            </a>
        </li>
        <li role="presentation">
            <a href="#social-networks" aria-controls="social-networks" role="tab" data-toggle="tab">
                <i class="fa fa-info"></i>
                Property Details
            </a>
        </li>
        <li role="presentation">
            <a href="#auth" aria-controls="auth" role="tab" data-toggle="tab">
                <i class="fa fa-asterisk"></i>
                Property Description
            </a>
        </li>
        <li role="presentation">
            <a href="#address" aria-controls="auth" role="tab" data-toggle="tab">
                <i class="fa fa-map-marker"></i>
                Hotel Address 
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="details">
            <div class="row">
                <div class="col-md-12">
               
                   {!! Form::open(['route' => ['hotel.storePropertyinfo'], 'method' => 'post', 'files' => true, 'id' => 'property-info-form']) !!}
                   
                       
                    <div class="panel panel-default">
                        <div class="panel-heading">Property Information</div>

                        <div class="panel-body">
                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label for="prop_name">Property Name</label>
                                <input type="text" class="form-control" id="prop_name" name="prop_name" placeholder="" value="{{ $Propertyinfo ? $Propertyinfo->prop_info_name : '' }}">
                            </div>
                           <!-- <div class="form-group">
                                <label for="group_name">Group Name</label>
                                <input type="text" class="form-control" id="group_name" name="group_name" placeholder="" value="{{ $Propertyinfo ? $Propertyinfo->prop_info_grp_name : '' }}">
                            </div>
                            <div class="form-group">
                                <label for="chain_name">Chain Name</label>
                                <input type="text" class="form-control" id="chain_name" name="chain_name" placeholder="" value="{{ $Propertyinfo ? $Propertyinfo->prop_info_chain_name : '' }}">
                            </div>-->
                            <div class="form-group">
                                <label for="web_address">Website Address</label>
                                <input type="text" class="form-control" id="website" name="web_address" placeholder="" value="{{ $Propertyinfo ? $Propertyinfo->prop_info_web_address : '' }}">
                            </div>    
                            <div class="form-group">
                                <label for="fhrai">FHRAI Class</label>
                                {!! Form::select('fhrai', $fhrai, $Propertyinfo ? $Propertyinfo->fhrai_class_id : '', ['class' => 'form-control']) !!}
                            </div>     
                            </div>    
                            <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">Telephone Number</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="" value="{{ $Propertyinfo ? $Propertyinfo->prop_info_tel_number : '' }}">
                            </div>

                            <div class="form-group">
                                <label for="fax">Fax Number</label>
                                <input type="text" class="form-control" id="fax" name="fax" placeholder="Optional" value="{{ $Propertyinfo ? $Propertyinfo->prop_info_fax_number : '' }}">
                            </div>
                            <div class="form-group">
                                <label for="email">Hotel Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="" value="{{ $Propertyinfo ? $Propertyinfo->prop_info_email : '' }}">
                            </div>  
                            <div class="form-group">
                               
                                    <input type="hidden" class="form-control" id="" name="id" placeholder="" value="<?php echo $id; ?>">
                                    <?php if(isset($Propertyinfo->id)) { ?>
                                    <input type="hidden" class="form-control" id="" name="propertyinfo" placeholder="" value="<?php echo $Propertyinfo->id; ?>">
                                    <?php } ?>
                            </div>  
                            </div>
                            </div>    
                            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                <i class="fa fa-refresh"></i>
                                Update Details
                            </button>
                        </div>
                    </div>
                   
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="social-networks">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => ['hotel.propdetailstore'], 'method' => 'post', 'files' => true, 'id' => 'details-details-form']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">Property Details</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="hotel_details_no_rooms">Number of Rooms</label>
                                        <input type="text" class="form-control" id="hotel_details_no_rooms"
                                            name="hotel_details_no_rooms" value="{{$Propertydetails ? $Propertydetails->hotel_details_no_rooms : ''}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="hotel_details_checkin_time">Check-in Time</label>
                                        <div class='input-group date' id='datetimepicker5'>
                                            <!--<label for="to">From</label>-->
                                            <input type="text" class="form-control"  id="timepicker_in" name="timepicker_in" placeholder="" value="{{$Propertydetails ? $Propertydetails->hotel_details_checkin_time : ''}}" >
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="hotel_details_checkout_time">Check-out Time</label>
                                        <div class='input-group date' id='datetimepicker6'>
                                            <!--<label for="to">From</label>-->
                                            <input type="text" class="form-control"  id="timepicker_out" name="timepicker_out" placeholder="" value="{{$Propertydetails ? $Propertydetails->hotel_details_checkout_time : '' }}" >
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="hotel_details_no_restaurants">No. of Restaurants</label>
                                        <input type="text" class="form-control" id="hotel_details_no_restaurants" placeholder=""
                                            name="hotel_details_no_restaurants" value="{{$Propertydetails ? $Propertydetails->hotel_details_no_restaurants : '' }}">
				
                                    </div>

							        <div class="form-group">
                                        <label for="hotel_details_roomservice_timing">Room Service Timing</label>
                                        <div class= "row">
                                        <div class="col-md-6">
                                        <div class='input-group date' id='datetimepicker3'>
                                            <!--<label for="to">From</label>-->
                                            <input type="text" class="form-control"  id="timepicker_from" name="timepicker_from" placeholder="From" value="{{$Propertydetails ? $Propertydetails->hotel_details_roomservice_from : '' }}" >
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class='input-group date' id='datetimepicker4'>
                                            <!--<label for="to">From</label>-->
                                            <input type="text" class="form-control"  id="timepicker_to" name="timepicker_to" placeholder="To" value="{{$Propertydetails ? $Propertydetails->hotel_details_roomservice_to : '' }}" >
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
							        <div class="form-group">
                                        <label for="hotel_details_bar">No.of Bars</label>
                                        <input type="text" class="form-control" id="hotel_details_bar" placeholder=""
                                            name="hotel_details_bar" value="{{$Propertydetails ? $Propertydetails->hotel_details_bar : '' }}">
                                    </div>
                                </div>
							    <input type="hidden" class="form-control" id="" name="id" placeholder="" value="<?php echo $id; ?>">
							    <?php if(isset($Propertydetails->id)) { ?>
								    <input type="hidden" class="form-control" id="" name="propertydetails" placeholder="" value="<?php echo $Propertydetails->id; ?>">
                                <?php } ?>
                            </div>
                            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                <i class="fa fa-refresh"></i>
                                     Update Details
                            </button>
                        </div>
                    </div>
					{!! Form::close() !!}
                </div>
            </div>
        </div>
        

        <div role="tabpanel" class="tab-pane" id="auth">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => ['hotel.storepropdetailsdesc'], 'method' => 'post', 'id' => 'desc-details-form']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">Property Description</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="hotel_details_desc">Description</label>
                                <textarea type="text" rows="10" cols="10" class="form-control" id="hotel_details_desc"
                                       name="hotel_details_desc" placeholder="Description" value="">{{$Propertydetails ? $Propertydetails->hotel_details_desc : '' }}</textarea>
                            </div>
							<input type="hidden" name="id" value="<?php echo $id; ?>">
							<?php if(isset($Propertydetails->id)){?>
								<input type="hidden" class="form-control" id="" name="descdetails" placeholder="" value="<?php echo $Propertydetails->id; ?>">
							<?php } ?>
                            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                <i class="fa fa-refresh"></i>
                                    Update Details
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="address">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => ['hotel.storepropinfoaddress'], 'method' => 'post', 'id' => 'desc-details-form']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">Hotel Address</div>
                        <div class="panel-body">
                            <div class="row form-group">
                                <div class="col-md-10">
                                    <label for="address">Hotel Address</label>
                                    <textarea class="form-control" rows="5" cols="5" id="address" placeholder="Enter Full Hotel Address"
                                           name="address" value="">{{ $hotel ? $hotel->address : '' }}</textarea>
                                </div>

                                 <input type="hidden" class="form-control" id="" name="id" placeholder="" value="<?php echo $id; ?>">
                                 <?php if(isset($Propertyinfo->id)) { ?>
                                     <input type="hidden" class="form-control" id="" name="addressinfo" placeholder="" value="<?php echo $Propertyinfo->id; ?>">
                                 <?php } ?>
                            </div>

                            <!-- #~~~~~~~~~~~~~~~~~~~~~Google Map Starts Here!~~~~~~~~~~~~~~~~~~~# -->
                          
                            <div class="row form-group">
                                <div class="col-md-10">
                                    <input type="text" id="auto" name="add" value="" class="form-control" placeholder="Enter hotel address to mark location on map and click Search">
                                </div>
                                <div class="col-md-2">
                                    <input type="button" class="btn btn-primary" name="search" value="Search">
                                </div>
                            </div>
                            <div id="coords"></div>
                            <div class="row col-md-12">
                                <div class="form-group">
                                    <div id="gmap" class="col-md-12" style="height:500px;"></div>
                                </div>      
                            </div>

                            <!-- #~~~~~~~~~~~~~~~~~~~~~Google Map Ends Here!~~~~~~~~~~~~~~~~~~~# -->      
                                
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Latitude:</label>
                                        <input type="text" readonly="readonly" name="latitude" class="form-control txtfld" id="lat" value="{{$Propertyinfo ? $Propertyinfo->latitude : ''}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Longitude:</label>
                                        <input type="text" readonly="readonly" name="longitude" class="form-control txtfld" id="lng" value="{{$Propertyinfo ? $Propertyinfo->longitude : ''}}">
                                    </div>
                                </div>
                            </div>
                            <br>                           
                        <div> 
                            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                <i class="fa fa-refresh"></i>
                                    Update Details
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-----Second Row------------->
<div class="nav-tabs-custom">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#policy" aria-controls="details" role="tab" data-toggle="tab">
                <i class="fa fa-product-hunt"></i>
                Hotel Policy
            </a>
        </li>
        <li role="presentation">
            <a href="#bank" aria-controls="social-networks" role="tab" data-toggle="tab">
                <i class="fa fa-credit-card "></i>
                Bank Details
            </a>
        </li>
        <li role="presentation">
            <a href="#contacts" aria-controls="auth" role="tab" data-toggle="tab">
                <i class="fa fa-phone"></i>
                Authorised Contacts
            </a>
        </li>
        <li role="presentation">
            <a href="#mealtype" aria-controls="auth" role="tab" data-toggle="tab">
                <i class="fa fa-cutlery"></i>
                Meal Type
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="policy">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => ['hotel.storehotelpolicy'], 'method' => 'post', 'id' => 'policy-details-form']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">Hotel Policy</div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label for="email">No Show Policy</label>
                                <textarea type="text" rows="5" cols="5" class="form-control" id="hotel_policy_noshow"
                                       name="hotel_policy_noshow" placeholder="No Show Policy" value="">{{$hotelpolicy ? $hotelpolicy->hotel_policy_noshow : '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="username">Cancellation Policy</label>
                                <textarea type="text" rows="5" cols="5" class="form-control" id="hotel_policy_cancellation" placeholder="Cancellation Policy"
                                       name="hotel_policy_cancellation" value="">{{$hotelpolicy ? $hotelpolicy->hotel_policy_cancellation : '' }}</textarea>
                            </div>
							<input type="hidden" name="id" value="<?php echo $id; ?>">
							<?php if(isset($hotelpolicy->id)){?>
								<input type="hidden" class="form-control" id="" name="policydetails" placeholder="" value="<?php echo $hotelpolicy->id; ?>">
							<?php } ?>
                            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                <i class="fa fa-refresh"></i>
                                Update Details
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="bank">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => ['hotel.storebankdetails'], 'method' => 'post', 'id' => 'bank-details-form']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">Bank Details</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="bank_details_acc_name">Account Name</label>
                                        <input type="text" class="form-control" id="bank_details_acc_name"
                                            name="bank_details_acc_name" placeholder="" value="{{$bankdetails ? $bankdetails->bank_details_acc_name : '' }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="bank_details_acc_number">Account Number</label>
                                        <input type="text" class="form-control" id="bank_details_acc_number" placeholder=""
                                            name="bank_details_acc_number" placeholder="" value="{{$bankdetails ? $bankdetails->bank_details_acc_number : '' }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="bank_details_bank_name">Bank Name</label>
                                        <input type="text" class="form-control" id="bank_details_bank_name"
                                            name="bank_details_bank_name" placeholder="" value="{{$bankdetails ? $bankdetails->bank_details_bank_name : '' }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="bank_details_branch">Branch</label>
                                        <input type="text" class="form-control" id="bank_details_branch"
                                            name="bank_details_branch" placeholder="" value="{{$bankdetails ? $bankdetails->bank_details_branch : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
							        <div class="form-group">
                                        <label for="bank_details_bank_address">Bank Address</label>
                                        <input type="text" class="form-control" id="bank_details_bank_address"
                                            name="bank_details_bank_address" placeholder="" value="{{$bankdetails ? $bankdetails->bank_details_bank_address : '' }}">
                                    </div>
							        <div class="form-group">
                                        <label for="bank_details_ifsc_code">IFSC code</label>
                                        <input type="text" class="form-control" id="bank_details_ifsc_code"
                                            name="bank_details_ifsc_code" placeholder="" value="{{$bankdetails ? $bankdetails->bank_details_ifsc_code : '' }}">
                                    </div>
							        <div class="form-group">
                                        <label for="bank_details_swift_code">Swift Code</label>
                                        <input type="text" class="form-control" id="bank_details_swift_code"
                                            name="bank_details_swift_code" placeholder="Optional" value="{{$bankdetails ? $bankdetails->bank_details_swift_code : '' }}">
                                    </div>
                                </div>
							    <input type="hidden" name="id" value="<?php echo $id; ?>">
							    <?php if(isset($bankdetails->id)){?>
								    <input type="hidden" class="form-control" id="" name="detailsbank" placeholder="" value="<?php echo $bankdetails->id; ?>">
							    <?php } ?>
                            </div>
							<button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                <i class="fa fa-refresh"></i>
                                    Update Details
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="contacts">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => ['hotel.storecontactdetails'], 'method' => 'post', 'files' => true, 'id' => 'bank-details-form']) !!}
                        <div class="panel panel-default">
                            <div class="panel-heading">Authorised Contacts</div>
                            <div class="panel-body">
                                <?php if (count($authcontact)) { ?>
                                    <?php for ($i = 0; $i < count($authcontact); $i++) { ?>
                                     <div class="optionBox" id="optionBox">
                                        <div class="block1" id="block1" >
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="hotel_auth_name">Contact Name</label>
                                                        <input type="text" class="form-control" id="hotel_auth_name"
                                                        name="hotel_auth_name<?php echo $i; ?>" placeholder="" value="<?php echo $authcontact[$i]->hotel_auth_name; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="hotel_auth_category">Contact Category</label>
                                                        <input type="text" class="form-control" id="hotel_auth_category" placeholder="Sales, Reservation, Account"
                                                        name="hotel_auth_category<?php echo $i; ?>" value="<?php echo $authcontact[$i]->hotel_auth_category; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="hotel_auth_designation">Designation</label>
                                                        <input type="text" class="form-control" id="hotel_auth_designation"
                                                        name="hotel_auth_designation<?php echo $i; ?>" placeholder="" value="<?php echo $authcontact[$i]->hotel_auth_designation; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="hotel_auth_email"> Email</label>
                                                        <input type="text" class="form-control" id="hotel_auth_email" name="hotel_auth_email<?php echo $i; ?>" placeholder="" value="<?php echo $authcontact[$i]->hotel_auth_email; ?>">
                                                    </div>  
                                                     <div class="form-group">
                                                        <label for="hotel_auth_landline">Landline</label>
                                                        <input type="text" class="form-control" id="hotel_auth_landline"
                                                        name="hotel_auth_landline<?php echo $i; ?>" placeholder="" value="<?php echo $authcontact[$i]->hotel_auth_landline; ?>">
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="hotel_auth_mobile">Mobile</label>
                                                        <input type="text" class="form-control" id="hotel_auth_mobile"
                                                        name="hotel_auth_mobile<?php echo $i; ?>" placeholder="" value="<?php echo $authcontact[$i]->hotel_auth_mobile; ?>">
                                                    </div>
                                                </div>
                                                <?php  if ($i != 0) { ?>
                                                    <span id="remove" class="re btn-primary btn " style="margin-left:850px;">Remove Row</span>
                                                <?php  } ?>
                                                
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        </div>
                                    <?php } ?>
                                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                                    <input type="hidden" class="form-control" id="contactdetails" name="contactdetails" placeholder="" value="<?php echo count($authcontact); ?>">
                                    <span class="added btn-primary btn">Add Row</span>
                                    <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                        <i class="fa fa-refresh"></i>
                                        Update Details
                                    </button>
                                <?php } else { ?>
                                    <div class="optionBox" id="optionBox">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="hotel_auth_name">Contact Name</label>
                                                <input type="text" class="form-control" id="hotel_auth_name"
                                                name="hotel_auth_name0" placeholder="" value="">
                                            </div>
                                            <div class="form-group">
                                                <label for="hotel_auth_category">Contact Category</label>
                                                <input type="text" class="form-control" id="hotel_auth_category" placeholder="Sales, Reservation, Account"
                                                name="hotel_auth_category0" value="">
                                            </div>
                                            <div class="form-group">
                                                <label for="hotel_auth_designation">Designation</label>
                                                <input type="text" class="form-control" id="hotel_auth_designation"
                                                name="hotel_auth_designation0" placeholder="" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="hotel_auth_email"> Email</label>
                                                <input type="text" class="form-control" id="hotel_auth_email" name="hotel_auth_email0" placeholder="" value="">
                                            </div>  
                                            <div class="form-group">
                                                <label for="hotel_auth_landline">Landline</label>
                                                <input type="text" class="form-control" id="hotel_auth_landline"
                                                name="hotel_auth_landline0" placeholder="" value="">
                                            </div>
                                            <div class="form-group">
                                                <label for="hotel_auth_mobile">Mobile</label>
                                                <input type="text" class="form-control" id="hotel_auth_mobile"
                                                name="hotel_auth_mobile0" placeholder="" value="">
                                            </div>
                                        </div>
                                        </div>
                                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                                        <div class="clearfix"></div>
                                        <input type="hidden" value=1 id="contactdetails" name="contactdetails"/><br>
                                            <div class="block1" id="block1" ></div>
                                        <span class="added btn-primary btn">Add Row</span>
                                        <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                            <i class="fa fa-refresh"></i>
                                            Update Details
                                        </button>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="mealtype">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => ['hotel.storemeals'], 'method' => 'post', 'id' => 'hotel-fac-form']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">Meal Type</div>
                        <div class="panel-body">
                        <div class="row">
                            <?php 
                                $all_data = array();
                                foreach($Hotelmeal as $role)
                                {
                                     $all_data[] =  $role->meal_id_fk;
                                }
                            ?>
                            <?php $countmeal = count($Mealtype); ?>

                              <!--\\\\\\\\\\\\\\\\\\First///////////////////////-->
                            
                                <div class="col-md-2">
                                    
                                        {{ Form::label('type1', 'CP') }} <br>
                                        {{ Form::checkbox('meal_type_cp','1', in_array(1, $all_data),['class' => 'switch']) }}<br>
                                        <div class="radio">
                                            <label for="type1.1">
                                                <input type="radio"  name="type_cp" id= "type1.1" value="1" onclick="paid()"  <?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){ 
                                                    if($Hotelmeal[$i]->freeorpaid =='1' &&  $Hotelmeal[$i]->meal_id_fk =='1') 
                                                    { echo 'checked'; } } } ?> >
                                                            Free
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label for="type1.2">
                                                <input type="radio" name= "type_cp" id= "type1.2" value="2" onclick="paid()"  <?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){
                                                    if($Hotelmeal[$i]->freeorpaid =='2' &&  $Hotelmeal[$i]->meal_id_fk =='1') 
                                                    { echo 'checked'; } } }?> >
                                                            Paid
                                            </label>
                                        </div>
                                        <div id="dv1" class="form-group" style="display: none">
                                            <label for="rate">Rate</label>
                                            <input type="text" name="rate1" id="" placeholder="Rate" value="<?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){ if($Hotelmeal[$i]->meal_id_fk =='1'){?> {{$Hotelmeal[$i] ? $Hotelmeal[$i]->meal_rates : ''}} <?php } } }?>" >
                                        </div>
                                        <br><br>
                                    </div>
                                            <!--\\\\\\\\\\\\\\\\\\Second///////////////////////-->

                                    <div class="col-md-2">
                                        {{ Form::label('type2', 'EP') }} <br>
                                        {{ Form::checkbox('meal_type_ep','2', in_array(2, $all_data),['class' => 'switch']) }}<br>
                                        <div class="radio">
                                            <label>
                                                <input type="radio"  name= "type_ep" id= "type2.1" value="1" onclick="paid1()" <?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){
                                                    if($Hotelmeal[$i]->freeorpaid =='1' &&  $Hotelmeal[$i]->meal_id_fk =='2') 
                                                    { echo 'checked';}}} ?> >
                                                            Free
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name= "type_ep" id= "type2.2" value="2" onclick="paid1()" <?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){
                                                    if($Hotelmeal[$i]->freeorpaid =='2' &&  $Hotelmeal[$i]->meal_id_fk =='2') 
                                                    { echo 'checked'; }}} ?> >
                                                            Paid
                                            </label>
                                        </div>
                                        <div id="dv2" class="form-group" style="display: none">
                                            <label for="rate">Rate</label>
                                            <input type="text" name="rate2" id="" placeholder="Rate" value="<?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){ if($Hotelmeal[$i]->meal_id_fk =='2'){?> {{$Hotelmeal[$i] ? $Hotelmeal[$i]->meal_rates : ''}} <?php } } }?>" >
                                    
                                        </div> 
                                        <br><br>
                                    
                                    </div>

                                             <!--\\\\\\\\\\\\\\\\\\Third///////////////////////-->

                                <div class="col-md-2">
                                    
                                        {{ Form::label('type3', 'AP') }} <br>
                                        {{ Form::checkbox('meal_type_ap','3', in_array(3, $all_data),['class' => 'switch']) }}<br>
                                        <div class="radio">
                                            <label>
                                                <input type="radio"  name= "type_ap" id= "type3.1" value="1" onclick="paid2()" <?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){ 
                                                    if($Hotelmeal[$i]->freeorpaid =='1' &&  $Hotelmeal[$i]->meal_id_fk =='3') 
                                                    { echo 'checked';}}} ?> >
                                                        Free
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio"  name= "type_ap" id= "type3.2" value="2" onclick="paid2()" <?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){ 
                                                    if($Hotelmeal[$i]->freeorpaid =='2' &&  $Hotelmeal[$i]->meal_id_fk =='3') 
                                                    { echo 'checked';}}} ?> >
                                                            Paid
                                            </label>
                                        </div>
                                        <div id="dv3" class="form-group" style="display: none">
                                            <label for="rate">Rate</label>
                                            <input type="text" name="rate3" id="" placeholder="Rate" value="<?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){ if($Hotelmeal[$i]->meal_id_fk =='3'){?> {{$Hotelmeal[$i] ? $Hotelmeal[$i]->meal_rates : ''}} <?php } } }?>" >
                                        </div>
                                        <br><br>
                                </div>
                                    <!--\\\\\\\\\\\\\\\\\\fourth///////////////////////-->

                                    <div class="col-md-2">
                                        {{ Form::label('type4', 'MAP') }} <br>
                                        {{ Form::checkbox('meal_type_map','4', in_array(4, $all_data),['class' => 'switch']) }}<br>
                                        <div class="radio">
                                            <label>
                                                <input type="radio"  name= "type_map" id= "type4.1" value="1" onclick="paid3()" <?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){
                                                    if($Hotelmeal[$i]->freeorpaid =='1' &&  $Hotelmeal[$i]->meal_id_fk =='4') 
                                                    { echo 'checked';}}} ?> >
                                                            Free
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name= "type_map" id= "type4.2" value="2" onclick="paid3()" <?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){
                                                if($Hotelmeal[$i]->freeorpaid =='2' &&  $Hotelmeal[$i]->meal_id_fk =='4') 
                                                { echo 'checked';}}}?>>
                                                        Paid
                                            </label>
                                        </div>
                                        <div id="dv4" class="form-group" style="display: none">
                                            <label for="rate">Rate</label>
                                            <input type="text" name="rate4" id="" placeholder="Rate" value="<?php for($i=0;$i<=3;$i++) { if(isset($Hotelmeal[$i])){ if($Hotelmeal[$i]->meal_id_fk =='4'){?> {{$Hotelmeal[$i] ? $Hotelmeal[$i]->meal_rates : ''}} <?php } } }?>" >
                                        </div>
                                        <br>                            
                                    
                                </div>
                                <input type="hidden" name="id" placeholder="" value="<?php echo $id; ?>">
                                <input type="hidden" name="facilityCount" value="<?php echo $countmeal; ?>">
                            </div>
                            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                <i class="fa fa-refresh"></i>
                                    Update Details
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

<!----Second Row End------------>
<!-----Third Row------------->
<div class="nav-tabs-custom">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#facilities" aria-controls="details" role="tab" data-toggle="tab">
                <i class="fa fa-star"></i>
                Hotel Facilities
            </a>
        </li>
        <li role="presentation">
            <a href="#networks" aria-controls="social-networks" role="tab" data-toggle="tab">
                <i class="fa fa-fort-awesome"></i>
               Property Type
            </a>
        </li>
        
        <li role="presentation">
            <a href="#social" aria-controls="auth" role="tab" data-toggle="tab">
                <i class="fa fa-wrench"></i>
                Technical Details
            </a>
        </li>
        <li role="presentation">
            <a href="#room-types" aria-controls="room-types" role="tab" data-toggle="tab">
                <i class="fa fa-fort-awesome"></i>
               Room Types
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="facilities">
            <div class="row">
               <div class="col-md-12">
                   {!! Form::open(['route' => ['hotel.storeHotelfacility'], 'method' => 'post', 'files' => true, 'id' => 'hotel-fac-form']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">Hotel Facilities</div>

                        <div class="panel-body">
                        <div class="row">
                            <?php 
                                $all_data = array();
                                foreach($Hotelfacility as $role){
                                     $all_data[] =  $role->fac_id_fk;
                                }
                            ?>
                            <?php $countFac = count($facility); 
                             
                                  foreach ($facility as $value) {  ?>
                            <div class="col-md-4" style="margin-bottom:25px;">
                                {{ Form::label('role', $value['fac_name']) }} 
                                
                                <div style="width: 90px;height: 40%;">
                                {{ Form::checkbox('hotel_facility[]', $value['id'], in_array($value['id'], $all_data),['class' => 'switch']) }}
                             </div>
                            </div> 
                            <?php }?>
                            <input type="hidden" name="id" placeholder="" value="<?php echo $id; ?>">
                            <input type="hidden" name="facilityCount" value="<?php echo $countFac; ?>">
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                <i class="fa fa-refresh"></i>
                                Update Details
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    
        <div role="tabpanel" class="tab-pane" id="networks">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => ['hotel.storeHoteltype'], 'method' => 'post', 'id' => 'hotel-fac-form']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">Property Type</div>

                        <div class="panel-body">
                        <div class="row">
							<?php 
                                $all_data = array();
                                foreach($hoteltype as $role){
                                     $all_data[] =  $role->prop_id_fk;
                                }
                            ?>
							<?php $countprop = count($type); 
                                  foreach ($type as $value) { ?>
                            <div class="col-md-2" style="margin-bottom:25px;">
                                 {{ Form::label('role', $value['prop_type_name']) }} 
                               
                                <div style="width: 90px;height: 50%;">
                                {{ Form::checkbox('hotel_type[]', $value['id'], in_array($value['id'], $all_data),['class' => 'switch']) }}
                            </div>
                            </div> 
                            <?php }?>
                            <input type="hidden" name="id" placeholder="" value="<?php echo $id; ?>">
                            <input type="hidden" name="facilityCount" value="<?php echo $countFac; ?>">
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                <i class="fa fa-refresh"></i>
                                Update Details
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            </div>
       
        <div role="tabpanel" class="tab-pane" id="social">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => ['hotel.storetechdetails'], 'method' => 'post', 'id' => 'tech-details-form']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">Technical Details</div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label for="Pms_name">Name of PMS</label>
                                <input type="text" class="form-control" id="Pms_name"
                                       name="Pms_name" placeholder="Name of PMS" value="{{$tech ? $tech->hotel_pms_name : '' }}">
                            </div>
                            <div class="form-group">
                                <label for="Engine_provider">Channel Manager</label>
                                <input type="text" class="form-control" id="channel_manager" placeholder="Channel Manager"
                                       name="channel_manager" value="{{$tech ? $tech->hotel_channel_name : '' }}">
                            </div>
						      <input type="hidden" name="id" value="<?php echo $id; ?>">
							<?php if(isset($tech->id)){?>
								<input type="hidden" class="form-control" id="" name="technicaldetails" placeholder="" value="<?php echo $tech->id; ?>">
							<?php } ?>
							<button type="submit" class="btn btn-primary" id="update-login-details-btn">
                                <i class="fa fa-refresh"></i>
                                Update Details
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
     <!------Room Type--------------------------->
        <div role="tabpanel" class="tab-pane" id="room-types">
            <div class="row">
                <div class="col-md-12">
                   {!! Form::open(['route' => ['hotel.storeExtrabed'], 'method' => 'post', 'files' => true, 'id' => 'extra-bed-form']) !!}
                    @include('dashboard.hotel.partials.extrabed')
                    {!! Form::close() !!}
                     {!! Form::open(['route' => ['hotel.storeExtrakidsbed'], 'method' => 'post', 'files' => true, 'id' => 'extra-bed-form']) !!}
                    @include('dashboard.hotel.partials.extrakidsbed')
                    {!! Form::close() !!}
                     {!! Form::open(['route' => ['hotel.storeHotelroomamenities'], 'method' => 'post', 'files' => true, 'id' => 'extra-bed-form']) !!}
                     @include('dashboard.hotel.partials.roomtypes')
                     {!! Form::close() !!}
                </div>
            </div>

        </div>
    <!--------------End--------------------------->
    </div>
</div>
<!----Third Row End------------>
@stop
@section('after-scripts-end')

<script type="text/javascript" src="{{ asset('assets/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
<script>
        //$(".switch").bootstrapSwitch({size: 'small'}); 
        $(document).ready(function() {
      
        $('.ddlCars2').multiselect({ 
          includeSelectAllOption: true,
            enableFiltering:true         

      });
      //-----------------Add Rows Dynamically-----------------//
      var max_fields = 5;
    var addDiv = 0;
    var counters = $('#counter').val();
    if(counters) {
        var counter = parseInt(counters);  
    } else {
        var counter = 1;
    }

    $('.add').click(function () {
        
        if (addDiv < max_fields) {
         
        $('.block:last').after('<div class="optionBox" id="optionBox">'
                + '<div class="col-md-8">'
                + '<div class="well clearfix">'
                + '<div class="form-group">'
                + ' <label for="room_type">Room Categories</label>'
                + '<input type="text" name="room_type'+counter+'" placeholder="Enter Room Category" value="" class="form-control room_type">'
                + '</div>'
                + '<div class="form-group">'

                + '<b> Select Room Aminities </b>'
                + '<select name="room_aminities'+counter+'[]" id="ddlCars2" class="ddlCars2 form-control room_aminities" multiple="multiple">'
                <?php
                foreach ($Room_aminity as $k => $value) {
                    $selected = "";
                    ?>
                            +'<option value="<?php echo $value['id']; ?>" <?php echo $selected; ?>><?php echo $value['room_amn_name']; ?></option>'

                    <?php
                }
                ?>
                + '</select>'
                + '</div>'
                + '<br>'
                + '<div class="col-md-2">'
                + '<div class="form-group">'
                + '<label for="rooms">No of Rooms</label>'
                <?php
                $rooms = "";
                $adults = "";
                $kids = "";
                ?>
                + '<input type="text" name="rooms'+counter+'" placeholder="No of Rooms" value="<?php echo $rooms; ?>" class="form-control rooms"><br>'
                + '</div>'
                + '</div>'
                + '<div class="main col-lg-9 myHalfCol">'
                + '<div class="col-lg-6">'
                + '<label for="adults">Adults</label> '
                + '<div class="clearfix"></div>'
                + '<input type="text" name="adults'+counter+'" placeholder="Max Occupancy of Adults" value="<?php echo $adults; ?>" class="form-control adults"><br>'
                + '</div>'
                + '<div class="col-lg-6">'
                + '<label for="rooms">Kids</label> '
                + '<div class="clearfix"></div>'
                + '<input type="text" name="kids'+counter+'" placeholder="Max Occupancy of Kids" value="<?php echo $kids; ?>" class="form-control kids"><br>'
                + '</div>'
                + '</div>'

                + '</div>'
                + '</div><br>'
                + '<span id="remove' + counter + '" class="remove btn-primary btn ">Remove Row</span>'
                + '<div class="clearfix"></div>'
                + '</div><div class="clearfix"></div>'
                );
                $('.ddlCars2').multiselect({
                includeSelectAllOption: true,
                        enableFiltering:true

                });
                counter += 1;
                $('#counter').val(counter);
                } else {

                alert('Limit Reached...');
                }
                $('#remove' + counter).prop('disabled', false);
                addDiv++;
    });
    $('.optionBox').on('click', '.remove', function (e) {
        $(this).parent().remove();
        addDiv--;
        
        counter -= 1;
        $('#counter').val(counter);
        $('#remove' + counter).prop('disabled', false);
        $(".room_type").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'room_type' + name);

        });
        $(".room_aminities").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'room_aminities' + name);
        });
        $(".rooms").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'rooms' + name);
        });
        $(".adults").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'adults' + name);
        });
        $(".kids").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'kids' + name);
        });

    });
    //---------------------------End----------------------//

     });
//------------------row for contacts-----------------//
     $(document).ready(function() {
    var max_fields = 5;
    var addDiv = 0;
    var counters = $('#contactdetails').val();
    if(counters) {
        var counter = parseInt(counters);  
    } else {
        var counter = 1;
    }

    $('.added').click(function () {
        
        if (addDiv < max_fields) {
         
        $('.block1:last').after('<div class="optionBox" id="optionBox">'
                +'<div class="block1" id="block1" >'
                + '<div class="row">'
                +'<div class="col-md-6">'
                +'<div class="form-group">'
                +'<label for="hotel_auth_name">Contact Name</label>'
                +'<input type="text" class="form-control" id="hotel_auth_name" name="hotel_auth_name'+counter+'" placeholder="" value="">'
                +'</div>'
                +'<div class="form-group">'
                +'<label for="hotel_auth_category">Contact Category</label>'
                +'<input type="text" class="form-control" id="hotel_auth_category" placeholder="Sales, Reservation, Account" name="hotel_auth_category'+counter+'" value="">'
                +'</div>'
                +'<div class="form-group">'
                +'<label for="hotel_auth_designation">Designation</label>'
                +'<input type="text" class="form-control" id="hotel_auth_designation" name="hotel_auth_designation'+counter+'" placeholder="" value="">'
                +'</div>'
                +'</div>'
                +'<div class="col-md-6">'
                +'<div class="form-group">'
                +'<label for="hotel_auth_email"> Email</label>'
                +'<input type="hotel_auth_email" class="form-control" id="hotel_auth_email" name="hotel_auth_email'+counter+'" placeholder="" value="">'
                +'</div>'  
                +'<div class="form-group">'
                +'<label for="hotel_auth_landline">Landline</label>'
                +'<input type="text" class="form-control" id="hotel_auth_landline" name="hotel_auth_landline'+counter+'" placeholder="" value="">'
                +'</div>'
                +'<div class="form-group">'
                +'<label for="hotel_auth_mobile">Mobile</label>'
                +'<input type="text" class="form-control" id="hotel_auth_mobile" name="hotel_auth_mobile'+counter+'" placeholder="" value="">'
                +'</div>'
                +'</div>'
                +'<br>'
                + '<span id="remove' + counter + '" class="remove btn-primary btn " style="margin-left:850px;">Remove Row</span>'
                + '</div>'
                + '</div>'
                +'</div>'
                );
                counter += 1;
                $('#contactdetails').val(counter);
                } else {

                alert('Limit Reached...');
                }
                $('#remove' + counter).prop('disabled', false);
                addDiv++;
    });
    $('.optionBox').on('click', '.re', function (e) {
        $(this).parent().remove();
        addDiv--;
        
        counter -= 1;
        $('#contactdetails').val(counter);
        $('#remove' + counter).prop('disabled', false);
        $(".hotel_auth_name").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'hotel_auth_name' + name);

        });
        $(".hotel_auth_category").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'hotel_auth_category' + name);
        });
        $(".hotel_auth_designation").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'hotel_auth_designation' + name);
        });
        $(".hotel_auth_email").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'hotel_auth_email' + name);
        });
        $(".hotel_auth_landline").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'hotel_auth_landline' + name);
        });
        $(".hotel_auth_mobile").each(function (index) {
            var name = index + 1;
            $(this).attr('name', 'hotel_auth_mobile' + name);
        });

    });
});
//------------------End-----------------------------//
function getrates(id) {
    if(id==1){
       if(document.getElementById("extra_bed").checked == true) {
       $("#rate").show();
       $("#rate_field").val(1);
    } else {
       $("#rate").hide(); 
       $("#rate_field").val("");
    } 
    } else {
        
    if(document.getElementById("extra_bed_kids").checked == true) {
       $("#Kids_bed_rate").show();
       $("#rate_field_kids").val(1);
    } else {
       $("#Kids_bed_rate").hide(); 
       $("#rate_field_kids").val("");
    }
    }
}

    //------------------------Rate field-----------------//

    $(document).ready(function() {
        paid();
        paid1();
        paid2();
        paid3();
    });
    function paid(){
        if(document.getElementById("type1.2").checked == true) {
            $("#dv1").show();
        } else {
            $("#dv1").hide();
        }
    }
    function paid1(){
        if(document.getElementById("type2.2").checked == true) {
            $("#dv2").show();
        } else {
            $("#dv2").hide();
        }
    }
    function paid2(){
        if(document.getElementById("type3.2").checked == true) {
            $("#dv3").show();
        } else {
            $("#dv3").hide();
        }
    }
    function paid3(){
        if(document.getElementById("type4.2").checked == true) {
            $("#dv4").show();
        } else {
            $("#dv4").hide();
        }
    }

</script>

<script src="http://maps.google.com/maps/api/js?v=3.exp&signed in=true&libraries=places&sensor=false" type="text/javascript"></script>
<script type="text/javascript">
google.maps.event.addDomListener(window,'load',intilize);
function intilize(){
    var autocomplete=new google.maps.places.Autocomplete(document.getElementById('auto'));
    google.maps.event.AddListener(autocomplete,'place_changed',function() {
    var places=autocomplete.getPlace();
    
    });
};
 
 
$(document).ready(function() {
    paid();
$('a[href="#address"]').on('shown.bs.tab', function(e)     {        

    //alert('I am inside tab');
    // Load google map
    var map = new google.maps.Map( document.getElementById("gmap"),  {
        center: new google.maps.LatLng(0,0),
        zoom: 3,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: false,
        streetViewControl: false,
        mapTypeControl: false
       
    });
 

    
    $('input[name=search]').click(function() {
 
        var geocoder = new google.maps.Geocoder(); 
        geocoder.geocode({
                address : $('input[name=add]').val(), 
                region: 'no' 
            },
            function(results, status) {
                if (status.toLowerCase() == 'ok') {
                    // Get center
                    var coords = new google.maps.LatLng(
                        results[0]['geometry']['location'].lat(),
                        results[0]['geometry']['location'].lng()
                    );
                    
                    var elem = document.getElementById("lat");
                     elem.value = coords.lat();
                     var elem1 = document.getElementById("lng");
                    elem1.value = coords.lng(); 
                    map.setCenter(coords);
                    map.setZoom(18);
 
                    // Set marker also
                    marker = new google.maps.Marker({
                        position: coords, 
                        map: map, 
                        title: $('input[name=add]').val(),
                    });
 
                }
            }
        );
    });
});
 
});

 </script>

<link href="{{ URL::asset('assets/css/bootstrap.css')}}" rel="stylesheet">

<script type="text/javascript">
    $(function () {
        $('#datetimepicker3').datetimepicker({
                    format: 'LT'
        });
        $('#datetimepicker4').datetimepicker({
                    format: 'LT'
        }); 
        $('#datetimepicker5').datetimepicker({
                    format: 'LT'
        }); 
        $('#datetimepicker6').datetimepicker({
                    format: 'LT'
        });  
    });
</script>
@stop
@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Hotel\CreatePropertyInfoRequest', '#property-info-form') !!}
@stop
@section('after-scripts-end')

{!! Html::script('assets/js/btn.js') !!}
{!! Html::script('assets/js/profile.js') !!}
{!! JsValidator::formRequest('App\Http\Requests\User\UpdateDetailsRequest', '#details-form') !!}

@if (settings('2fa.enabled'))
{!! JsValidator::formRequest('App\Http\Requests\User\EnableTwoFactorRequest', '#two-factor-form') !!}
@endif
@stop
