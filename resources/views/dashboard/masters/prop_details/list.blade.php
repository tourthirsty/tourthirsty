@extends('dashboard.layouts.master')
@section('page-title', 'Hotel Details')
@section('page-header')
<h1>
Property Details
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li class="active">Property Details</li>
</ol>
@endsection
@section('content')
@include('partials.messages')

<div class="row tab-search">
    <div class="col-md-2 col-xs-2">
        <a href="{{ route('dashboard.propertydetails.create') }}" class="btn btn-success" id="add-user">
            <i class="glyphicon glyphicon-plus"></i>
           Add Details
        </a>
    </div>
    <div class="col-md-5 col-xs-3"></div>
    <!--<form method="GET" action="" accept-charset="UTF-8" id="users-form">
        <div class="col-md-2 col-xs-3">
        </div>
        <div class="col-md-3 col-xs-4">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" name="search" value="{{ Input::get('search') }}" placeholder="@lang('app.search_for_users')">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="search-users-btn">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    @if (Input::has('search') && Input::get('search') != '')
                        <a href="{{ route('master.locationTypes') }}" class="btn btn-danger" type="button" >
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    @endif
                </span>
            </div>
        </div>
    </form>-->
</div>


<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Details</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <div id="users-table-wrapper">
                    <table class="table table-hover table-striped">
                        <tbody>
                            <tr>
                               <th>No.of rooms</th>
								<th>Checkin Time</th>
								<th>Checkout Time</th>
								<th>No.of restaurants</th>
								<th>Room Service timing</th>
								<th>No.of bars</th>
								<th>Created</th>
								<th class="text-center">@lang('app.action')</th>
                            </tr>
                 
				@if (count($prop))
                @foreach ($prop as $property)
					<tr>
						<td>{{ $property->hotel_details_no_rooms}}</td>
						<td>{{ $property->hotel_details_checkin_time}}</td>
						<td>{{ $property->hotel_details_checkout_time}}</td>
						<td>{{ $property->hotel_details_no_restaurants}}</td>
						<td>{{ $property->hotel_details_roomservice_timing}}</td>
						<td>{{ $property->hotel_details_bar}}</td>
                                <td>{{ $property->created_at }}</td>
                                <td class="text-center">
                                    <!--<a href="#" class="btn btn-success btn-circle"
                                        title="" data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a>-->
                                    <a href="{{ route('dashboard.propertydetails.edit', $property->id) }}" class="btn btn-primary btn-circle edit" title="Edit Details"
                                        data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    <a href="{{ route('dashboard.propertydetails.destroy', $property->id) }}" class="btn btn-danger btn-circle" title="Delete details"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-method="DELETE"
                                        data-confirm-title="Please Confirm"
                                        data-confirm-text="Are you sure that you want to delete ?"
                                        data-confirm-delete="Yes Delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </a>
                                
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6"><em>No records found</em></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@stop


			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
              