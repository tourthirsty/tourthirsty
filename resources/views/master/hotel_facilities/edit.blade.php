@extends('dashboard.layouts.master')

@section('page-title', 'Edit Hotel Facility')

@section('page-header')
<h1>
    Edit Hotel Facility
    <small>Hotel Facility</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('master.hotelFacility') }}">>Hotel Facility</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => ['master.hotelFacility.update', $hotel_fac->id], 'method' => 'PUT', 'id' => 'hotel-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Hotel Facility Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="hotel_facility">Hotel Facility</label>
                    <input type="text" class="form-control" id="hotel_facility" name="hotel_facility" placeholder="AC Room" required="" value="{{$hotel_fac->hotel_fac_name }}">
                </div>
                <div class="form-group">
                    <label for="hotel_facility_desc">Description</label>
                    <textarea name="hotel_facility_desc" placeholder="Eg:The hill stations are high-altitude towns used, especially by European colonialists..." class="form-control" required="">{{$hotel_fac->hotel_fac_desc }}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Hotel_facility\UpdateRequest', '#hotel-form') !!}
@stop