@include('frontend.includes.header')
<section>

  <div class="banner">
    <div class="container">
      <div class="row">
        <div class="holidyblk">

          <?php //echo "<pre>"; print_r($locationtypes); ?>

          <div class="topblk">Customized Holidays</div>
          <div class="btmblk">
            <div class="innerblk">
              <div class="col-md-4 col-sm-4 col-xs-3 brdrgt">
                <div class="pull-left">Date of Travel</div>
                <div class="pull-right"><i class="fa fa-calendar"></i></div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-2 brdrgt">
                <div class="pull-left">No of Pax</div>
                <div class="pull-right arrow">
                  <div class="btn-group">
                     <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                          <span> <i class="fa fa-angle-down dropbtn"></i></span>
                     </button>
                      <ul class="dropdown-menu" role="menu">
                        
                          <li>
                              <div class="pull-left">Adults:</div>
                              <div class="pull-right dwnicn">
                                  <i class="fa fa-minus-square" aria-hidden="true"></i> 2 
                                  <i class="fa fa-plus-square" aria-hidden="true"></i>
                               </div>
                          </li>
                          <li>
                              <div class="pull-left">Children<br/>(age 2-11):</div>
                              <div class="pull-right dwnicn">
                                  <i class="fa fa-minus-square" aria-hidden="true"></i> 3 
                                  <i class="fa fa-plus-square" aria-hidden="true"></i>
                               </div>
                          </li>
                    </ul>
                  </div>
                </div>
              </div>
                <div class="col-md-2 col-sm-2 col-xs-3 brdrgt">
                    <div class="pull-left">No of Days</div>
                    <div class="pull-right arrow">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span> <i class="fa fa-angle-down dropbtn"></i></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                 <li><a href="#">2</a></li>
                                 <li><a href="#">3</a></li>
                                  <li><a href="#">4</a></li>
                                  <li><a href="#">5</a></li>
                             </ul>
                         </div>
                     </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-3 brdrgt">
                    <div class="pull-left">No of Rooms</div>
                    <div class="pull-right arrow">
                                    <div class="btn-group">
                                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                      <span> <i class="fa fa-angle-down dropbtn"></i></span>
                                  </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                        </ul>
                                 </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-1 search">
                    <span class="searchshw"><button>NEXT</button></span>
                    <span class="searchhide"><i class="fa fa-search"></i></span>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="test">
    <div class="container">
      <div class="row">
        <div class="tour">
          <div class="destnatn">
            <div class="titletxt">
                <span class="firstarrow"><img class="img-responsive" src="{{'assets/frontend/images/lftarw.png'}}"/></span>
                <span><h4>Pick the View, Find the <b> Tour Destinations</b></h4></span>
                <span class="lastarrow"><img class="img-responsive" src="{{'assets/frontend/images/rgtarw.png'}}"/></span>
            </div>
            <div class="locatns">
              <div class="col-md-3 col-sm-3 col-xs-6 lctnimg">
                  <img class="subimg img-responsive" src="{{'assets/frontend/images/kovalm.jpg'}}"/>
                  <img class="subbrd img-responsive" src="{{'assets/frontend/images/subbrd.jpg'}}"/>
                  <div class="btmbox">
                      <h6>KOVALAM BEACH</h6>
                      <div class="roundarw"><img src="{{'assets/frontend/images/arrobtm.png'}}"/></div>
                  </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6 lctnimg">
                  <img class="subimg img-responsive" src="{{'assets/frontend/images/munnar.jpg'}}"/>
                  <img class="subbrd img-responsive" src="{{'assets/frontend/images/subbrd.jpg'}}"/>
                  <div class="btmbox">
                      <h6>THEKKADY</h6>
                      <div class="roundarw"><img src="{{'assets/frontend/images/arrobtm.png'}}"/></div>
                  </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6 lctnimg">
                  <img class="subimg img-responsive" src="{{'assets/frontend/images/kumarakm.jpg'}}"/>
                  <img class="subbrd img-responsive" src="{{'assets/frontend/images/subbrd.jpg'}}"/>
                  <div class="btmbox">
                      <h6>KUMARAKOM RESORT</h6>
                      <div class="roundarw"><img src="{{'assets/frontend/images/arrobtm.png'}}"/></div>
                  </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6 lctnimg">
                  <img class="subimg img-responsive" src="{{'assets/frontend/images/thekkady.jpg'}}"/>
                  <img class="subbrd img-responsive" src="{{'assets/frontend/images/subbrd.jpg'}}"/>
                  <div class="btmbox">
                      <h6>MUNNAR HILL STATION</h6>
                      <div class="roundarw"><img src="{{'assets/frontend/images/arrobtm.png'}}"/></div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
   <div class="articleblk">
      <div class="container">
        <div class="row">
          <div class="col-md-9 col-sm-9 detailbx">
              <h5>Kerala Honeymoon Tour Destinations</h5>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum 
                  has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                  took a galley of type and scrambled it to make a type specimen book. It has survived not
                  only five centuries, but also the leap into electronic typesetting, remaining essentially
                  unchanged. It was popularised in the 1960s with the release of Letraset sheets containing 
                  Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker
                  including versions of Lorem Ipsum.</p>
              <div class="map"><img class="img-responsive" src="{{'assets/frontend/images/mapbook.png'}}"/></div>
          </div>
          <div class="col-md-3 col-sm-3 adblk no-padding-right">
            <div class="ad"><img class="img-responsive" src="{{'assets/frontend/images/add1.jpg'}}"/></div>
            <div class="ad"><img class="img-responsive" src="{{'assets/frontend/images/add2.jpg'}}"/></div>
          </div>
        </div>
     </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row">
      <div class="tourspots">

         @if (count($locationtypes))
          <div class="col-md-4 col-sm-4 col-xs-12 no-padding-right">
            <div class="spot">
              <img class="img-responsive" src="{{'assets/frontend/images/family.jpg'}}"/>
              <div class="btmhead col-md-12 col-sm-12">{{ $locationtypes['0']->loc_type_name }}</div>
            </div>
          </div>
        @else
        <div class="col-md-4 col-sm-4 col-xs-12 no-padding-right">
            <div class="spot">
              No Locations found !
            </div>
        </div>
        
        @endif
        

        @if (count($locationtypes))
        <div class="col-md-4 col-sm-4 col-xs-12 midblk">
          <div class="spot">
            <img class="img-responsive" src="{{'assets/frontend/images/beach.jpg'}}"/>
            <div class="btmhead col-md-12 col-sm-12">{{ $locationtypes['1']->loc_type_name }}</div></div>
          <div class="spot">
            <img class="img-responsive" src="{{'assets/frontend/images/hillstation.jpg'}}"/>
            <div class="btmhead col-md-12 col-sm-12">{{ $locationtypes['2']->loc_type_name }}</div>
          </div>
        </div>
        @else
        <div class="col-md-4 col-sm-4 col-xs-12 no-padding-right">
            <div class="spot">
              No Locations found !
            </div>
        </div>
        
        @endif
        @if (count($locationtypes))
        <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
            <div class="spot">
              <img class="img-responsive" src="{{'assets/frontend/images/romantic.jpg'}}"/>
              <div class="btmhead col-md-12 col-sm-12">{{ $locationtypes['3']->loc_type_name }}</div>
            </div>
        </div>
        @else
        <div class="col-md-4 col-sm-4 col-xs-12 no-padding-right">
            <div class="spot">
              No Locations found !
            </div>
        </div>
        
        @endif
      </div>
    </div>
  </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="btmad"><img class="img-responsive" src="{{'assets/frontend/images/btmad.jpg'}}"/></div>
        </div>
    </div>
</section>
@include('frontend.includes.footer')