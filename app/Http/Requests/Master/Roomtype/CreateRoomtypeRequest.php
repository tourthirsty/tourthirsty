<?php

namespace App\Http\Requests\Master\Roomtype;
use App\Http\Requests\Request;
use App\Room_type;
class CreateRoomtypeRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'room_type_name' => 'required',
		];
	}
}
