<?php

namespace App\Repositories\Master;

use App\Location_types;

interface LocationtypesRepository {
    
    /**
	 * Paginate registered users.
	 *
	 * @param $perPage
	 * @param null $search
	 * @param null $status
	 * @return mixed
	 */
	public function paginate($perPage, $search = null, $status = null);
        /**
	 * Find user by its id.
	 *
	 * @param $id
	 * @return null|User
	 */
	public function find($id);
        
        /**
	 * Create new user.
	 *
	 * @param array $data
	 * @return mixed
	 */
	public function create(array $data);

	/**
	 * Update user specified by it's id.
	 *
	 * @param $id
	 * @param array $data
	 * @return mixed
	 */
	public function update($id, array $data);

	/**
	 * Delete user with provided id.
	 *
	 * @param $id
	 * @return mixed
	 */
	public function delete($id);

    
}