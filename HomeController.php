<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use DB;
use App\Cms as Cms;
use App\Location as Locations;
use App\Locationtype as Loctype;
use App\Places_to_visit as Places;
use App\Images_place as Placeimages;


class HomeController extends Controller {

	public function home() {
        $locationtypes = DB::table('locationtypes')->take(4)->get();


        /*$place='';
        foreach ($locations as $key => $value) {
            $loc_type= $value->loc_type;
            $id= $value->id;
            $pstr = explode(",",$loc_type);
            $place = Places::where('places_to_visit.loc_id_fk', $id)
                        ->leftJoin('images_place', 'images_place.place_id_fk', '=', 'places_to_visit.id')
                        ->select('images_place.url','locationtypes.loc_type_name' )
                        ->groupBy('locationtypes.id')
                        ->get();
            
        }
        foreach ($pstr as $key => $value) {
                echo $value.'<br>';
                $place='';
                $place = Places::where('places_to_visit.loc_id_fk', $id)
                        ->leftJoin('images_place', 'images_place.place_id_fk', '=', 'places_to_visit.id')
                        ->select('images_place.url','locationtypes.loc_type_name' )
                        ->groupBy('places_to_visit.id')
                        ->get();
                //die;
                //print_r($place);

            }*/

		return view('frontend.index')->with('locationtypes',$locationtypes);
	}

	public function showcms($id)
    {
        //Get results by targeting id
        $cms = Cms::find($id);
 
        //Redirecting to index.blade.php with $cms variable
        return view('frontend.cms')->with('cms',$cms);
    }



}
