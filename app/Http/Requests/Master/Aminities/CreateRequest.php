<?php

namespace App\Http\Requests\Master\Aminities;

use App\Http\Requests\Request;
use App\Room_aminity;

class CreateRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'room_amn_name' => 'required|min:3',
			'room_amn_desc' => 'required|min:6',
		    ];
	}
}
