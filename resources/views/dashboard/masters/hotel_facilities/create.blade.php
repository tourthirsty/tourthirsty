@extends('dashboard.layouts.master')

@section('page-title', 'Add Hotel Facility')

@section('page-header')
<h1>
    Add Hotel Facility
    <small>Hotel Facility</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('master.hotelFacility') }}">>Hotel Facility</a></li>
    <li class="active">Add New</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => 'master.hotelFacility.store', 'files' => true, 'id' => 'hotel-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Hotel Facilities</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="hotel_facility">Hotel Facility</label>
                    <input type="text" class="form-control" id="hotel_facility" name="hotel_facility" placeholder="AC Room" required="">
                </div>
                <div class="form-group">
                    <label for="hotel_facility_desc">Description</label>
                    <textarea name="hotel_facility_desc" placeholder="Eg:The hill stations are high-altitude towns used, especially by European colonialists..." class="form-control" required=""></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Hotel_facility\CreateRequest', '#hotel-form') !!}
@stop
