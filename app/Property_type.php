<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property_type extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'property_types';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'prop_type_name', 'prop_type_desc', 
	];
}
