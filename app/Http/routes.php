<?php
use App\Room_details as Room_details;
use App\Availability as Available;

Route::any('/', array( 'as' => 'home', 'uses' => 'Frontend\HomeController@home' ));
Route::group(['middleware' => 'web'], function () {
	Route::get('login', 'Auth\AuthController@getLogin');
	Route::post('login', 'Auth\AuthController@postLogin');

	Route::get('logout', [
		'as' => 'auth.logout',
		'uses' => 'Auth\AuthController@getLogout',
	]);

	if (settings('reg_enabled')) {
		Route::get('register', 'Auth\AuthController@getRegister');
		Route::post('register', 'Auth\AuthController@postRegister');
		Route::get('register/confirmation/{token}', [
			'as' => 'register.confirm-email',
			'uses' => 'Auth\AuthController@confirmEmail',
		]);
		Route::get('Hotelregister', 'Auth\AuthController@getHotelRegister');
		Route::post('Auth/postHotelRegister', [
                                'as' => 'auth.postHotelRegister',
                                'uses' => 'Auth\AuthController@postHotelRegister',
                        ]);
	}

	if (settings('forgot_password')) {
		Route::get('password/remind', 'Auth\PasswordController@forgotPassword');
		Route::post('password/remind', 'Auth\PasswordController@sendPasswordReminder');
		Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
		Route::post('password/reset', 'Auth\PasswordController@postReset');
	}

	if (settings('2fa.enabled')) {
		Route::get('auth/two-factor-authentication', [
			'as' => 'auth.token',
			'uses' => 'Auth\AuthController@getToken',
		]);

		Route::post('auth/two-factor-authentication', [
			'as' => 'auth.token.validate',
			'uses' => 'Auth\AuthController@postToken',
		]);
	}

	Route::get('auth/{provider}/login', [
		'as' => 'social.login',
		'uses' => 'Auth\SocialAuthController@redirectToProvider',
		'middleware' => 'social.login',
	]);

	Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');

	Route::get('auth/twitter/email', 'Auth\SocialAuthController@getTwitterEmail');
	Route::post('auth/twitter/email', 'Auth\SocialAuthController@postTwitterEmail');

});

Route::group(['middleware' => 'web'], function () {
	Route::localizedGroup(function () {

		Route::get('/', 'Frontend\HomeController@home')->name('frontend.index');
		Route::group(['namespace' => 'Dashboard', 'prefix' => 'dashboard'], function () {
			Route::get('/', [
				'as' => 'dashboard',
				'uses' => 'DashboardController@index',
			]);

			Route::get('profile', [
				'as' => 'profile',
				'uses' => 'ProfileController@index',
			]);

			Route::get('profile/activity', [
				'as' => 'profile.activity',
				'uses' => 'ProfileController@activity',
			]);

			Route::put('profile/details/update', [
				'as' => 'profile.update.details',
				'uses' => 'ProfileController@updateDetails',
			]);

			Route::post('profile/avatar/update', [
				'as' => 'profile.update.avatar',
				'uses' => 'ProfileController@updateAvatar',
			]);

			Route::post('profile/avatar/update/external', [
				'as' => 'profile.update.avatar-external',
				'uses' => 'ProfileController@updateAvatarExternal',
			]);

			Route::put('profile/login-details/update', [
				'as' => 'profile.update.login-details',
				'uses' => 'ProfileController@updateLoginDetails',
			]);

			Route::put('profile/social-networks/update', [
				'as' => 'profile.update.social-networks',
				'uses' => 'ProfileController@updateSocialNetworks',
			]);

			Route::post('profile/two-factor/enable', [
				'as' => 'profile.two-factor.enable',
				'uses' => 'ProfileController@enableTwoFactorAuth',
			]);

			Route::post('profile/two-factor/disable', [
				'as' => 'profile.two-factor.disable',
				'uses' => 'ProfileController@disableTwoFactorAuth',
			]);

			Route::get('profile/sessions', [
				'as' => 'profile.sessions',
				'uses' => 'ProfileController@sessions',
			]);

			Route::delete('profile/sessions/{session}/invalidate', [
				'as' => 'profile.sessions.invalidate',
				'uses' => 'ProfileController@invalidateSession',
			]);

			Route::get('user', [
				'as' => 'user.list',
				'uses' => 'UsersController@index',
			]);

			Route::get('user/create', [
				'as' => 'user.create',
				'uses' => 'UsersController@create',
			]);

			Route::post('user/create', [
				'as' => 'user.store',
				'uses' => 'UsersController@store',
			]);

			Route::get('user/{user}/show', [
				'as' => 'user.show',
				'uses' => 'UsersController@view',
			]);

			Route::get('user/{user}/edit', [
				'as' => 'user.edit',
				'uses' => 'UsersController@edit',
			]);

			Route::put('user/{user}/update/details', [
				'as' => 'user.update.details',
				'uses' => 'UsersController@updateDetails',
			]);

			Route::put('user/{user}/update/login-details', [
				'as' => 'user.update.login-details',
				'uses' => 'UsersController@updateLoginDetails',
			]);

			Route::delete('user/{user}/delete', [
				'as' => 'user.delete',
				'uses' => 'UsersController@delete',
			]);

			Route::post('user/{user}/update/avatar', [
				'as' => 'user.update.avatar',
				'uses' => 'UsersController@updateAvatar',
			]);

			Route::post('user/{user}/update/avatar/external', [
				'as' => 'user.update.avatar.external',
				'uses' => 'UsersController@updateAvatarExternal',
			]);

			Route::post('user/{user}/update/social-networks', [
				'as' => 'user.update.socials',
				'uses' => 'UsersController@updateSocialNetworks',
			]);

			Route::get('user/{user}/sessions', [
				'as' => 'user.sessions',
				'uses' => 'UsersController@sessions',
			]);

			Route::delete('user/{user}/sessions/{session}/invalidate', [
				'as' => 'user.sessions.invalidate',
				'uses' => 'UsersController@invalidateSession',
			]);

			Route::post('user/{user}/two-factor/enable', [
				'as' => 'user.two-factor.enable',
				'uses' => 'UsersController@enableTwoFactorAuth',
			]);

			Route::post('user/{user}/two-factor/disable', [
				'as' => 'user.two-factor.disable',
				'uses' => 'UsersController@disableTwoFactorAuth',
			]);

			Route::get('role', [
				'as' => 'role.index',
				'uses' => 'RolesController@index',
			]);

			Route::get('role/create', [
				'as' => 'role.create',
				'uses' => 'RolesController@create',
			]);

			Route::post('role/store', [
				'as' => 'role.store',
				'uses' => 'RolesController@store',
			]);

			Route::get('role/{role}/edit', [
				'as' => 'role.edit',
				'uses' => 'RolesController@edit',
			]);

			Route::put('role/{role}/update', [
				'as' => 'role.update',
				'uses' => 'RolesController@update',
			]);

			Route::delete('role/{role}/delete', [
				'as' => 'role.delete',
				'uses' => 'RolesController@delete',
			]);

			Route::post('permission/save', [
				'as' => 'dashboard.permission.save',
				'uses' => 'PermissionsController@saveRolePermissions',
			]);

			Route::resource('permission', 'PermissionsController');
			
			Route::resource('proptype', 'ProptypeController');
			
			Route::get('proptype', [
				'as' => 'proptype.index',
				'uses' => 'ProptypeController@index',
			]);
			Route::get('create', [
				'as' => 'proptype.create',
				'uses' => 'ProptypeController@create',
			]);
			Route::post('store', [
				'as' => 'proptype.store',
				'uses' => 'ProptypeController@store',
			]);
			Route::get('edit/{id}', [
				'as' => 'proptype.edit',
				'uses' => 'ProptypeController@edit',
			]);
			Route::put('update/{id}', [
				'as' => 'proptype.update',
				'uses' => 'ProptypeController@update',
			]);
			Route::delete('destroy/{id}', [
				'as' => 'proptype.destroy',
				'uses' => 'ProptypeController@destroy',
			]);
			
			
			Route::resource('aminities', 'AminitiesController');
			Route::get('aminities', [
				'as' => 'aminities.index',
				'uses' => 'AminitiesController@index',
			]);
			Route::get('create', [
				'as' => 'aminities.create',
				'uses' => 'AminitiesController@create',
			]);
			Route::post('store', [
				'as' => 'aminities.store',
				'uses' => 'AminitiesController@store',
			]);
			Route::get('edit/{id}', [
				'as' => 'aminities.edit',
				'uses' => 'AminitiesController@edit',
			]);
			Route::put('update/{id}', [
				'as' => 'aminities.update',
				'uses' => 'AminitiesController@update',
			]);
			Route::delete('destroy/{id}', [
				'as' => 'aminities.destroy',
				'uses' => 'AminitiesController@destroy',
			]);
			
			
			Route::resource('roomtype', 'RoomtypeController');
			
			Route::get('roomtype', [
				'as' => 'roomtype.index',
				'uses' => 'RoomtypeController@index',
			]);
			Route::get('create', [
				'as' => 'roomtype.create',
				'uses' => 'RoomtypeController@create',
			]);
			Route::post('store', [
				'as' => 'roomtype.store',
				'uses' => 'RoomtypeController@store',
			]);
			Route::get('edit/{id}', [
				'as' => 'roomtype.edit',
				'uses' => 'RoomtypeController@edit',
			]);
			Route::put('update/{id}', [
				'as' => 'roomtype.update',
				'uses' => 'RoomtypeController@update',
			]);
			Route::delete('destroy/{id}', [
				'as' => 'roomtype.destroy',
				'uses' => 'RoomtypeController@destroy',
			]);



			Route::resource('meal', 'MealController');
			Route::get('meal', [
				'as' => 'meal.index',
				'uses' => 'MealController@index',
			]);
			Route::get('create', [
				'as' => 'meal.create',
				'uses' => 'MealController@create',
			]);
			Route::post('store', [
				'as' => 'meal.store',
				'uses' => 'MealController@store',
			]);
			Route::get('edit/{id}', [
				'as' => 'meal.edit',
				'uses' => 'MealController@edit',
			]);
			Route::put('update/{id}', [
				'as' => 'meal.update',
				'uses' => 'MealController@update',
			]);
			Route::delete('destroy/{id}', [
				'as' => 'meal.destroy',
				'uses' => 'MealController@destroy',
			]);


			Route::resource('additionalamin', 'AdditionalaminController');
			Route::get('additionalamin', [
				'as' => 'additionalamin.index',
				'uses' => 'AdditionalaminController@index',
			]);
			Route::get('create', [
				'as' => 'additionalamin.create',
				'uses' => 'AdditionalaminController@create',
			]);
			Route::post('store', [
				'as' => 'additionalamin.store',
				'uses' => 'AdditionalaminController@store',
			]);
			Route::get('edit/{id}', [
				'as' => 'additionalamin.edit',
				'uses' => 'AdditionalaminController@edit',
			]);
			Route::put('update/{id}', [
				'as' => 'additionalamin.update',
				'uses' => 'AdditionalaminController@update',
			]);
			Route::delete('destroy/{id}', [
				'as' => 'additionalamin.destroy',
				'uses' => 'AdditionalaminController@destroy',
			]);
			

			Route::get('settings', [
				'as' => 'settings.general',
				'uses' => 'SettingsController@general',
				'middleware' => 'permission:settings.general',
			]);

			Route::post('settings/general', [
				'as' => 'settings.general.update',
				'uses' => 'SettingsController@update',
				'middleware' => 'permission:settings.general',
			]);

			Route::get('settings/auth', [
				'as' => 'settings.auth',
				'uses' => 'SettingsController@auth',
				'middleware' => 'permission:settings.auth',
			]);

			Route::post('settings/auth', [
				'as' => 'settings.auth.update',
				'uses' => 'SettingsController@update',
				'middleware' => 'permission:settings.auth',
			]);

			if (env('AUTHY_KEY')) {
				Route::post('settings/auth/2fa/enable', [
					'as' => 'settings.auth.2fa.enable',
					'uses' => 'SettingsController@enableTwoFactor',
					'middleware' => 'permission:settings.auth',
				]);

				Route::post('settings/auth/2fa/disable', [
					'as' => 'settings.auth.2fa.disable',
					'uses' => 'SettingsController@disableTwoFactor',
					'middleware' => 'permission:settings.auth',
				]);
			}

			Route::post('settings/auth/registration/captcha/enable', [
				'as' => 'settings.registration.captcha.enable',
				'uses' => 'SettingsController@enableCaptcha',
				'middleware' => 'permission:settings.auth',
			]);

			Route::post('settings/auth/registration/captcha/disable', [
				'as' => 'settings.registration.captcha.disable',
				'uses' => 'SettingsController@disableCaptcha',
				'middleware' => 'permission:settings.auth',
			]);

			Route::get('settings/notifications', [
				'as' => 'settings.notifications',
				'uses' => 'SettingsController@notifications',
				'middleware' => 'permission:settings.notifications',
			]);

			Route::post('settings/notifications', [
				'as' => 'settings.notifications.update',
				'uses' => 'SettingsController@update',
				'middleware' => 'permission:settings.notifications',
			]);

			Route::get('activity', [
				'as' => 'activity.index',
				'uses' => 'ActivityController@index',
			]);

			Route::get('activity/user/{user}/log', [
				'as' => 'activity.user',
				'uses' => 'ActivityController@userActivity',
			]);

			Route::group([
				'prefix' => 'log-viewer',
				'middleware' => 'role:Admin',
			], function () {
				Route::get('/', [
					'as' => 'log-viewer::dashboard',
					'uses' => '\Arcanedev\LogViewer\Http\Controllers\LogViewerController@index',
				]);
				Route::group([
					'prefix' => 'logs',
				], function () {
					Route::get('/', [
						'as' => 'log-viewer::logs.list',
						'uses' => '\Arcanedev\LogViewer\Http\Controllers\LogViewerController@listLogs',
					]);
					Route::delete('delete', [
						'as' => 'log-viewer::logs.delete',
						'uses' => '\Arcanedev\LogViewer\Http\Controllers\LogViewerController@delete',
					]);
				});
				Route::group([
					'prefix' => '{date}',
				], function () {
					Route::get('/', [
						'as' => 'log-viewer::logs.show',
						'uses' => '\Arcanedev\LogViewer\Http\Controllers\LogViewerController@show',
					]);
					Route::get('download', [
						'as' => 'log-viewer::logs.download',
						'uses' => '\Arcanedev\LogViewer\Http\Controllers\LogViewerController@download',
					]);
					Route::get('{level}', [
						'as' => 'log-viewer::logs.filter',
						'uses' => '\Arcanedev\LogViewer\Http\Controllers\LogViewerController@showByLevel',
					]);
				});
			});
		
                         Route::get('hotel/createHotels/{id}', [
                            'as' => 'hotel.createHotels',
                            'uses' => 'HotelController@createHotels',
                        ]);
                         Route::get('hotel/hotels/', [
                            'as' => 'hotel.hotels',
                            'uses' => 'HotelController@hotels',
                        ]);
                         Route::get('hotel/hotels/{id}', [
                            'as' => 'hotel.hotels',
                            'uses' => 'HotelController@hotels',
                        ]);
                         Route::delete('hotel/destroyhotel/{id}', [
                            'as' => 'hotel.destroyhotel',
                            'uses' => 'HotelController@destroyhotel',
                        ]);
                        Route::get('hotel/allHotels/0', [
                            'as' => 'hotel.allHotels',
                            'uses' => 'HotelController@allHotels',
                        ]);
                        Route::get('hotel/allGroups/', [
                            'as' => 'hotel.allGroups',
                            'uses' => 'HotelController@allGroups',
                        ]);
			Route::get('hotel/rateCalandar/', [
				'as' => 'hotel.rateCalandar',
				'uses' => 'HotelController@rateCalandar',
			    ]);
			    Route::get('hotel/rateinCalandar/{id}', [
				'as' => 'hotel.rateinCalandar',
				'uses' => 'HotelController@rateinCalandar',
			    ]);
			    Route::get('hotel/availabilityCalandar/', [
				'as' => 'hotel.availabilityCalandar',
				'uses' => 'HotelController@availabilityCalandar',
			    ]);
			    Route::get('hotel/availabilityinCalandar/{id}', [
				'as' => 'hotel.availabilityinCalandar',
				'uses' => 'HotelController@availabilityinCalandar',
			    ]);
			    Route::post('hotel/availabilityCalandarDetails/', [
				'as' => 'hotel.availabilityCalandarDetails',
				'uses' => 'HotelController@availabilityCalandarDetails',
			    ]);
			    Route::post('hotel/rateCalandarDetails/', [
				'as' => 'hotel.rateCalandarDetails',
				'uses' => 'HotelController@rateCalandarDetails',
			    ]);
                        Route::post('hotel/storeHotels', [
				'as' => 'dashboard.hotel.storeHotels',
				'uses' => 'HotelController@storeHotels',
			]);
                        Route::delete('hotel/{user}/delete', [
                            'as' => 'hotel.delete',
                            'uses' => 'HotelController@delete',
                        ]);
                        Route::resource('fhrai', 'FhraiclassificationController');
                        Route::get('/fhrai', [
                            'as' => 'dashboard.fhrai',
                            'uses' => 'FhraiclassificationController@index',
                        ]);
                        Route::resource('facility', 'FacilityController');
                        Route::get('/facility', [
                            'as' => 'dashboard.facility',
                            'uses' => 'FacilityController@index',
                        ]);
                        Route::resource('locationType', 'LocationtypeController');
                        Route::get('/locationType', [
                            'as' => 'dashboard.locationType',
                            'uses' => 'LocationtypeController@index',
                        ]);
                        Route::resource('attraction_types', 'Attraction_typesController');
                        Route::get('/attraction_types', [
                            'as' => 'dashboard.attraction_types',
                            'uses' => 'Attraction_typesController@index',
                        ]);
                        Route::resource('activities', 'ActivitiesController');
                        Route::get('/activities', [
                            'as' => 'dashboard.activities',
                            'uses' => 'ActivitiesController@index',
                        ]); 
                        Route::get('location/getPlacetovisit/{id}', [
                            'as' => 'location.placetovisit',
                            'uses' => 'LocationController@getPlacetovisit',
                        ]);
			Route::get('location/createPlacetovisit/{id}', [
                            'as' => 'location.createplace',
                            'uses' => 'LocationController@createPlacetovisit',
                        ]);
                        Route::post('location/storePlacetovisit', [
                            'as' => 'location.storeplace',
                            'uses' => 'LocationController@storePlacetovisit',
                        ]);
			Route::get('location/editPlacetovisit/{id}', [
                            'as' => 'location.editplace',
                            'uses' => 'LocationController@editPlacetovisit',
                        ]);
                        Route::post('location/updatePlacetovisit', [
				'as' => 'location.updateplace',
				'uses' => 'LocationController@updatePlacetovisit',
			    ]);
			    Route::post('location/deletePlaceimage', [
				'as' => 'location/deleteplaceimage',
				'uses' => 'LocationController@deletePlaceimage',
			    ]);
                        Route::get('location/showPlacetovisit/{id}', [
				'as' => 'location.showplace',
				'uses' => 'LocationController@showPlacetovisit',
			    ]);
			Route::delete('location/deleteNearplace/{id}', [
				'as' => 'location.deletenearplace',
				'uses' => 'LocationController@deletenearplace',
			    ]);
                        Route::get('hotel/listgroup/{id}', [
				'as' => 'hotel.listgroup',
				'uses' => 'HotelController@listgroup',
			]);
                        Route::get('hotel/creategroup/{id}', [
				'as' => 'hotel.creategroup',
				'uses' => 'HotelController@creategroup',
			]);
                        Route::post('hotel/groupstore', [
				'as' => 'hotel.groupstore',
				'uses' => 'HotelController@groupstore',
			]);
			Route::get('hotel/editgroup/{id}', [
				'as' => 'hotel.editgroup',
				'uses' => 'HotelController@editgroup',
			]);
			Route::put('hotel/updategroup/{id}', [
				'as' => 'hotel.updategroup',
				'uses' => 'HotelController@updategroup',
			]);
			Route::delete('hotel/destroygroup/{id}', [
				'as' => 'hotel.destroygroup',
				'uses' => 'HotelController@destroygroup',
			]);


			Route::get('location/index', [
                'as' => 'location.index',
                'uses' => 'LocationController@index',
            ]);
			Route::get('location/create', [
				'as' => 'location.create',
				'uses' => 'LocationController@create',
			]);
			Route::post('location/store', [
				'as' => 'location.store',
				'uses' => 'LocationController@store',
			]);
			Route::get('location/edit/{id}', [
				'as' => 'location.edit',
				'uses' => 'LocationController@edit',
			]);
			Route::post('location/update/{id}', [
				'as' => 'location.update',
				'uses' => 'LocationController@update',
			]);
			Route::delete('location/destroy/{id}', [
				'as' => 'location.destroy',
				'uses' => 'LocationController@destroy',
			]);
			Route::get('availability/index', [
					'as' => 'availability.index',
					'uses' => 'AvailabilityController@index',
			]);
			Route::get('rate/index', [
					'as' => 'rate.index',
					'uses' => 'RateController@index',
			]);
			Route::get('/getname',function(){
	
				$room=Input::get('hotel_name');
				$room_select= Room_details::select('room_type','id')->where('hotel_id_fk', '=' , $room)->get();
				return Response::json($room_select);

			});
			Route::get('/getcount',function(){
	
				$room=Input::get('hotel_name');
				
				$Avail=Available::where('hotel_id_fk', '=', $room)->get();
				if(count($Avail))
				{
					return Response::json($Avail);
				}
				else 
				{
       			$room_select = Room_details::where('hotel_id_fk', '=' , $room)->get();
       			return Response::json($room_select);
       			}

			});
			Route::get('/getroom',function()
			{
				$room=Input::get('hotels');
				$room1=Input::get('rooms');
				$view=DB::table('rates')
                ->where('rates.hotel_id_fk', $room)->where('rates.room_type', $room1)
                ->join('hotels', 'hotels.id' , '=' , 'rates.hotel_id_fk')
                ->join('room_details','room_details.id', '=' , 'rates.room_type')
                ->select('rates.*','hotels.hotel_name','room_details.room_type')
                ->get();
                $spec_view=DB::table('special_rates')
                ->where('special_rates.hotel_id_fk', $room)->where('special_rates.room_type', $room1)
                
                ->join('hotels', 'hotels.id' , '=' , 'special_rates.hotel_id_fk')
                ->join('room_details','room_details.id', '=' , 'special_rates.room_type')

                ->select('special_rates.*','hotels.hotel_name','room_details.room_type')
                ->get();
                $total[0]['Rates']=$view;
                $total[0]['Sp_rates']=$spec_view;
                return Response::json($total);
				
			});
			Route::post('availability/store', [
				'as' => 'availability.store',
				'uses' => 'AvailabilityController@store',
			]);

			Route::post('rate/storecaldetails', [
				'as' => 'rate.storecaldetails',
				'uses' => 'RateController@storecaldetails',
			]);
			Route::delete('rate/norcaldestroy/{id}', [
				'as' => 'rate.norcaldestroy',
				'uses' => 'RateController@norcaldestroy',
			]);
			Route::delete('rate/speccaldestroy/{id}', [
				'as' => 'rate.speccaldestroy',
				'uses' => 'RateController@speccaldestroy',
			]);
			Route::get('rate/norcaledit/{id}', [
				'as' => 'rate.norcaledit',
				'uses' => 'RateController@norcaledit',
			]);
			Route::get('rate/speccaledit/{id}', [
				'as' => 'rate.speccaledit',
				'uses' => 'RateController@speccaledit',
			]);
			Route::put('rate/norcalupdate/{id}', [
				'as' => 'rate.norcalupdate',
				'uses' => 'RateController@norcalupdate',
			]);
			Route::put('rate/speccalupdate/{id}', [
				'as' => 'rate.speccalupdate',
				'uses' => 'RateController@speccalupdate',
			]);
			Route::post('rate/storeExtrabed', [
				'as' => 'rate.storeExtrabed',
				'uses' => 'RateController@storeExtrabed',
			]);
			Route::post('rate/storeExtrakidsbed', [
				'as' => 'rate.storeExtrakidsbed',
				'uses' => 'RateController@storeExtrakidsbed',
			]);
		
			

			Route::resource('hotel', 'HotelController');
            Route::get('/hotel', [
                'as' => 'dashboard.hotel',
                'uses' => 'HotelController@index',
            ]);
                        Route::post('hotel/storePropertyinfo', [
				'as' => 'hotel.storePropertyinfo',
				'uses' => 'HotelController@storePropertyinfo',
			]);
			Route::post('hotel/storeHotelfacility', [
				'as' => 'hotel.storeHotelfacility',
				'uses' => 'HotelController@storeHotelfacility',
			]);
			Route::post('hotel/storeExtrabed', [
				'as' => 'hotel.storeExtrabed',
				'uses' => 'HotelController@storeExtrabed',
			]);
			Route::post('hotel/storeExtrakidsbed', [
				'as' => 'hotel.storeExtrakidsbed',
				'uses' => 'HotelController@storeExtrakidsbed',
			]);
			Route::post('hotel/propdetailstore', [
				'as' => 'hotel.propdetailstore',
				'uses' => 'HotelController@propdetailstore',
			]);
			Route::post('hotel/updatepropdetails', [
				'as' => 'hotel.updatepropdetails',
				'uses' => 'HotelController@updatepropdetails',
			]);
			Route::post('hotel/storepropdetailsdesc', [
				'as' => 'hotel.storepropdetailsdesc',
				'uses' => 'HotelController@storepropdetailsdesc',
			]);
			Route::post('hotel/updatepropdetailsdesc', [
				'as' => 'hotel.updatepropdetailsdesc',
				'uses' => 'HotelController@updatepropdetailsdesc',
			]);
			Route::post('hotel/storehotelpolicy', [
				'as' => 'hotel.storehotelpolicy',
				'uses' => 'HotelController@storehotelpolicy',
			]);
			Route::post('hotel/updatehotelpolicy', [
				'as' => 'hotel.updatehotelpolicy',
				'uses' => 'HotelController@updatehotelpolicy',
			]);
			Route::post('hotel/storebankdetails', [
				'as' => 'hotel.storebankdetails',
				'uses' => 'HotelController@storebankdetails',
			]);
			Route::post('hotel/updatebankdetails', [
				'as' => 'hotel.updatebankdetails',
				'uses' => 'HotelController@updatebankdetails',
			]);
			Route::post('hotel/storecontactdetails', [
				'as' => 'hotel.storecontactdetails',
				'uses' => 'HotelController@storecontactdetails',
			]);
			Route::post('hotel/updatecontactdetails', [
				'as' => 'hotel.updatecontactdetails',
				'uses' => 'HotelController@updatecontactdetails',
			]);
			Route::post('hotel/storeHoteltype', [
				'as' => 'hotel.storeHoteltype',
				'uses' => 'HotelController@storeHoteltype',
			]);
			Route::post('hotel/storetechdetails', [
				'as' => 'hotel.storetechdetails',
				'uses' => 'HotelController@storetechdetails',
			]);
            Route::post('hotel/storeHotelroomamenities', [
				'as' => 'hotel.storeHotelroomamenities',
				'uses' => 'HotelController@storeHotelroomamenities',
			]);
			Route::post('hotel/storemeals', [
				'as' => 'hotel.storemeals',
				'uses' => 'HotelController@storemeals',
			]);
			Route::post('hotel/storepropinfoaddress', [
				'as' => 'hotel.storepropinfoaddress',
				'uses' => 'HotelController@storepropinfoaddress',
			]);
			Route::post('hotel/storecaldetails', [
					'as' => 'hotel.storecaldetails',
					'uses' => 'HotelController@storecaldetails',
				]);
				Route::delete('hotel/norcaldestroy/{id}', [
					'as' => 'hotel.norcaldestroy',
					'uses' => 'HotelController@norcaldestroy',
				]);
				Route::delete('hotel/speccaldestroy/{id}', [
					'as' => 'hotel.speccaldestroy',
					'uses' => 'HotelController@speccaldestroy',
				]);
				Route::get('hotel/norcaledit/{id}', [
					'as' => 'hotel.norcaledit',
					'uses' => 'HotelController@norcaledit',
				]);
				Route::get('hotel/speccaledit/{id}', [
					'as' => 'hotel.speccaledit',
					'uses' => 'HotelController@speccaledit',
				]);
				Route::put('hotel/norcalupdate/{id}', [
					'as' => 'hotel.norcalupdate',
					'uses' => 'HotelController@norcalupdate',
				]);
				Route::put('hotel/speccalupdate/{id}', [
					'as' => 'hotel.speccalupdate',
					'uses' => 'HotelController@speccalupdate',
				]);
			
                        
                });
	});

});
Route::group(['middleware' => 'web'], function () {
    Route::localizedGroup(function () {
        Route::group(['namespace' => 'Master', 'prefix' => 'master'], function () {

            Route::resource('hotelFacility', 'HotelfacilityController');
            Route::get('/hotelFacility', [
                'as' => 'master.hotelFacility',
                'uses' => 'HotelfacilityController@index',
            ]);
        
         
        });
    });
});
