@extends('dashboard.layouts.master')

@section('page-title', 'Add Hotel User')

@section('page-header')

<h1>
    Add Hotel User
    <small>Hotel User</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.hotel') }}">>Hotel User</a></li>
    <li class="active">Add New</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => 'dashboard.hotel.store', 'files' => true, 'id' => 'hotel-form']) !!}

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">@lang('app.user_details')</div>
            <div class="panel-body">
                        
                        <div class="form-group">
                            <label for="full_name">Full Name</label>
                            <input type="text" class="form-control" id="first_name" name="full_name" placeholder="Full Name" value="">
                        </div>
<!--                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="@lang('app.last_name')" value="">
                        </div>-->
                        <div class="form-group">
                            <label for="phone">@lang('app.phone')</label>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="@lang('app.phone')" value="">
                        </div>
                        <br>
                        <div class="form-group">
                             <label class="radio-inline">
                                <input type="radio" name="optiontype" value="1"> Group
                              </label>
                              <label class="radio-inline">
                                <input type="radio" name="optiontype" value="2"> Hotel
                              </label>
                        </div>
            </div>

        </div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">@lang('app.login_details')</div>
            <div class="panel-body">
                <div class="form-group">
                           <label for="email">@lang('app.email')</label>
                           <input type="email" class="form-control" id="email"
                           name="email" placeholder="@lang('app.email')" value="">
                </div>
                <!--<div class="form-group">
                    <label for="username">@lang('app.username')</label>
                    <input type="text" class="form-control" id="username" placeholder="(@lang('app.optional'))"
                           name="username" value="">
                </div>-->
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password"
                           name="password">
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Confirm Password</label>
                    <input type="password" class="form-control" id="password_confirmation"
                           name="password_confirmation">
                </div>
            </div>
        </div>   
    </div>
</div>



 
            <input type="hidden" class="form-control" id="role" name="role"  value=3>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-save"></i>
                @lang('app.create_user')
            </button>
       
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Hotel\CreateRequest', '#hotel-form') !!}
@stop

