<?php

namespace App\Http\Requests\Masters\Proptype;

use App\Http\Requests\Request;

class BaseProptypeRequest extends Request {
	/**
	 * Validation messages.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'proptype.unique' => 'Type alredy exist',
		];
	}
}