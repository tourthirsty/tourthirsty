# TOURTHIRSTY

##Server Requirements

In order to install, your server must meet following requirements:

* PHP >= 5.5.9 (including PHP 7)
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* GD PHP Extension
* Fileinfo PHP Extension

## Quick Start

**Setup Commands**

 1. `git clone`
 2. `composer install`
 3. `npm install`
 4. `bower install`
 5. `cp .env.example .env`
 6. `php artisan key:generate`
 7. `php artisan migrate`
 8. Set administrator info in UserSeeder.php
 9. `php artisan db:seed`
 10. `gulp --production` (Install gulp (sudo npm install -g gulp) if needed)

