<?php
    $this->load->view('home/common/inner_head');
?>


<style>
@media print {

.header-panel {
position: relative;
display: none;
}
#jobs
{
position: relative;

}

#button_loadmore
{
position: relative;
display: none;

}

footer {
position: relative;
display: none;
}
.btn {
  display: none;

}

#lhc_status_container, #lhc_questionary_container, #lhc_faq_container, #lhc_chatbox_container {
display: none;
}





}

 </style>
<div class="col-md-12 middle-div no-padding ">
  <div class="col-md-9 no-padding brd-crumb">
    <a href="<?php echo base_url();?>">HOME</a> > Transactions history</div>
          <div class="col-md-12 col-sm-12 col-xs-12 right-section">
            <?php if($this->session->flashdata('message')) { ?> <br>
            <div class="alert alert-success">
                <?php  echo $this->session->flashdata('message');?>
            </div>
            <?php } ?>



<div data-ng-app="job" id="milestone_content" ng-cloak >

<section class="inner-content cf " data-ng-controller="ClientCtrl" >
 <section class="row cf inner_header " class="job_listing">
          <aside class="col-lg-10 col-md-10 col-sm-10 nopadding ">
            <h3>Transactions history</h3>
          </aside>
          <aside class="col-lg-2 col-md-2  col-sm-2">

          </aside>
        </section>
        <section class="row">
          <section class="col-lg-12 nopadding ">
<div class="table-responsive eplace-table " data-ng-controller="ClientCtrl">

<form id="selection" action="">
<table class="table table-bordered table-striped" id="jobs">
<tbody>
<tr>
<td colspan="9"><b>This page contains all the transactions made from your account</b></td>
</tr>
<tr>
<td class="text-nowrap" scope="row" style="padding-top: 15px;">From</td>
<td>
<div class='input-group date' id='datetimepicker1'>
<input type="text" class="form-control"  id="datepicker_from" name="datepicker_from" readonly>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
</div>
</td>

<td style="padding-top: 15px;">To</td>
<td>
<div class='input-group date' id='datetimepicker1'>
<input type="text" class="form-control"  id="datepicker_to" name="datepicker_to"readonly>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
</div>
</td>
<td>
<select class="form-control" id="sel1 transaction_type" name="transaction_type">
    <option value="">All Transactions</option>
    <option value="y">Success</option>
    <option value="n">Failed</option>
    <option value="d">Denied</option>
  </select>
</td>
<td>





<select class="form-control" id="sel1 client_name" name="client_name" ng-model="job" >
<option value=""><?php if($gid!=5){ echo'All Clients'; }else{echo'All Freelancers';} ?> </option>

<?php if($gid!=5):?>
<option ng:repeat='x in list_clients' value='{{x.cpro_uacc_fk}}'> {{x.epro_first_name}}&nbsp;{{x.epro__name}} </option>
<?php else:?>
<option ng:repeat='x in list_clients' value='{{x.cpro_uacc_fk}}'> {{x.cpro_first_name}}&nbsp;{{x.cpro_last_name}} </option>
<?php endif;?>

</select>



</td>

<td>
<button type="button" class="btn btn-default" ng-click="searchformController()">Go</button>
</td>

<td style="padding-top: 15px;">
<a href="">
<span  id="cmd" class="glyphicon glyphicon-list-alt" title="XL" onclick="savexl();">

</span>
</a>
</td>

<td style="padding-top: 15px;">
 <span class="glyphicon glyphicon-print" title="Print" onClick="printdiv('print_table');"></span>
</td>
</tr>
</tbody>
</table>





<table  class="table table-striped" id="print_table">
<thead>
<th>Job Title</th>
<th><?php if($gid!=5){ echo'All Clients'; }else{echo'All Freelancers';} ?></th>
<th>Budget</th>
<th><?php if($gid!=5){ echo'Amount recieved till now'; }else{echo'Amount paid till now';} ?></th>
<th title="Pending Milestones/Total Milestones">Milestones</th>
<th>Status</th>
<th>Date </th>
<th></th>
</thead>
<tbody>
<?php   ?>

<tr class="job_box" ng-if="list_jobs_counts" data-ng-repeat="job in list_jobs_counts">
<td><a href="<?php echo site_url('work/details'); ?>/{{job.job_id}}">{{job.job_title}}</a></td>
<td>
<?php if($gid!=5):?>
{{job.epro_first_name}} {{job.epro_last_name}}
<?php else:?>
{{job.cpro_first_name}} {{job.cpro_last_name}}
<?php endif;?>
</td>
<td><span ng-show="job.grand_total != 0"> INR {{job.grand_total}}</span></td>
<td><span ng-show="job.paid_total != 0"> INR {{job.paid_total}}</span></td>
<td title="Pending Milestones/Total Milestones"> {{job.milestones_pending}}/ {{job.num_milestones}}
<span ng-show="job.upfront == 'y'"> + Upfront Paid</span>
</td>
<td>
<div ng-show="job.status == 'y'">Success</div>
<div ng-show="job.status == 'n'">Failed</div>
<div ng-show="job.status == 'd'">Declined</div>
<div ng-show="job.status == ''">Error</div>
</td>

<td>{{job.postdate}} </td>

<td> <a ng-href="milestones/{{job.job_id}}/{{job.freelancer}}"  class="btn orangebtn" name="accept">View Milestones</a></td>

</tr>
<tr ng-hide="list_jobs_counts.length" >
	<td colspan="7">
		No Data Found
	</td>
</tr>

</div>
</tbody>
</table>

	<div class="col-lg-12 nopadding"  id="button_loadmore"  >
            <a href="javascript:void(0)" class="btn-load-more" ng-hide="button_clicked" ng-show="list_jobs_counts.length" ng-click="loadmore(list_jobs_counts.length)" role="button" style="margin-left:350px">LOAD MORE</a>

    </div>



  <table cellspacing="0" cellpadding="0" border="0"   class='table_sub' >
  <tbody>

  <tr class="bottomborder topborder">
    <td align="" width="40%" class="bd-merchline"><b>Grand Total</b></td>
    <td align="" width="16%" class="bd-merchline"  ><span ng-show="all_jobs_grand_total != 0">INR {{all_jobs_grand_total}}</span></td>
  </tr>



<tr class="bottomborder topborder">
    <td align="" width="40%" class="bd-merchline"><b>Payment Completed</b></td>
    <td align="" width="16%" class="bd-merchline"><span ng-show="all_jobs_paid_total != 0"> INR {{all_jobs_paid_total}} </span>    </td>
  </tr>

<tr class="bottomborder topborder">
    <td align="" width="40%" class="bd-merchline"><b>Sub Total</b></td>
    <td align="" width="16%" class="bd-merchline"> <span ng-show="all_jobs_sub_total != 0">INR {{all_jobs_sub_total}}  </span>   </td>
  </tr>
    <tr class="bottomborder">
    <td align="" width="40%" class="bd-merchline"><b>Milestones Pending</b></td>
    <td align="" width="16%" class="bd-merchline"> <span ng-show="all_jobs_milestones_pending != 0"> {{all_jobs_milestones_pending}}   </span>  </td>
  </tr>


      <tr class="bottomborder">
    <td align="" width="40%" class="bd-merchline"><b>Total Milestones</b></td>
    <td align="" width="16%" class="bd-merchline"> <span ng-show="all_jobs_num_milestones != 0"> {{all_jobs_num_milestones}}   </span>  </td>
  </tr>



  </tbody></table>




</form>










        </div>


</section>
</section>
</div>
</div>
</div>
</div>
<?php
    $this->load->view('home/common/inner_footer');
?>


<style>
.form-control{
  height: auto;
width: auto;

}
</style>



<script>

    function setupLabel() {
        if ($('.label_check input').length) {
            $('.label_check').each(function(){
                $(this).removeClass('c_on');
            });
            $('.label_check input:checked').each(function(){
                $(this).parent('label').addClass('c_on');
            });
        };
        if ($('.label_radio input').length) {
            $('.label_radio').each(function(){
                $(this).removeClass('r_on');
            });
            $('.label_radio input:checked').each(function(){
                $(this).parent('label').addClass('r_on');
            });
        };
    };
    $(document).ready(function(){
        $('body').addClass('has-js');
        $('.label_check, .label_radio').click(function(){
            setupLabel();
        });
        setupLabel();
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>includes/js/angular.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>includes/home/anjularjs/angular-resource.js"></script>
<script type="text/javascript" src='<?php echo base_url();?>includes/home/anjularjs/angular-slider.js'  charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url();?>includes/js/transactions.js"></script>


<link rel="stylesheet" href="<?php echo base_url();?>includes/css/jquery-ui.css">


<script src="<?php echo base_url();?>includes/js/jquery-ui.js"></script>
<script type="text/javascript">
$(function() {

  $("#datepicker_from").datepicker({
       numberOfMonths: 1,
       dateFormat: 'yy-mm-dd',
       onSelect: function (selected) {
           var dt = new Date(selected);
           dt.setDate(dt.getDate() + 1);
           $("#datepicker_to").datepicker("option", "minDate", dt);
       }
   });
   $("#datepicker_to").datepicker({
       numberOfMonths: 2,
       dateFormat: 'yy-mm-dd',
       onSelect: function (selected) {
           var dt = new Date(selected);
           dt.setDate(dt.getDate() - 1);
           $("#datepicker_from").datepicker("option", "maxDate", dt);
       }
   });
  });

function printdiv()
{

    window.print();


}

function savexl()
{

var base_url=document.getElementById('base_url').value;
var datepicker_from = $( "#datepicker_from" ).val();
var datepicker_to = $( "#datepicker_to" ).val();
var client_name = $( "#client_name" ).val();
var transaction_type = $( "#transaction_type" ).val();
if(! datepicker_from){var datepicker_from = ''; }
if(! datepicker_to){var datepicker_to = ''; }
if(! client_name){var client_name = ''; }
if(! transaction_type){var transaction_type = ''; }


var dataString ='datepicker_from=' + datepicker_from+ '&datepicker_to=' + datepicker_to+ '&client_name=' + client_name+ '&transaction_type=' + transaction_type+ '&nocache=' +Math.random();
window.top.location = base_url+"reports/transactions_report_file_xls?"+dataString;

$.ajax({
        type: "GET",
        url: base_url+"reports/transactions_report_file_xls",
                data: dataString,
        success: function(response){ }
            ,
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });

}


function savepdf()
{

var base_url=document.getElementById('base_url').value;
var datepicker_from = $( "#datepicker_from" ).val();
var datepicker_to = $( "#datepicker_to" ).val();
var client_name = $( "#client_name" ).val();
var transaction_type = $( "#transaction_type" ).val();
if(! datepicker_from){var datepicker_from = ''; }
if(! datepicker_to){var datepicker_to = ''; }
if(! client_name){var client_name = ''; }
if(! transaction_type){var transaction_type = ''; }


var dataString ='datepicker_from=' + datepicker_from+ '&datepicker_to=' + datepicker_to+ '&client_name=' + client_name+ '&transaction_type=' + transaction_type+ '&nocache=' +Math.random();
        $.ajax({
        type: "GET",
        url: base_url+"reports/transactions_report_file_xls",
                data: dataString,
        success: function(response){}
            ,
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });

}


</script>
