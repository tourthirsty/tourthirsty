<?php

namespace App\Http\Requests\Location;

use App\Http\Requests\Request;

class CreateRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		$rules = [
			'place' => 'required|min:6',
			'desc' => 'required|min:6',
                        'add'=> 'required',
                     
		];

		return $rules;
	}

	public function messages() {
		return [
			'add.required'  => 'Please Select Place in Map',
		];
	}
}
