<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel_meal_type extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'hotel_meal_types';
}
