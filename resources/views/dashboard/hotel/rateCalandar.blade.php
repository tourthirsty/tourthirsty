@extends('dashboard.layouts.master')
@section('page-title', 'Hotel Users')
@section('page-header')
<link href='{{ asset('assets/css/fullcalendar.css') }}' rel='stylesheet' />
<link href='{{ asset('assets/css/fullcalendar.print.css') }}' rel='stylesheet' media='print' />
<style>

    .calandar {
        margin-top: 40px;
        text-align: center;
        font-size: 14px;
        font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
    }
    #trash{
        width:32px;
        height:32px;
        float:left;
        padding-bottom: 15px;
        position: relative;
    }

    #wrap {
        width: 1100px;
        margin: 0 auto;
    }

    #external-events {
        float: left;
        width: 150px;
        padding: 0 10px;
        border: 1px solid #ccc;
        background: #eee;
        text-align: left;
    }

    #external-events h4 {
        font-size: 16px;
        margin-top: 0;
        padding-top: 1em;
    }

    #external-events .fc-event {
        margin: 10px 0;
        cursor: pointer;
    }

    #external-events p {
        margin: 1.5em 0;
        font-size: 11px;
        color: #666;
    }

    #external-events p input {
        margin: 0;
        vertical-align: middle;
    }

    #calendar {
        float: right;
        border: 1px solid #ddd;
    }

</style>
<h1>
    Hotels
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li class="active">Hotels</li>
</ol>
@endsection
@section('content')
@include('partials.messages')
<div class="row tab-search">
    <div class="col-md-2 col-xs-2">
        <!--<a href="" class="btn btn-success" id="add-user">
            <i class="glyphicon glyphicon-plus"></i>
            Manage Rate Calander
        </a>-->
    </div>
    <div class="col-md-5 col-xs-3"></div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Hotels</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <div id="users-table-wrapper">
                    <div class="calandar">
                        <div id='wrap'>
                            <div id='calendar'></div>
                            <div style='clear:both'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    @stop
    @section('after-scripts-end')
    <script type="text/javascript" src="{{ asset('assets/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/fullcalendar.min.js') }}"></script>
    <script>

    $(document).ready(function () {

        var zone = "05:30"; //Change this to your timezone
        var APP_URL = {!! json_encode(url('/en/dashboard/hotel/rateCalandarDetails')) !!};
        var token = $("#token").val();
        var hotel = "<?php echo $id; ?>";
        $.ajax({
            url: APP_URL,
            type: 'POST', // Send post data
            data: 'type=fetch&_token=' + token+'&id='+hotel,
            async: false,
            success: function (s) {
                //console.log(s);
                json_events = s;
            }
        });


        var currentMousePos = {
            x: -1,
            y: -1
        };
        jQuery(document).on("mousemove", function (event) {
            currentMousePos.x = event.pageX;
            currentMousePos.y = event.pageY;
        });

        /* initialize the external events
         -----------------------------------------------------------------*/

        $('#external-events .fc-event').each(function () {

            // store data so the calendar knows to render an event upon drop
            $(this).data('event', {
                title: $.trim($(this).text()), // use the element's text as the event title
                stick: true // maintain when user navigates (see docs on the renderEvent method)
            });

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });


        /* initialize the calendar
         -----------------------------------------------------------------*/

        $('#calendar').fullCalendar({
            events: JSON.parse(json_events),
            //events: [{"id":"14","title":"New Event","start":"2015-01-24T16:00:00+04:00","allDay":false}],
            utc: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
        });

    });

    </script>
    @stop
