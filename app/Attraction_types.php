<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attraction_types extends Model
{
    protected $table = 'attraction_types';
        protected $fillable = array(
			'type',
			'description');
}
