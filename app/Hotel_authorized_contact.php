<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel_authorized_contact extends Model
{
    protected $table = 'hotel_authorized_contacts';
}
