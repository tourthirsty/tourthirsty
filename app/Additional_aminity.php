<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Additional_aminity extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'additional_aminities';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'add_aminity', 'add_amini_desc', 
	];
}
