<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use Session;

use App\Http\Requests;

use App\Hotel_policy;

use App\Http\Controllers\Controller;

class HotelpolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prop = Hotel_policy::all();
		return view('dashboard.masters.hotel_policy.list',compact('prop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('dashboard.masters.hotel_policy.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
				'hotel_details_no_rooms' => 'required',
				'hotel_details_checkin_time' => 'required',
				'hotel_details_checkout_time' => 'required',
				'hotel_details_no_restaurants' => 'required',
				'hotel_details_roomservice_timing' => 'required',
				'hotel_details_bar' => 'required'
				]);
		$input=$request->all();
		Hotel_policy::create($input);
		Session::flash('flash_message', 'Type successfully added!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $prop = Hotel_policy::findOrFail($id);

         return view('dashboard.masters.hotel_policy.edit',compact('prop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prop = Hotel_policy::findOrFail($id);

        $this->validate($request, [
				'hotel_details_no_rooms' => 'required',
				'hotel_details_checkin_time' => 'required',
				'hotel_details_checkout_time' => 'required',
				'hotel_details_no_restaurants' => 'required',
				'hotel_details_roomservice_timing' => 'required',
				'hotel_details_bar' => 'required'
        ]);

        $input = $request->all();

        $prop->fill($input)->save();

        Session::flash('flash_message', 'Property type successfully updated!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prop = Hotel_policy::findOrFail($id);

        $prop->delete();

        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('propertydetails.index');
    }
}
