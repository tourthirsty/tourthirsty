<?php

namespace App\Http\Requests\Hotel;

use App\Http\Requests\Request;

class CreatePropertyInfoRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'prop_name' => 'required|min:3',
			'website' => 'required|min:3',
			'fhrai' => 'required',
 			'email' => 'required|email',
			'phone' => 'required',
			
		    ];
	}
}
