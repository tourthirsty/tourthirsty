@extends('dashboard.layouts.master')

@section('page-title')

@section('page-header')
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
    <h1>
       Add Group
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li><a href="{{ route('hotel.listgroup',$id) }}">Group</a></li>
        <li class="active">Create</li>
      </ol>
@endsection

@section('content')


@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif


{!! Form::open(['route' => 'hotel.groupstore', 'id' => 'group-form']) !!}


<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Group Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="hotel_grp_name">Group</label>
                    <input type="text" class="form-control" id="hotel_grp_name"
                           name="hotel_grp_name" placeholder="Group Name">
                </div>
	        <div class="form-group">
                    <label for="hotel_grp_address">Address</label>
                    <textarea class="form-control" rows="5" id="hotel_grp_address" name="hotel_grp_address" placeholder="Group Address"></textarea>
                </div>
				<div class="form-group">
                    <label for="hotel_grp_contact">Contact</label>
                    <input type="text" class="form-control" id="hotel_grp_contact"
                           name="hotel_grp_contact" placeholder="Group Contact Number">
                </div>
                
               </div>
            </div>
        </div>
    </div>
<input type="hidden" class="form-control" id="" name="group" placeholder="" value="<?php echo $id; ?>">
<div class="row">
    <div class="col-md-2">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Create 
        </button>
    </div>
</div>

@stop

@section('fter-scripts-end')
    
        {!! JsValidator::formRequest('App\Http\Requests\Hotel_group\CreateGroupRequest', '#group-form') !!}
   <

@stop
