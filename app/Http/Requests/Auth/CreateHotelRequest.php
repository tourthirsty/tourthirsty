<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class CreateHotelRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		$rules = [
			'email' => 'required|email|unique:users,email',
			'password' => 'required|confirmed|min:6',
                        'full_name'=> 'required|min:6',
                        'optiontype' => 'required',
                        'phone' => 'required'
		];

		if (settings('tos')) {
			$rules['tos'] = 'accepted';
		}

		return $rules;
	}

	public function messages() {
		return [
			'tos.accepted' => trans('app.you_have_to_accept_tos'),
                        'optiontype.required'  => 'Please Select any Option',
		];
	}
}
