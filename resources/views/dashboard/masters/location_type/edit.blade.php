@extends('dashboard.layouts.master')

@section('page-title', 'Edit Location Type')

@section('page-header')
<h1>
    Edit Location Type
    <small>Location Type</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.locationType') }}">>Location Types</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => ['dashboard.locationType.update', $Location_type->id], 'method' => 'PUT', 'id' => 'location-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Location Type Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="email">Location Type</label>
                    <input type="text" class="form-control" id="location_type" name="location_type" placeholder="Hill Station" required="" value="{{$Location_type->loc_type_name }}">
                </div>
                <div class="form-group">
                    <label for="username">Description</label>
                    <textarea name="location_type_desc" placeholder="Eg:The hill stations are high-altitude towns used, especially by European colonialists..." class="form-control" required="">{{$Location_type->loc_type_desc }}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\UpdateLocationTypeRequest', '#location-form') !!}
@stop
