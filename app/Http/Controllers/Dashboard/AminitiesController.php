<?php

namespace App\Http\Controllers;
namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Room_aminity;
use App\Http\Controllers\Controller;

class AminitiesController extends Controller
{
	/**
	 * Displays the page with all room aminities.
	 *
	 *
	 */
    public function index() {
		$prop = Room_aminity::all();
		return view('dashboard.masters.aminities.list',compact('prop'));
		
	}
	
	/**
     * Show the form for creating a new propert type.
     *
     * @return \Illuminate\Http\Response
     */
	public function create()
    {
        return view('dashboard.masters.aminities.add');
    }
	
	 /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
	{
		$this->validate($request,[
				'room_amn_name' => 'required',
				'room_amn_desc' => 'required'
				]);
		$input=$request->all();
		Room_aminity::create($input);
		Session::flash('flash_message', 'Room aminity successfully added!');

        return redirect()->back();
   
	}
	
	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function edit($id)
    {
         $prop = Room_aminity::findOrFail($id);

         return view('dashboard.masters.aminities.edit',compact('prop'));
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 public function update(Request $request, $id)
    {
        $prop = Room_aminity::findOrFail($id);

        $this->validate($request, [
            'room_amn_name' => 'required',
            'room_amn_desc' => 'required'
        ]);

        $input = $request->all();

        $prop->fill($input)->save();
        //Session::flash('flash_message', 'Room aminity successfully updated!');

        return redirect()->back()->withSuccess(trans('Details Updated Succesfully'));
    }
	
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function destroy($id)
    {
        $prop = Room_aminity::findOrFail($id);

        $prop->delete();

        Session::flash('flash_message', 'Room aminity successfully deleted!');

        return redirect()->route('aminities.index');
    }
}
