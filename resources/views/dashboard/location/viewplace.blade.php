@extends('dashboard.layouts.master')

@section('page-title', 'Place Details')

@section('page-header')
<h1>
    Place Details
   
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="#">>Location</a></li>
    <li class="active">Place Details</li>
</ol>
@endsection

@section('content')




<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">{{ $place->place }} Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="email">Place Name :</label>
                    {{$place->place }}
                </div>
                <div class="form-group">
                    <label for="username">Description :</label>
                    {{$place->desc }}
                   
                </div>
                <div class="row form-group">
                    <div class="col-md-10">
                        <?php $urls = explode(',', $place['url']);
                              if(isset($urls[0])) {
                              $img1 = $urls[0];
                              } else {
                              $img1 = ""; 
                              }
                              if(isset($urls[1])) {
                              $img2 = $urls[1];
                               } else {
                              $img2 = ""; 
                              }
                              if(isset($urls[2])) {
                              $img3 = $urls[2];
                              } else {
                              $img3 = "";
                              }
                        ?>
                        
                        <?php if($img1) { ?>
                      
                        <div class="ImagePreviewBox col-md-3" id="ImagePreviewBox1">
                        <img class="img-thumbnail" style="height:150px;width:150px;" src="{{ asset('upload/places/'.$img1) }}" class=""> 
                        </div>
                        <?php } if($img2) {  ?>
                        <div class="ImagePreviewBox col-md-3" id="ImagePreviewBox1">
                        <img class="img-thumbnail" style="height:150px;width:150px;" src="{{ asset('upload/places/'.$img2) }}" class=""> 
                        </div>
                        <?php } if($img3) {  ?>
                        <div class="ImagePreviewBox col-md-3" id="ImagePreviewBox1">
                        <img class="img-thumbnail" style="height:150px;width:150px;" src="{{ asset('upload/places/'.$img3) }}" class=""> 
                        </div> 
                        <?php }  ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="username">Created On :</label>
                    {{$place->created_at }}
                   
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        
        <a href="{{ URL::previous() }}" class="btn btn-default"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Back</a>
    </div>
</div>



@stop


