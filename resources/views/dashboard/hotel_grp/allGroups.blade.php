@extends('dashboard.layouts.master')
@section('page-title', 'Group Details')
@section('page-header')
<h1>
Groups
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li class="active">Groups</li>
</ol>
@endsection

@section('content')
@include('partials.messages')

<div class="row tab-search">
@if(Session::has('flash_message'))
    <div class="alert alert-success">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('flash_message') }}
    </div>
@endif
 

    <div class="col-md-5 col-xs-3"></div>
   
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Groups</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <div id="users-table-wrapper">
                    <table class="table table-hover table-striped">
                        <tbody>
                            <tr>
                                <th>Group</th>
                                <th>Address</th>
                                 <th>Contact</th>
                                 <th>Hotel User</th>
				<th>Created_at</th>
                                <th class="text-center">Action</th>
                            </tr>
               
                            @if (count($Groups))
                            @foreach ($Groups as $group)
                            <tr>
                                <td>{{ $group->hotel_grp_name }}</td>
                                <td>{{ $group->hotel_grp_address }}</td>
                                <td>{{ $group->hotel_grp_contact }}</td>
                                <td>{{ $group->first_name }}</td>
							    <td>{{ $group->created_at }}</td>
                             
                                <td class="text-center">
                                    <?php //$id = $group->id; ?>
                                    <a href="{{ route('hotel.hotels',$group->id) }}" class="btn btn-success btn-circle"
                                        title="Manage Group" data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a>
                                    <a href="{{ route('hotel.editgroup', $group->id) }}" class="btn btn-primary btn-circle edit" title="Edit Group"
                                        data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    <a href="{{ route('hotel.destroygroup', $group->id) }}" class="btn btn-danger btn-circle" title="Delete Group"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-method="DELETE"
                                        data-confirm-title="Please Confirm"
                                        data-confirm-text="Are you sure that you want to delete ?"
                                        data-confirm-delete="Yes Delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </a>
                                
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6"><em>No records found</em></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@stop

