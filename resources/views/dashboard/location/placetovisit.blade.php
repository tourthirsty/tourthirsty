@extends('dashboard.layouts.master')
@section('page-title', 'Nearest Places')
@section('page-header')
<h1>
Nearest Places of <?php echo $loc->loc_name; ?>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li class="active">Location</li>
</ol>
@endsection
@section('content')
@include('partials.messages')
@if(Session::has('flash_message'))
    <div class="alert alert-success">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ Session::get('flash_message') }}
    </div>
@endif
<div class="row tab-search">
    <div class="col-md-2 col-xs-2">
        <a href="{{ route('location.createplace', $id) }}" class="btn btn-success" id="add-user">
            <i class="glyphicon glyphicon-plus"></i>
           Add Near Places
        </a>
    </div>
    <div class="col-md-5 col-xs-3"></div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Nearest Places of <?php echo $loc->loc_name; ?></h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <div id="users-table-wrapper">
                    <table class="table table-hover table-striped">
                        <tbody>
                            <tr>
                                <th>Place</th>
                                <th>Description</th>
                                 <th>Distance From <?php echo $loc->loc_name; ?></th>
                                <th class="text-center">Action</th>
                            </tr>
                            @if ($places)
                            @foreach ($places as $place)
                            <tr>
                                <td>{{ $place->place }}</td>
                                <td style="width:50%">{{ $place->desc}}</td>
                                <td>{{ $place->dist_from_loc }} KM</td>
                                
                                <td class="text-center">
                                    <a href="{{ route('location.showplace', $place->id) }}" class="btn btn-success btn-circle"
                                        title="" data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a>
                                    <a href="{{ route('location.editplace', $place->id) }}" class="btn btn-primary btn-circle edit" title="Edit Place Details"
                                        data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    <a href="{{ route('location.deletenearplace', $place->id) }}" class="btn btn-danger btn-circle" title="Delete Place"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-method="DELETE"
                                        data-confirm-title="Please Confirm"
                                        data-confirm-text="Are you sure that you want to delete ?"
                                        data-confirm-delete="Yes Delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </a>
                                
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6"><em>No records found</em></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@stop

