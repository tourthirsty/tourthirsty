<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';
    protected $fillable =[
        'Location_name',
        'location_types[]',
        'Location_desc'
    ];
}
