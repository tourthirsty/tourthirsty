<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 ftrbox">
                <h4>ABOUT US</h4>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                    been the industry's standard dummy text ever since the 1500s, when an unknown printer took 
                    a galley of type and scrambled it to make a type specimen book. It has survived not only 
                    five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </p>
            </div>
            <div class="col-md-5 ftrbox">
                <h4>SUBSCRIBE TO OUR NEWSLETTER</h4>
                <div>
                    <span><input type="text" placeholder="Your email id"/></span>
                    <span><button>SUBSCRIBE</button></span>
                </div>
                <h3>FOLLOW US</h3>
                <ul>
                    <li><a><img src="{{URL::asset('assets/frontend/images/fb.jpg')}}"/></a></li>
                    <li><a><img src="{{URL::asset('assets/frontend/images/twitr.jpg')}}"/></a></li>
                    <li><a><img src="{{URL::asset('assets/frontend/images/linkdin.jpg')}}"/></a></li>
                    <li><a><img src="{{URL::asset('assets/frontend/images/youtube.jpg')}}"/></a></li>
                    <li><a><img src="{{URL::asset('assets/frontend/images/googlepls.jpg')}}"/></a></li>
                </ul>
            </div>
            <div class="col-md-3 ftrbox">
                <h4>CONTACT US</h4>
                <div class="contactbx"><i class="fa fa-map-marker"></i> 15488VegasDrive,Las Vega,Naveda</div>
                <div class="contactbx"><i class="fa fa-phone-square"></i> +1800965743,+189076543</div>
                <div class="contactbx"><i class="fa fa-envelope"></i> http://www.tourthirsty.com</div>
                <div class="contactbx"><i class="fa fa-skype"></i> info@tourthirsty.com</div>
            </div>
        </div>
   </div>
</footer>
<section class="footerbtm">
    <div class="container">
       <div class="row">
            <div class="btmfooter">
                <div class="col-md-4 btmlft">&copy; Copyright 2014 All Rights Reserved By TourThirsty</div>
                <div class="clear:both;"></div>
                <div class="col-md-8 btmmenu">
                    <ul>
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="btmrgt"></li>
                        <li><a href="{{url('showcms/1')}} ">About Us</a></li>
                        <li class="btmrgt"></li>
                        <!--<li><a>Hotels</a></li>
                        <li class="btmrgt"></li>
                        <li><a href="#">Destinations</a></li>
                        <li class="btmrgt"></li>
                        <li><a>Gallery</a></li>
                        <li class="btmrgt"></li>
                        <li><a>Blog</a></li>
                        <li class="btmrgt"></li>-->

                        <li><a href="{{url('showcms/2')}}">T & C</a></li>
                        <li class="btmrgt"></li>
                        <li><a href="{{url('showcms/3')}}">Privacy Policy   </a></li>
                        <li class="btmrgt"></li>
                        <li><a href="{{url('showcms/4')}}">Disclosure</a></li>
                        <li class="btmrgt"></li>


                        <li><a href="{{url('contact_us')}}">Contact Us</a></li>


                    </ul>
                </div>
            </div>
       </div>
    </div>
</section>
        </div>
    </div>
</body>
<script src="{{URL::asset('assets/frontend/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{URL::asset('assets/frontend/js/bootstrap.min.js')}}"></script>
<script type="text/javascript">

$(document).ready(function(){ 
        $("[href]").each(function() {
    if (this.href == window.location.href) {
        $(this).addClass("selected");
        }
    });
});
</script>
@yield('before-scripts-end')
    {!! Html::script(elixir('js/dashboard.js')) !!}
    {!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}
    @yield('after-scripts-end')

</html>