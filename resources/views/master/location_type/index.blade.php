@extends('dashboard.layouts.master')
@section('page-title', 'Location Type')
@section('page-header')
<h1>
Location Type
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li class="active">@lang('app.users')</li>
</ol>
@endsection
@section('content')
@include('partials.messages')

<div class="row tab-search">
    <div class="col-md-2 col-xs-2">
        <a href="{{ route('master.locationType.create') }}" class="btn btn-success" id="add-user">
            <i class="glyphicon glyphicon-plus"></i>
           Add Location Type
        </a>
    </div>
    <div class="col-md-5 col-xs-3"></div>
    <!--<form method="GET" action="" accept-charset="UTF-8" id="users-form">
        <div class="col-md-2 col-xs-3">
        </div>
        <div class="col-md-3 col-xs-4">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" name="search" value="{{ Input::get('search') }}" placeholder="@lang('app.search_for_users')">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="search-users-btn">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    @if (Input::has('search') && Input::get('search') != '')
                        <a href="{{ route('master.locationTypes') }}" class="btn btn-danger" type="button" >
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    @endif
                </span>
            </div>
        </div>
    </form>-->
</div>


<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Location Types</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <div id="users-table-wrapper">
                    <table class="table table-hover table-striped">
                        <tbody>
                            <tr>
                                <th>Location Type</th>
                                <th>Description</th>
                                 <th>Created</th>
                                <th class="text-center">Action</th>
                            </tr>
                 
                            @if (count($Locationtype))
                            @foreach ($Locationtype as $location)
                            <tr>
                                <td>{{ $location->loc_type_name ?: trans('app.n_a') }}</td>
                                <td>{{ $location->loc_type_desc }}</td>
                                <td>{{ $location->created_at }}</td>
                                
                                <td class="text-center">
                                    <!--<a href="#" class="btn btn-success btn-circle"
                                        title="" data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a>-->
                                    <a href="{{ route('master.locationType.edit', $location->id) }}" class="btn btn-primary btn-circle edit" title="Edit Location Type"
                                        data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    <a href="{{ route('master.locationType.destroy', $location->id) }}" class="btn btn-danger btn-circle" title="Delete Location Type"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-method="DELETE"
                                        data-confirm-title="Please Confirm"
                                        data-confirm-text="Are you sure that you want to delete ?"
                                        data-confirm-delete="Yes Delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </a>
                                
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6"><em>No records found</em></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@stop

