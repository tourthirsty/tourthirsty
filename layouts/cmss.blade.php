@include('frontend.includes.header')
<link href=" {{ URL::asset('assets/frontend/css/innerstyle.css')}}" rel="stylesheet">
<link href=" {{ URL::asset('assets/frontend/css/styletogle.css')}}" rel="stylesheet">
<section>
    <div class="innerbnr">
        <div class="container no-padding-right">
            <h3 class="innertitle">{{$cms->name }}</h3>
            <div class="innerwrapr">
                <div class="pagetxt">HOME <span>> {{$cms->name }}</span></div>
                <div class="subbox">
                    We're truely dedicated to make your travel experience as much simple and fun as possible
                    <div class="imgrgt"><img class="img-responsive" src="{{URL::asset('assets/frontend/images/chair.png')}}"/></div>
                </div>
                <h4>{{$cms->name }}</h4>
                <p> <?php echo nl2br($cms->content);?></p>
                <!--<div class="btmwrapr">
                    <div class="col-md-12 no-padding-left box">
                        <div class="col-md-4 no-padding-left"><img class="img-responsive"  src="{{URL::asset('assets/frontend/images/article1.png')}}"/></div>
                        <div class="col-md-12  no-padding articletxt">
                            
                        </div>
                    </div>
                </div>-->

                <!--<div class="btmwrapr">
                    <div class="col-md-12 no-padding-left box">
                        <div class="col-md-4 no-padding-left"><img class="img-responsive"  src="{{URL::asset('assets/frontend/images/article1.png')}}"/></div>
                        <div class="col-md-8  no-padding articletxt">
                            <h4>Tour Thirsty</h4>
                            <p> Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor 
                                eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, 
                                feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean 
                                imperdiet. Etiam ultricies nisi vel augue. Curabitur corper ultricies nisi. Nam eget dui.
                                    Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam sempe.</p>
                        </div>
                    </div>
                    <div class="col-md-12 box">
                        <div class="col-md-8  no-padding articletxt">
                            <h4>Tour Thirsty</h4>
                            <p> Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor 
                                eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, 
                                feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean 
                                imperdiet. Etiam ultricies nisi vel augue. Curabitur corper ultricies nisi. Nam eget dui.
                                    Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam sempe.</p>
                        </div>
                         <div class="col-md-4 no-padding-right"><img class="img-responsive" src="{{URL::asset('assets/frontend/images/article2.png')}}"/></div>
                    </div>
                    <div class="col-md-12 no-padding btmdiv">
                        <div class="col-md-6 no-padding-left">
                           <h5>Know More About Us</h5>
                            <div class="boxlft">
                            <div class="toggle"><span class="image"></span>
                                <span class="text">Travel Insurance Single Person</span>
                            </div>
                            <div class="content">
                            	when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                              It has survived not only five centuries, but also the leap into electronic
                              with the release of Letraset sheets containing Lorem Ipsum passages, 
                            </div>
	                        <div class="toggle"><span class="image"></span>
	                            <span class="text">Infight Dinner/Lunch Deal</span>
	                        </div>
	                        <div class="content">when an unknown printer took a galley of type and scrambled it to make
	                            a type specimen book. It has survived not only five centuries, but also the leap into 
	                            electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s
	                            desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
	                        <div class="toggle lst"><span class="image"></span>
	                            <span class="text">Luxury Appartment for Family</span>
	                        </div>
	                        <div class="content">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
	                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
	                            unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
	                        </div>
                        </div>
                        <div class="col-md-6">
                            <h5>Our Core Values</h5>
                            <div class="boxrgt">
                                <div class="toptitle"><span>Satisfied Customers</span></div>
                                <div class="details">
                                    <img class="img-responsive" src="{{URL::asset('assets/frontend/images/article3.png')}}"/>
                                    <div>
                                        <h6>Ocean Park Tour</h6>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                            unknown printer took a galley of type and scrambled it to make a type specimen boo
                                        </p>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                            unknown printer took a galley of type and scrambled it to make a type </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="btmad"><img class="img-responsive" src="{{URL::asset('assets/frontend/images/btmad.jpg')}}"/></div>
        </div>
    </div>
</section>

		<?php //print_r($cms);?>
	

@include('frontend.includes.footer') 