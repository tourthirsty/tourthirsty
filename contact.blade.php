@include('frontend.includes.header')
<link href=" {{ URL::asset('assets/frontend/css/innerstyle.css')}}" rel="stylesheet">
<link href=" {{ URL::asset('assets/frontend/css/styletogle.css')}}" rel="stylesheet">
@section('page-title', trans('app.login'))
@section('page-title', trans('app.sign_up'))

<div class="contactbnr">
  <div class="container no-padding-right">
    <h3 class="innertitle">CONTACT US</h3>
    <div class="innerwrapr">
      <div class="pagetxt">HOME <span>>CONTACT US</span></div>
      <div class="contactform">
        @include('partials.messages')
        <div class="col-md-7 col-sm-7 no-padding">
          {!! Form::open(['route' => 'frontend.cms.store', 'files' => true, 'id' => 'cms-form']) !!}
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="forminner">
            <div class="col-md-6 col-sm-6 no-padding-left">
              <label>Name</label>
              <input type="text" value="" name="full_name"/>
            </div>
            <div class="col-md-6 col-sm-6 no-padding-right">
              <label>Email</label>
              <input type="email" name="email" value="" />
            </div>
          </div>
          <div class="forminner">
            <div class="col-md-6 col-sm-6 no-padding-left">
              <label>Phone</label>
              <input type="text" value="" name="phone" placeholder="@lang('app.phone')" />
            </div>
            <div class="col-md-6 col-sm-6 no-padding-right">
              <label>Subject</label>
              <input type="text" value=""name="subject"  />
            </div>
          </div>
          <div class="forminner">
            <div class="col-md-6 col-sm-6 no-padding-left">
              <label>Content</label>
              <textarea name="content" ></textarea>
            </div>
            <div class="col-md-6 col-sm-6 no-padding-right formbtn">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
              
            </div>
          </div>

          {!! Form::close() !!}
        </div>

        <div class="col-md-5 col-sm-5 no-padding contactbox">
          <ul>
            <li>
              <div class="leftdiv"><img src=" {{ URL::asset('assets/frontend/images/callus.png')}}"/></div>
              <div class="rightdiv">
                <h6>Call Us</h6>
                <p>+18086036035</p>
                <p>+18086036035</p>
              </div>
            </li>
            <li>
              <div class="leftdiv"><img src=" {{ URL::asset('assets/frontend/images/mailus.png')}}"/></div>
              <div class="rightdiv">
                <h6>Mail Us</h6>
                <p>info@tourthirsty.com</p>
              </div>
            </li>
            <li>
              <div class="leftdiv"><img src=" {{ URL::asset('assets/frontend/images/locate.png')}}"/></i></div>
              <div class="rightdiv">
                <h6>Address</h6>
                <p>15489 vegas Drive,Las Vegas,Neveda</p>
                <p>Cochin,kerala</p>
                <p>India,682013</p>
              </div>
            </li>
          </ul>
        </div>
        <div class="mapblock"> <img src=" {{ URL::asset('assets/frontend/images/contact-us.jpg')}}"/> </div>
      </div>
    </div>
  </div>
</div>
<section>
  <div class="container">
    <div class="row">
      <div class="btmad"><img class="img-responsive" src=" {{ URL::asset('assets/frontend/images/btmad.jpg')}}"/></div>
    </div>
  </div>
</section>
@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Auth\CreateRequest', '#cms-form') !!}
@stop
@include('frontend.includes.footer')