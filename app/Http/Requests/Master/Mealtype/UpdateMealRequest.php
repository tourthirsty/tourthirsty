<?php

namespace App\Http\Requests\Master\Mealtype;
use App\Http\Requests\Request;
use App\Hotel_meal_type;

class UpdateMealRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'meal_type' => 'required',
			'meal_desc' => 'required',
		];
	}
}
