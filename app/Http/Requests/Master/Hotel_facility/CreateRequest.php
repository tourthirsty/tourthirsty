<?php

namespace App\Http\Requests\Master\Hotel_facility;

use App\Http\Requests\Request;
use App\Hotelfacility;

class CreateRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'hotel_facility' => 'required|min:3',
			'hotel_facility_desc' => 'required|min:6',
		    ];
	}
}