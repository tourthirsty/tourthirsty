<?php

namespace App\Http\Requests\Master\Location_type;

use App\Http\Requests\Request;
use App\Locationtype;

class UpdateRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'location_type' => 'required|min:3',
			'location_type_desc' => 'required|min:6',
		    ];
	}
}
