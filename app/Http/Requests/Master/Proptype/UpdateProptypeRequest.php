<?php

namespace App\Http\Requests\Master\Proptype;
use App\Http\Requests\Request;
use App\Property_type;

class UpdateProptypeRequest extends Request {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'prop_type_name' => 'required',
		];
	}
}
