<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Activities as Activities;

//Controller for activity types of places
class ActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Activities = Activities::all();

        return view('dashboard.masters.activities.index',compact('Activities'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.masters.activities.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'activity' => 'required',
            'description' => 'required'
        ]);

        
        
        $Activities = new Activities;

        $Activities->activity = $request->activity;
        $Activities->description = $request->description;

        $Activities->save();
        
        return redirect()->route('dashboard.activities')
                        ->withSuccess(trans('Activity Added Succesfully'));
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $Activities = Activities::findOrFail($id);

         return view('dashboard.masters.activities.edit',compact('Activities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Activities = Activities::findOrFail($id);
        $this->validate($request, [
            'activity' => 'required',
            'description' => 'required'
        ]);

        $Activities->activity = $request->activity;
        $Activities->description = $request->description;

        $Activities->save();
        
        return redirect()->route('dashboard.activities')
                        ->withSuccess(trans('Activity Updated Succesfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Activities = Activities::findOrFail($id);

        $Activities->delete();

        return redirect()->route('dashboard.activities')
                        ->withSuccess(trans('Activity Deleted Succesfully'));
    }
}
