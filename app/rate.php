<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rate extends Model
{
    protected $table = 'rates';

    protected $fillable = [
		'room_type', 'rate_type', 'from_date', 'to_date', 'rate',
	];
}
