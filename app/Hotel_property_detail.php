<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel_property_detail extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'hotel_property_details';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		 'hotel_details_no_rooms','hotel_details_checkin_time','hotel_details_checkout_time',
		 'hotel_details_no_restaurants','hotel_details_roomservice_timing','hotel_details_bar', 
	];
}
