@extends('dashboard.layouts.master')

@section('page-title')

@section('page-header')
@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
    <h1>
       Add Meal Type
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li><a href="{{ route('meal.index') }}">Meal type</a></li>
        <li class="active">Create</li>
      </ol>
@endsection

@section('content')


@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif


    {!! Form::open(['route' => 'dashboard.meal.store', 'id' => 'create_meal-form']) !!}


<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Meal Type details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="meal_type">Meal type</label>
                    <input type="text" class="form-control" id="meal_type"
                           name="meal_type" placeholder="Eg:CP, EP, AP, MAP">
                </div>
                
                <div class="form-group">
                    <label for="meal_desc">Description</label>
                    <textarea name="meal_desc" id="meal_desc" class="form-control"></textarea>
                </div>
                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-2">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            Create 
        </button>
    </div>
</div>

@stop

@section('fter-scripts-end')
    
        {!! JsValidator::formRequest('App\Http\Requests\Master\Mealtype\CreateMealRequest', '#create_meal-form') !!}
   <

@stop