@extends('dashboard.layouts.master')

@section('page-title', 'Edit Room Type')

@section('page-header')

@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<h1>
    Edit Room Type
    <small>Room Type Details</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('roomtype.index') }}">>Room Types</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@include('partials.messages')



{!! Form::open(['route' => ['dashboard.roomtype.update', $prop->id], 'method' => 'PUT', 'id' => 'roomtype-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Room Type Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="room_type_name">Room Type</label>
                    <input type="text" class="form-control" id="room_type_name" name="room_type_name" required="" value="{{$prop->room_type_name }}">
                </div>
                <div class="form-group">
                    <label for="room_type_desc">Description</label>
                    <textarea name="room_type_desc" class="form-control" required="">{{$prop->room_type_desc }}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Roomtype\UpdateRoomtypeRequest', '#roomtype-form') !!}
@stop