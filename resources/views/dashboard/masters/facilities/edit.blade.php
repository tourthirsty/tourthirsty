@extends('dashboard.layouts.master')

@section('page-title', 'Edit Facility')

@section('page-header')
<h1>
    Edit Facility
    <small>Facility</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.facility') }}">>Facility</a></li>
    <li class="active">Update Details</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => ['dashboard.facility.update', $facility->id], 'method' => 'PUT', 'id' => 'facility-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Facility Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="facility">Facility</label>
                    <input type="text" class="form-control" id="facility" name="facility" placeholder="AC Room" required="" value="{{$facility->fac_name }}">
                </div>
                <div class="form-group">
                    <label for="facility_desc">Description</label>
                    <textarea name="facility_desc" placeholder="Eg:The hill stations are high-altitude towns used, especially by European colonialists..." class="form-control" required="">{{$facility->fac_desc }}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Facility\UpdateRequest', '#facility-form') !!}
@stop
