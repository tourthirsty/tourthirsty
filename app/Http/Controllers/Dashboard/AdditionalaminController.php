<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Additional_aminity;
use App\Http\Controllers\Controller;

class AdditionalaminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aminity = Additional_aminity::all();
        return view('dashboard.masters.additional_amini.index',compact('aminity'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.masters.additional_amini.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'add_aminity' => 'required',
                'add_amini_desc' => 'required',
                'ad_amin_rate' => 'required',
                ]);
        $add = new Additional_aminity;
        $add->add_aminity = $request->add_aminity;
        $add->add_amini_desc = $request->add_amini_desc;
        $add->ad_amin_rate = $request->ad_amin_rate; 
        $add->save();
        return redirect()->back()->withSuccess(trans('Addons successfully added!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $aminity = Additional_aminity::findOrFail($id);
         return view('dashboard.masters.additional_amini.edit',compact('aminity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aminity = Additional_aminity::findOrFail($id);

        $this->validate($request, [
            'add_aminity' => 'required',
            'add_amini_desc' => 'required',
            'ad_amin_rate' => 'required',
        ]);

        $aminity->add_aminity = $request->add_aminity;
        $aminity->add_amini_desc = $request->add_amini_desc;
        $aminity->ad_amin_rate = $request->ad_amin_rate; 
        $aminity->save();
        return redirect()->back()->withSuccess(trans('Addons successfully updated!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aminity = Additional_aminity::findOrFail($id);
        $aminity->delete();

        return redirect()->route('additionalamin.index')->withSuccess(trans('Additional Aminity successfully deleted!'));
    }
}
