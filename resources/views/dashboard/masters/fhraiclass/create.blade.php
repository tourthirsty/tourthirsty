@extends('dashboard.layouts.master')

@section('page-title', 'Add FHRAI Classification')

@section('page-header')
<h1>
    Add FHRAI Classification
    <small>FHRAI Classification</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
    <li><a href="{{ route('dashboard.fhrai') }}">>Fhrai Classification</a></li>
    <li class="active">Add New</li>
</ol>
@endsection

@section('content')

@include('partials.messages')


{!! Form::open(['route' => 'dashboard.fhrai.store', 'files' => true, 'id' => 'fhrai-form']) !!}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">FHRAI Classification</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="fhrai_class">FHRAI Class</label>
                    <input type="text" class="form-control" id="fhrai_class" name="fhrai_class" placeholder="Five Star" required="">
                </div>
                <div class="form-group">
                    <label for="fhrai_class_desc">Description</label>
                    <textarea name="fhrai_class_desc" placeholder="Eg:The hill stations are high-altitude towns used, especially by European colonialists..." class="form-control" required=""></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('after-scripts-end')
{!! JsValidator::formRequest('App\Http\Requests\Master\Fhrai\CreateRequest', '#fhrai-form') !!}
@stop
