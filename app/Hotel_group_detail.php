<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel_group_detail extends Model
{
     protected $table = 'hotel_group_details';
	 protected $fillable = array(
			'hotel_grp_name',
			'hotel_grp_address',
			'hotel_grp_contact');
}
